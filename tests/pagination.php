<?php
class koreTests_pagination extends PHPUnit_Framework_TestCase
{
    public function testFromRequest()
    {
        $_SERVER['PHP_SELF'] = '/';
        $_GET = array('foo' => 'bar', kore_pagination::URL_PARAM => 42);

        $pagination = kore_pagination::fromRequest();

        $p = rawurlencode(kore_pagination::URL_PARAM);
        $this->assertEquals($pagination->getUrl(), "/?foo=bar&$p=42");
        $this->assertEquals($pagination->getUrl(5), "/?foo=bar&$p=5");
    }

    public function testFromRoute()
    {
        $_SERVER['PHP_SELF'] = '/';
        $_GET = array('foo' => 'bar', kore_pagination::URL_PARAM => 42);

        $route = kore_route_route::fromRequest();
        $pagination = $route->addPagination();

        $p = rawurlencode(kore_pagination::URL_PARAM);
        $this->assertEquals($pagination->getUrl(), "/?foo=bar&$p=42");
        $this->assertEquals($pagination->getUrl(5), "/?foo=bar&$p=5");
    }

    public function testUrl()
    {
        $baseURL = 'http://foo/';
        $p = rawurlencode(kore_pagination::URL_PARAM);

        $pagination = new kore_pagination($baseURL);

        $this->assertEquals($pagination->getUrl(0), $baseURL .'?'.$p.'=0');
        $this->assertEquals($pagination->getUrl(1), $baseURL);
        $this->assertEquals($pagination->getUrl(2), $baseURL .'?'.$p.'=2');
    }

    public function testPages()
    {
        $pagination = new kore_pagination('http://foo/');
        $this->assertSame($pagination->getCurrentPage(), 1);

        $pagination->setMaxPerPage(10);
        $pagination->registerNumberOfItems(25);

        foreach([1, 2, 3] as $page){
            $pagination->setCurrentPage($page);
            $this->assertTrue($pagination->isValid());
        }

        foreach([0, 4, 5, 10] as $page){
            $pagination->setCurrentPage($page);
            $this->assertFalse($pagination->isValid());
        }
    }

}
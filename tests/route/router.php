<?php
class koreTests_route_router extends PHPUnit_Framework_TestCase
{
    public $router;

    public function setUp()
    {
        parent::setUp();

        kore::$conf->response_ignoreIfHeaderAlreadySent = true;

        $this->router  = new kore_route_router();
        $this->router->disableHTTPHeaders();
    }

    /**
     * Internal controller to help testing routing.
     *
     * @param  kore_route_route $route
     * @return kore_response_json
     */
    static public function controllerAction(kore_route_route $route)
    {
        $resp = new kore_response_json();
        $resp->setData($route);
        return $resp;
    }

    /**
     * Dispatch the given URI and return the route used.
     *
     * @param  string $uri
     * @param  string $domain
     * @param  array  $getParams
     * @return kore_route_route
     */
    protected function _dispatch($uri, $domain = null, array $getParams = null)
    {
        ob_start();
        $this->router->dispatch($uri, $domain, $getParams);
        $output = ob_get_clean();
        return json_decode($output);
    }

    /**
     * Shortcut to assert that the result rule is the good one.
     *
     * @param  string $uri
     * @param  string $domain
     * @param  string $routeName
     * @return kore_route_route
     */
    protected function _assertRoute($uri, $domain, $routeName)
    {
        $route = $this->_dispatch($uri, $domain);
        $this->assertEquals($route->name, $routeName);

        return $route;
    }

    /**
     * Check that if no rule match our URL, we obtain a 404 error.
     * (this test is run in a separate process, to be able to send headers)
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testNotFound()
    {
        $this->_dispatch('/foo', 'example.tld');
    }

    /**
     * Check handling of direct rules, when a query string is given.
     *
     * Direct rules have to be done before standard rules.
     * URI have to be checked before URL.
     */
    public function testDR_withQueryString()
    {
        $ctrl = __CLASS__ .'::controller';

        $this->router->setRules(array(
            ['/home/', $ctrl]
            ));

        $this->router->setDirectRules(array(
            '/home'         => $ctrl,
            '/home/'        => $ctrl,
            '/home/?test=1' => $ctrl,
            ));

        $route = $this->_dispatch('/home/?test=1', 'b.tld');

        $this->assertEquals($route->type, 'direct');
        $this->assertEquals($route->rule, '/home/?test=1');
    }

    /**
     * Check handling of direct rules, when a query string is given.
     *
     * Direct rules have to be done before standard rules.
     */
    public function testDR_withoutQueryString()
    {
        $ctrl = __CLASS__ .'::controller';

        $this->router->setRules(array(
            ['/home/', $ctrl]
            ));

        $this->router->setDirectRules(array(
            '/home'         => $ctrl,
            '/home/'        => $ctrl,
            '/home/?test=1' => $ctrl,
            ));

        $route = $this->_dispatch('/home/', 'b.tld');

        $this->assertEquals($route->type, 'direct');
        $this->assertEquals($route->rule, '/home/');
    }

    /**
     * Initialize some «standard routing» rules
     */
    protected function _initSR_rules()
    {
        $ctrl = __CLASS__ .'::controller';

        $this->router->setRules(array(
            'R00' => ['/index/[:foo]', $ctrl],

            'R10' => ['/home/[:foo]/[w:bar]', $ctrl],

            'R20' => ['/catA/[:foo]/[:bar]?', $ctrl],
            'R21' => ['/catB/[:foo]?/[:bar]', $ctrl],

            'R22' => ['/catC/[:foo]', $ctrl],

            ));
    }

    public function testSR_R00()
    {
        $this->_initSR_rules();
        $this->_assertRoute('/index/bar', null, 'R00');
    }

    public function testSR_R00_withQueryString()
    {
        $this->_initSR_rules();
        $this->_assertRoute('/index/bar?foo=1', null, 'R00');
    }

    public function testSR_R10_one_word()
    {
        $this->_initSR_rules();
        $this->_assertRoute('/home/lorem/ipsum', null, 'R10');
    }

    public function testSR_R10_multi_words()
    {
        $this->_initSR_rules();
        $this->_assertRoute('/home/lorem-ipsum/dolor_sit_amet', null, 'R10');
    }

    /**
     * Check that by adding a prefix, the route doesn't work anymore.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_R10_withPrefix()
    {
        $this->_initSR_rules();
        $this->_dispatch('foo/home/lorem-ipsum/dolor_sit_amet');
    }

    /**
     * Check that by adding a suffix, the route doesn't work anymore.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_R10_withSuffix()
    {
        $this->_initSR_rules();
        $this->_dispatch('/home/lorem-ipsum/dolor_sit_amet.html');
    }

    /**
     * Check that rules are correctly escaped : symbols which have a special
     * meaning for PCRE must be escaped to avoid unexpected behavior.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_escaped()
    {
        $ctrl = __CLASS__ .'::controller';
        $this->router->setRules(array(
            ['/index.html', $ctrl],
            ));

        $this->_dispatch('/index/html');
    }

    /**
     * Check that the R20 rule match, with only one param.
     */
    public function testSR_R20_one_param()
    {
        $this->_initSR_rules();
        $this->_assertRoute('/catA/lorem', null, 'R20');
    }

    /**
     * Check that the R20 rule doesn't match with an extraneous slash.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_R20_one_param_then_slash()
    {
        $this->_initSR_rules();
        $this->_dispatch('/catA/lorem/');
    }

    /**
     * Check that the R20 rule match, with two params.
     */
    public function testSR_R20_two_params()
    {
        $this->_initSR_rules();
        $this->_assertRoute('/catA/lorem/ipsum', null, 'R20');
    }

    /**
     * Check that the R21 rule doesn't match without any param.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_R21_without_param()
    {
        $this->_initSR_rules();
        $this->_dispatch('/catB');
    }

    /**
     * Check that the R21 rule doesn't match without any param, even if
     * followed by a slash.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_R21_without_param_then_slash()
    {
        $this->_initSR_rules();
        $this->_dispatch('/catB/');
    }

    /**
     * Check that the R21 rule doesn't match with one param followed by a slash.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 404
     */
    public function testSR_R21_one_param_then_slash()
    {
        $this->_initSR_rules();
        $this->_dispatch('/catB/lorem/');
    }

    /**
     * Check that the R21 rule correctly match with only one parameter.
     */
    public function testSR_R21_one_param()
    {
        $this->_initSR_rules();
        $route = $this->_assertRoute('/catB/lorem', null, 'R21');
        $this->assertEquals($route->params->bar, 'lorem');
    }

    /**
     * Check that the R21 rule correctly match with two parameters.
     */
    public function testSR_R2X()
    {
        $this->_initSR_rules();
        $route = $this->_assertRoute('/catB/lorem/ipsum', null, 'R21');
        $this->assertEquals($route->params->foo, 'lorem');
        $this->assertEquals($route->params->bar, 'ipsum');
    }

    /**
     * Check that we can't overide routing parameters with standard query
     * string.
     *
     * @expectedException     kore_response_httpStatus
     * @expectedExceptionCode 403
     */
    public function testSR_overriding()
    {
        $ctrl = __CLASS__ .'::controller';

        $this->router->setRules(array(
            ['/catC/[:foo]', $ctrl],
            ));

        $this->_dispatch('/catC/one', 'example.tld', ['foo' => 'two']);
    }

    /**
     * Check several case of reverse routing
     */
    protected function _initReverseRules()
    {
        $ctrl = __CLASS__ .'::controller';

        $this->router->setRules(array(
            'R01' => ['/', $ctrl],
            'R02' => ['/index/[:foo]', $ctrl],
            'R03' => ['/home/[:foo]/[:bar]', $ctrl],
            'R04' => ['/catA/[:foo]/[:bar]?', $ctrl],
            'R05' => ['/catB.[:foo]?/[:bar]', $ctrl],
            ));
    }

    /**
     * Check that an empty rule works as defined.
     */
    public function testReverseWithoutParams()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R01');
        $this->assertEquals($route->reverse(), '/');
        $this->assertEquals($route->set('foo', 'b&r')->reverse(), '/?foo=b%26r');
    }

    /**
     * Check that we refuse building reverse for R02 if a parameter is missing.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR02MissingParameter()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R02');
        $route->reverse();
    }

    /**
     * Check that the reverse for R02 works as expected.
     *
     */
    public function testReverseR02()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R02');
        $this->assertEquals($route->set('foo', 'bar')->reverse(), '/index/bar');
    }

    /**
     * Check that the reverse for R03 is refused if "foo" was not provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR03_missingFoo()
    {
        $this->_initReverseRules();
        $route = $this->router->getRoute('R03');
        $route = clone $route;
        $route->set('bar', 'two');
        $route->reverse();
    }

    /**
     * Check that the reverse for R03 is refused if "bar" was not provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR03_missingBar()
    {
        $this->_initReverseRules();
        $route = $this->router->getRoute('R03');
        $route = clone $route;
        $route->set('foo', 'one');
        $route->reverse();
    }

    /**
     * Check that the reverse for R03 is refused if both params were not
     * provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR03_missingBoth()
    {
        $this->_initReverseRules();
        $route = $this->router->getRoute('R03');
        $route = clone $route;
        $route->reverse();
    }

    /**
     * Check that the reverse for R03 works as expected.
     */
    public function testReverseR03()
    {
        $this->_initReverseRules();
        $route = $this->router->getRoute('R03');
        $route = clone $route;
        $route->set('foo', 'one')->set('bar', 'two');

        $this->assertEquals($route->reverse(), '/home/one/two');
    }

    /**
     * Check that the reverse for R04 is refused if no param was provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR04_missingAll()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R04');
        $route->reverse();
    }

    /**
     * Check that the reverse for R04 is refused if only the "bar" param was
     * provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR04_bar()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R04');
        $route = clone $route;
        $route->set('bar', 'two');

        $route->reverse();
    }

    /**
     * Check that the reverse for R04 works as expected with only the "foo"
     * param.
     */
    public function testReverseR04_foo()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R04');
        $this->assertEquals($route->set('foo', 'one')->reverse(), '/catA/one');

        return $route;
    }

    /**
     * After checking testReverseR04_foo(), check that adding the "bar" param
     * works as expected.
     *
     * @depends testReverseR04_foo
     */
    public function testReverseR04_fooBar(kore_route_route $route)
    {
        $this->assertEquals($route->set('bar', 'two')->reverse(),
                '/catA/one/two');
    }

    /**
     * Check that the reverse for R05 is refused if no param was provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR05_missingAll()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R05');
        $route->reverse();
    }

    /**
     * Check that the reverse for R05 is refused if only "foo" was provided.
     *
     * @expectedException     kore_route_exception
     * @expectedExceptionCode kore_route_exception::MISSING_PARAMETER
     */
    public function testReverseR05_foo()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R05');
        $route = clone $route;
        $route->set('foo', 'one');
        $route->reverse();
    }

    /**
     * Check that the reverse for R05 works as expected, if only "bar" was
     * provided.
     */
    public function testReverseR05_bar()
    {
        $this->_initReverseRules();

        $route = $this->router->getRoute('R05');
        $this->assertEquals($route->set('bar', 'two')->reverse(), '/catB/two');

        return $route;
    }

    /**
     * Check that the reverse for R05 works as expected if both "foo" and "bar"
     * was provided.
     *
     * @depends testReverseR05_bar
     */
    public function testReverseR05_fooBar(kore_route_route $route)
    {
        $this->assertEquals($route->set('foo', 'one')->reverse(),
                '/catB.one/two');
    }

}

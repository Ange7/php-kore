#!/bin/sh

SCRIPT=`readlink -f "$0"`
SCRIPTDIR=`dirname "$SCRIPT"`

find "$SCRIPTDIR" -name '*php' -not -name 'phpunit*' -print0 |\
    xargs -r -n1 -0 phpunit --colors --verbose --bootstrap "$SCRIPTDIR/phpunit-bootstrap.php"

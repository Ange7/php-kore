<?php
/**
 * Module fournissant la classe { @link kore_db_dummy }.
 *
 * @package db
 */

/**
 * Classe de simulation de connexion SQL en erreur : tous
 * les appels de fonctions devraient renvoyer systématiquement "false".
 *
 * Cette classe est utilisée en remplacement de l'instance
 * d'une classe PDO ayant générée une erreur lors de l'instanciation.
 *
 * L'idée est de faire en sorte que le script continue à fonctionner, sans pour
 * autant déclencher d'erreur fatale.
 *
 * @package db
 *
 */
class kore_db_dummy extends kore_db_pdo
{
    public function __construct( $dsn = NULL, $user = NULL, $password = NULL, $options = NULL, $identifier = NULL )
    {
        /*
         * Trace l'identifiant de l'instance.
         */
        if( $identifier !== NULL )
            $this->identifier = $identifier;
    }

    public function exec( $statement )
    {
        return false;
    }

    public function query( $statement, $mode = PDO::FETCH_ASSOC, $modeOpt = null, $ctorargs = null )
    {
        return false;
    }

    public function q( $statement, array $params, $prepareOptions = null )
    {
        return false;
    }

    public function prepare( $statement, $options = array() )
    {
        return false;
    }
}

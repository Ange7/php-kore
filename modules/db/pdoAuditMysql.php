<?php
/**
 * Audit des connexions MySQL via PDO { @link kore_db_pdoAuditMysql }.
 *
 * @package db
 */

/**
 * Etablie une connexion à une base de données MySQL via PDO,
 * et surveille le bon fonctionnement des diverses requêtes
 * exécutées.
 *
 * @package db
 */
class kore_db_pdoAuditMysql extends kore_db_pdoAudit
{
	/**
	 * Version du serveur MySQL
	 *
	 * @var string
	 */
	protected $serverVersion = '3.23';

	/**
	 * Indique si le serveur supporte le profiling.
	 *
	 * @var boolean
	 */
    protected $isProfilingCapable = false;

	/**
	 * Initialize l'audit.
	 */
    protected function initializeAudit()
    {
        parent::initializeAudit();

        $this->setAttribute(PDO::MYSQL_ATTR_DIRECT_QUERY, true);

    	$this->ignoreNextExplain = true;
    	$version = $this->selectFirst("select version() as version", NULL,
    	       PDO::FETCH_OBJ);

        if( $version and isset($version->version) )
            $this->serverVersion = $version->version;

        if( version_compare($this->serverVersion, '5.0.37', '>=') ) {
            $this->ignoreNextExplain = true;
            $this->query("set profiling = 1");
            $this->isProfilingCapable = true ;
        }
    }

    /**
     * Lance le profiling de la requête.
     *
     * @return array
     */
    public function doProfiling( )
    {
        $profilingResult = array();
        if( ! $this->isProfilingCapable )
            return $profilingResult;

        if( ( $result = $this->query("show profile") ) !== false ) {
            $profilingResult['list'] = array();
            while( ( $row = $result->fetchObject() ) !== false ) {
                $row->Duration = bcmul($row->Duration, 1000, 7). 'ms';
                $profilingResult['list'][] = $row;
            }
        }

        return $profilingResult ;
    }

    /**
     * Lance l'explain de la requête.
     *
     * @link http://dev.mysql.com/doc/refman/5.1/en/explain.html
     *
     * @param  string  $queryString
     * @param  array   $params
     * @return array
     */
    public function doExplain( $queryString, $params )
    {
    	if( ( $result = parent::doExplain( $queryString, $params ) ) === false )
	       return $result;

    	$warn = array();

    	$firstWithoutConst = true;
    	$count = 0;
    	foreach( $result['list'] as $idx => $row ) {
    	    if( $row->Extra === 'Select tables optimized away' )
                continue;
    	    ++$count;

    	    $row->type = strtolower( $row->type );
    	    if( $row->select_type === 'UNION' )
                $firstWithoutConst = true;
    	    $checkFullScan = ( $row->select_type !== 'UNION RESULT' );
    	    $fullScan = ( $row->type === 'all' or $row->type === 'index' );

    	    if( $firstWithoutConst === true ) {
    		if( $row->type !== 'const' and $row->type !== 'system' ) {
    		    $firstWithoutConst = false;

    		    if( $checkFullScan and $fullScan ) {
                    $warn['fullscan_notice'] = 'not very good full table scan, line #'.($count);
    		    }
    		}

    	    } elseif( $checkFullScan and $fullScan ) {
                $warn['fullscan_warning'] = 'very bad full table scan, line #'.($count);
    	    }

    	    if( $row->key === NULL
                and ( stripos( $row->Extra, 'no tables' ) === false )
                and ( $row->rows >= 10 ) ) {

    		    $warn['nokey_notice'] = 'no key used, line #'.($idx+1);
    	    }

    	    if( stripos( $row->Extra, 'filesort' ) !== false and
                stripos( $row->Extra, 'temporary' ) !== false ) {

                $warn['filesort_and_temporary_notice'] = true ;
    	    }
    	}

    	$this->formatExplain( $result['list'] );

    	if( !empty($warn) )
    	    $result['alert'] = $warn;

    	if( ( $res = $this->query( 'show warnings' ) ) !== false )
    	    $result['extended infos'] = $res->fetchAll( PDO::FETCH_OBJ );

    	return $result ;
    }

}

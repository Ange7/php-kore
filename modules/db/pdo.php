<?php
/**
 * Module d'interfaçage avec PDO { @link kore_db_pdo }.
 *
 * @package db
 */

/**
 * Classe d'accès à une base de données via PDO.
 *
 * Ce qui change de la classe PDO d'origine :
 * - conserve la casse de la requete pour les colonnes de résultat
 * - résultats en FETCH_ASSOC par défaut (au lieu de FETCH_BOTH)
 * - méthodes q() & e(), versions de query() & exec() acceptant des paramètres
 * - ajout d'autres méthodes raccourci : selectFirst(), insert(), update(), etc
 *
 * De plus en cas d'erreur lors de l'instanciation de la classe, kore_db_config
 * remplacera l'instance PDO par une instance de kore_db_dummy, afin que le
 * script puisse continuer à fonctionner sans trop de problèmes.
 *
 * @package db
 */
class kore_db_pdo extends PDO
{
    /**
     * Identifiant de l'instance, utilisé uniquement à des fins de débugage.
     *
     * @var string
     */
    public $identifier;

    public $openKeyword  = '"';
    public $closeKeyword = '"';


    protected $_prepareCache;
    protected $_shutdownCallbacks;
    public $prepareCacheLimit;


    public static function getPDOType($phpType)
    {
        switch($phpType){
            case 'boolean':
                $type = PDO::PARAM_BOOL;
                break;
            case 'integer':
                $type = PDO::PARAM_INT;
                break;
            case 'NULL':
                $type = PDO::PARAM_NULL;
                break;
            default:
                $type = PDO::PARAM_STR;
        }

        return $type;
    }

    public static function bindAuto(PDOStatement $stmt, array $params)
    {
        foreach ($params as $key => $value) {
            $type = self::getPDOType(gettype($value));
            $stmt->bindValue(':'.$key, $value, $type);
        }
    }

    /**
     * Instanciation de la classe.
     *
     * @link http://fr.php.net/manual/fr/function.PDO-construct.php
     *
     * @param string  $dsn
     * @param string  $user
     * @param string  $password
     * @param array   $options
     */
    public function __construct( $dsn, $user = NULL, $password = NULL, $options = NULL, $identifier = NULL )
    {
        /*
         * Trace l'identifiant de l'instance.
         */
        if ($identifier !== NULL)
            $this->identifier = $identifier;
        else
            $this->identifier = $dsn;


        $defaultOptions = array(
            PDO::ATTR_ERRMODE => kore::$conf->get('db_errorMode', PDO::ERRMODE_WARNING),
            PDO::ATTR_TIMEOUT => kore::$conf->get('db_connectionTimeout', 5),
            PDO::ATTR_CASE => PDO::CASE_NATURAL,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            );

        if( is_array($options) )
            $defaultOptions = $options + $defaultOptions;

        parent::__construct($dsn, $user, $password, $defaultOptions);

        /**
         * Merci MySQL de ne pas respecter la norme...
         */
        if( $this->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql' )
            $this->closeKeyword = $this->openKeyword = '`';
    }

    /**
     * Destructeur.
     */
    public function __destruct()
    {
        $this->_close();

        /**
         * Informe kore::$db de la fermeture de l'objet.
         */
        kore::$db->eventClosed($this->identifier);
    }

    /**
     * we make __clone private so nobody can clone the instance
     */
    private function __clone()
    {}

    /**
     * Fourni une méthode à kore_db_config afin d'indiquer à l'objet qu'on veut
     * le détruire, et qu'il doit donc se débarrasser des références pouvant
     * empêcher cette destruction.
     */
    public function _close()
    {
        /*
         * If callbacks were registered on the current instance, call them to be
         * able to release resources.
         */
        if ($this->_shutdownCallbacks !== null)
            foreach ($this->_shutdownCallbacks as $callback)
                call_user_func($callback, $this);
        $this->_shutdownCallbacks = null;

        /*
         * S'assure que le cache de requêtes préparées soit vide.
         */
        $this->_prepareCache = null;
    }

    /**
     * Add a callback to the current instance, which will be called before
     * closing the database.
     * It can helps release some resources by clearing caches for example.
     *
     * @param callable $callback
     * @param string   $uniqueId
     */
    public function registerShutdownCallback(/*callable*/ $callback, $uniqueId = null)
    {
        if ($this->_shutdownCallbacks === null)
            $this->_shutdownCallbacks = array();

        if ($uniqueId === null)
            $this->_shutdownCallbacks[] = $callback;
        elseif (!isset($this->_shutdownCallbacks[$uniqueId]))
            $this->_shutdownCallbacks[$uniqueId] = $callback;
    }

    /**
     * Protège un mot clé.
     *
     * @param  string  $keyword
     * @return string
     */
    public function quoteKeyword( $keyword )
    {
        return $this->openKeyword . $keyword . $this->closeKeyword;
    }

    /**
     * Protège une valeur, en respectant son type.
     *
     * @param  mixed   $value
     * @return string
     */
    public function quoteValue( $value )
    {
        /**
         * TODO: trouver un moyen de corriger ça avec PDO
         */
        if( is_bool($value) ) $value = (int) $value;

        if( is_null($value) ) return 'NULL';

        return $this->quote($value, self::getPDOType(gettype($value)));
    }

    /**
     * Exécute une requête paramétrée et retourne le nombre de lignes affectées.
     *
     * @param  string  $statement
     * @param  array   $params
     * @param  array   $prepareOptions
     * @return integer
     */
    public function e( $statement, array $params, $prepareOptions = null )
    {
        $stmt = $this->q($statement, $params, $prepareOptions);
        if ($stmt === false)
           return false;
        else return $stmt->rowCount();
    }


    /**
     * Exécute une requête paramétrée et retourne un PDOStatement.
     *
     * $params peut être passé afin de passer les paramètres d'entrée de
     * la requête.
     *
     * @param  string  $statement
     * @param  array   $params
     * @param  array   $prepareOptions
     * @return PDOStatement
     */
    public function q( $statement, array $params, $prepareOptions = null )
    {
        if ($prepareOptions === null) {
            /*
             * Si aucune option n'a été fournie, alors tente d'activer
             * l'émulation des "prepared statement", afin d'éviter un éventuel
             * aller-retour inutile avec le serveur.
             */

            if ($this->getAttribute(PDO::ATTR_DRIVER_NAME) === 'mysql')
                $prepareOptions = array(PDO::MYSQL_ATTR_DIRECT_QUERY => true);
        }

        if ($prepareOptions !== null)
            $stmt = $this->prepare($statement, $prepareOptions);
        else
            $stmt = $this->prepare($statement);

        if ($stmt !== false and ($stmt->execute($params) === false))
            $stmt = false;

        return $stmt;
    }

    /**
     * Prepare une requête, et conserve l'instance de PDOStatement, afin d'en
     * faciliter la ré-utilisation.
     * A noter que si PDO est configuré pour émuler les prepare, cela n'a que
     * peu d'intérêt.
     *
     * @param  string  $statement
     * @return PDOStatement
     */
    public function keepPrepare( $statement )
    {
        if( $this->_prepareCache === null ){
            $this->_prepareCache = array();

            if( $this->prepareCacheLimit === null )
                $this->prepareCacheLimit = kore::$conf->get(
                        'db_prepareCacheLimit', 50);
        }

        $key = md5($statement);
        if( isset($this->_prepareCache[$key]) ){
            $stmt = $this->_prepareCache[$key];

            /**
             * Déplace systématiquement l'item en fin de tableau (LRU).
             */
            unset($this->_prepareCache[$key]);
            $this->_prepareCache[$key] = $stmt;

            return $stmt;
        }

        $stmt = $this->prepare($statement);
        if( $stmt === false )
            return false;
        $this->_prepareCache[$key] = $stmt;
        if( count($this->_prepareCache) > $this->prepareCacheLimit )
            array_shift($this->_prepareCache);

        return $stmt;
    }


    /**
     * Exécute une requête (paramétrée ou non) et retourne la première ligne de
     * résultat.
     *
     * @param  string  $statement
     * @param  array   $params
     * @param  integer $fetchStyle
     * @return mixed
     */
    public function selectFirst( $statement, $params = null, $fetchStyle = null )
    {
        if ($statement instanceOf PDOStatement) {
            if (!$statement->execute($params))
                return false;
            $res = $statement;
        } elseif (is_array($params)) {
            $res = $this->q($statement, $params);
        } else {
            $res = $this->query($statement);
        }
        $result = false;
        if ($res !== false){
            if (($row = $res->fetch($fetchStyle)) !== false)
                $result = $row ;
            $res->closeCursor();
        }
        return $result ;
    }


    /**
     * Exécute un insert simplement à partir d'un nom de table et d'un tableau
     * associatif.
     *
     * @param  string  $table
     * @param  array   $params
     * @param  string  $prefix
     * @param  string  $suffix
     * @return integer
     */
    public function insert( $table, $params, $prefix = null, $suffix = null )
    {
        $cols = '';
        $values = '';
        foreach( $params as $key => $value ){
            if( $cols !== '' ){
                $cols .= ', ';
                $values .= ', ';
            }
            $cols .= $this->quoteKeyword($key);
            $values .= $this->quoteValue($value);
        }

        $query = "insert $prefix into ".$this->quoteKeyword($table)." ($cols)
                  values ($values) $suffix";

        return $this->exec($query);
    }


    /**
     * Exécute un update simplement à partir d'un nom de table, et de deux
     * tableaux associatifs.
     *
     * @param  string  $table
     * @param  array   $params
     * @param  mixed   $keys
     * @param  string  $suffix
     * @return integer
     */
    public function update( $table, $sets, $keys, $prefix = null, $suffix = null )
    {
        $set = '';
        foreach( $sets as $k => $value ){
            if( $set !== '' ) $set .= ', ';
            $set .= $this->quoteKeyword($k) .'='. $this->quoteValue($value);
        }

        if( is_string($keys) )
            $where = $keys;
        else {
            $where = '';
            foreach( $keys as $k => $value ){
                if( $where !== '' ) $where .= "\nand ";
                $where .= $this->quoteKeyword($k).'='.$this->quoteValue($value);
            }
        }

        $query = "update $prefix ".$this->quoteKeyword($table).
                 "\nset $set\nwhere $where$suffix";

        return $this->exec($query);
    }


    /**
     * Exécute un update simplement à partir d'un nom de table, et de deux
     * tableaux associatifs.
     *
     * @param  string  $table
     * @param  mixed   $keys
     * @param  string  $suffix
     * @return integer
     */
    public function delete( $table, $keys, $prefix = null, $suffix = null )
    {
        if( is_string($keys) )
            $where = $keys;
        else {
            $where = '';
            foreach( $keys as $k => $value ){
                if( $where !== '' ) $where .= "\nand ";
                $where .= $this->quoteKeyword($k).'='.$this->quoteValue($value);
            }
        }

        $query = "delete $prefix from ".$this->quoteKeyword($table).
                 "\nwhere $where$suffix";

        return $this->exec($query);
    }


    /**
     * Fait un grand nombre d'insert par paquet.
     *
     * @param  array   $src
     * @param  string  $queryHeader
     * @param  integer $nbItems
     * @return integer
     */
    public function multiInsert( $src, $queryHeader, $nbItems = 1024 )
    {
        $count = 0;
        $buffer = array();

        $nbInserted = 0;
        foreach( $src as $row ) {
            $buffer[] = $row;
            if( (++$count % $nbItems )===0 ) {
                $nbInserted+=$this->multiInsertArray($buffer, $queryHeader);
                $buffer = array();
            }
        }

        if( count($buffer) > 0 )
            $nbInserted += $this->multiInsertArray($buffer, $queryHeader);

        return $nbInserted;
    }

    /**
     * Fait un grand nombre d'insert.
     *
     * @param  Array   $src
     * @param  string  $queryHeader
     * @return integer
     */
    public function multiInsertArray( $array, $queryHeader)
    {
        $query = $queryHeader;
        foreach( $array as $k=> $row ) {
            if( $k !== 0 )
                $line = ",\n(";
            else
                $line = '(';

            $nbCell = 0;
            foreach( $row as $cell ){
                if( $nbCell++ > 0 )
                    $line .= ',';
                $line .= $this->quoteValue($cell);
            }

            $query .= $line . ')';
        }

        return (int)$this->exec($query);
    }

    public function buildBindList($values, $separator, $repeatColumn = true)
    {
        $result = '';

        foreach( $values as $columnName => $value ){
            if( $result !== '' )
                $result .= $separator;
            if( $repeatColumn )
                $result .= $this->quoteKeyword($columnName).' = ';
            $result .= ':'.$columnName;
        }

        return $result;
    }


    /**
     * Méthode permettant d'utiliser des listes de valeur par lot.
     *
     * @param  $values
     * @param  $maxValue
     * @return array
     */
    public function buildInList($values, $maxValue = 1024, $function=null)
    {
        $return = array();

        $currentList = '';
        $nb = 0;
        $nbList = 0;
        foreach( $values as $value ){
            if( $currentList === '' )
                $str = '';
            else $str = ',';

            if($function!==null)
                $str .= $function.'(';

            if( is_int($value) )
                $str .= $value;
            else $str .= $this->quoteValue($value);

            if($function!==null)
                $str .= ')';

            $currentList .= $str;

            if( ++$nb >= $maxValue ) {
                $return[$nbList++] = $currentList;
                $currentList = '';
                $nb = 0;
            }
        }
        if( $currentList !== '' )
            $return[$nbList++] = $currentList;

        return $return;
    }

    /**
     * Méthode permettant d'utiliser des listes de valeur.
     *
     * @param  $values
     * @return string
     */
    public function buildSimpleInList($values)
    {
        $currentList = '';

        foreach ( $values as $value ) {
            if ( $currentList === '' )
                $str = '';
            else
                $str = ',';

            if ( is_int($value) )
                $str .= $value;
            else
                $str .= $this->quoteValue($value);

            $currentList .= $str;
        }

        return $currentList;
    }

}

<?php

class kore_db_schema_table
{
    static private $_cacheInstances  = array();

    /**
     *
     * @var kore_db_pdo
     */
    protected $_db;

    /**
     *
     * @var string
     */
    protected $_schema;


    /**
     *
     * @var string
     */
    protected $_name;


    /**
     *
     * @var boolean
     */
    protected $_haveUniqueKeys;


    /**
     * We use a column cache, to avoid a lot of SQL calls.
     *
     * @var array
     */
    protected $_columns;


    /**
     * Instancie la classe actuelle, en s'assurant de ne pas créer de doublon.
     *
     * @param  kore_db_pdo $db
     * @param  string $schema
     * @param  string $name
     * @return kore_db_schema_table
     */
    static public function instanciate(kore_db_pdo $db, $schema, $name)
    {
        $idInstance = $db->identifier.':'.$schema.'.'.$name;
        if (isset(self::$_cacheInstances[$idInstance]))
            return self::$_cacheInstances[$idInstance];

        $obj = new static;
        self::$_cacheInstances[$idInstance] = $obj;

        $obj->_db     = $db;
        $obj->_schema = $schema;
        $obj->_name   = $name;

        return $obj;
    }

    /**
     * Purge le cache d'instance de PDO.
     */
    static public function purgeInstanceCache()
    {
        kore_db_schema_foreignKey::purgeInstanceCache();
        kore_db_schema_column::purgeInstanceCache();

        self::$_cacheInstances = array();
    }

    /**
     * Retourne le nom de la table complet et protégé
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->getSchema()->getFullName() .'.'.
                $this->_db->quoteKeyword($this->_name);
    }

    /**
     * Retourne le nom de la table
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Retourne l'instance du schéma correspondant.
     *
     * @return kore_db_schema_schema
     */
    public function getSchema()
    {
        return kore_db_schema_schema::instanciate($this->_db, $this->_schema);
    }

    /**
     * Retourne la liste des colonnes de la table
     *
     * @return array
     */
    public function getColumns()
    {
        if (is_array($this->_columns))
            return $this->_columns;

        $query =
               "SELECT *
                FROM information_schema.columns
                WHERE table_schema = :tableSchema
                AND table_name = :tableName
                ORDER BY ordinal_position";

        $params = array(
                'tableSchema' => $this->_schema,
                'tableName'   => $this->_name,
                );

        $stmt = $this->_db->keepPrepare($query);

        $this->_columns = array();

        if ($stmt) {
            $stmt->execute($params);

            foreach ($stmt as $row){
                $column = kore_db_schema_column::fetch($this->_db, $row);
                $this->_columns[$column->getName()] = $column;
            }
        }

        return $this->_columns;
    }

    /**
     * Retourne une colonne de la table
     *
     * @param  kore_db_schema_column $columnName
     * @throws DomainException
     */
    public function getColumn($columnName)
    {
        $columns = $this->getColumns();

        foreach ($columns as $cName => $column){
            if (strcasecmp($cName, $columnName) === 0)
                return $column;
        }

        throw new DomainException("column not found ($columnName in {$this->_name})");
    }

    /**
     * Retourne la liste des colonnes composant la clé primaire de la table, ou
     * un tableau vide si la table n'a pas de clé primaire.
     *
     * @return array
     */
    public function getPrimaryKey()
    {
        $columns = $this->getColumns();

        $list = array();
        foreach ($columns as $column)
            if ($column->isPrimaryKey)
                $list[$column->getName()] = $column;

        return $list;
    }

    /**
     * Indique si la table contient au moins une clause d'unicité, en plus de la
     * primary key.
     *
     * @return boolean
     */
    public function haveUniqueKeys()
    {
        if ($this->_haveUniqueKeys === null){
            $this->_haveUniqueKeys = false;

            $query =
                   "SELECT count(*) as nb
                    FROM information_schema.table_constraints c
                    WHERE c.table_schema = :tableSchema
                    AND c.table_name = :tableName
                    AND c.constraint_type = 'UNIQUE'";

            $params = array(
                    'tableSchema' => $this->_schema,
                    'tableName'   => $this->_name,
                    );

            if ($row = $this->_db->selectFirst($query, $params))
                $this->_haveUniqueKeys = (bool) $row['nb'];
        }

        return $this->_haveUniqueKeys;
    }

    /**
     * Retourne la liste des contraintes d'unicité, à l'exception de la clé
     * primaire.
     *
     * @return array
     */
    public function getUniqueKeys()
    {
        $list = array();
        if (!$this->haveUniqueKeys())
            return $list;

        $query =
               "SELECT u.*
                FROM information_schema.table_constraints c
                JOIN information_schema.key_column_usage u USING (
                    table_schema, table_name, constraint_schema, constraint_name)
                WHERE c.table_schema = :tableSchema
                AND c.table_name = :tableName
                AND c.constraint_type = 'UNIQUE'
                ORDER BY u.constraint_name, u.ordinal_position";

        $params = array(
                'tableSchema' => $this->_schema,
                'tableName'   => $this->_name,
                );

        if ($res = $this->_db->q($query, $params)){
            foreach ($res as $row){
                $constraint = $row['CONSTRAINT_NAME'];

                $column = kore_db_schema_column::fetch($this->_db, $row);

                if (!isset($list[$constraint]))
                    $list[$constraint] = array($column->getName() => $column);
                else
                    $list[$constraint][$column->getName()] = $column;
            }
        }

        return $list;
    }


    /**
     * Retourne la liste des tables faisant référence à cette table.
     *
     * @return array
     */
    public function isReferencedBy()
    {
        $query =
               "SELECT *
                FROM information_schema.key_column_usage
                WHERE referenced_table_schema = :tableSchema
                AND referenced_table_name = :tableName
                ORDER BY constraint_schema, constraint_name, ordinal_position";

        $params = array(
                'tableSchema' => $this->_schema,
                'tableName'   => $this->_name,
                );

        $list = array();

        $stmt = $this->_db->keepPrepare($query);

        if ($stmt->execute($params)){
            foreach ($stmt as $row){
                $fk = kore_db_schema_foreignKey::instanciate($this->_db,
                        $row['CONSTRAINT_SCHEMA'], $row['CONSTRAINT_NAME']);
                $fk->fetchProperties($row);

                $list[$fk->getFullName()] = $fk;
            }
        }

        return $list;
    }


    /**
     * Retourne la liste des tables auxquelles cette table fait référence.
     *
     * @return array
     */
    public function references()
    {
        $query =
               "SELECT *
                FROM information_schema.key_column_usage
                WHERE table_schema = :tableSchema
                AND table_name = :tableName
                AND referenced_table_name is not null
                ORDER BY constraint_schema, constraint_name, ordinal_position";

        $params = array(
                'tableSchema' => $this->_schema,
                'tableName'   => $this->_name,
                );

        $list = array();

        $stmt = $this->_db->keepPrepare($query);

        if ($stmt->execute($params)){
            foreach ($stmt as $row){
                $fk = kore_db_schema_foreignKey::instanciate($this->_db,
                        $row['CONSTRAINT_SCHEMA'], $row['CONSTRAINT_NAME']);
                $fk->fetchProperties($row);

                $list[$fk->getFullName()] = $fk;
            }
        }

        return $list;
    }

    /**
     * Essaye de détecter s'il s'agit d'une table N..M ou non, en se basant sur
     * les ForeignKey.
     *
     * Ici par N..M on considère les tables dont la clé primaire est composée de
     * deux clés étrangères, et rien d'autre.
     *
     * Retourne les deux instances de table s'il s'agit d'une N..M, false sinon.
     *
     * @return array
     */
    public function isNM()
    {
        $pk = $this->getPrimaryKey();
        if (!$pk)
            return false;

        $listFK = $this->references();
        if (count($listFK) < 2)
            return false;

        $notUsedCols = $pk;

        /*
         * On ne conserve que les ForeignKey qui composent la PrimaryKey.
         */
        foreach ($listFK as $idx => $fk){
            $inPK = false;
            foreach ($fk->getOriginColumns() as $myCol => $column){
                if (isset($pk[$myCol])){
                    $inPK = true;
                    unset($notUsedCols[$myCol]);
                }
            }
            if (!$inPK)
                unset($listFK[$idx]);
        }


        /*
         * S'il reste des colonnes de la PrimaryKey qui ne font référence à
         * aucune table, alors ce n'est pas une simple N..M
         */
        if ($notUsedCols)
            return false;

        /*
         * De même, ce n'est une N..M que si la PrimaryKey est composée de
         * 2 FK différentes, pas plus, pas moins.
         */
        if (count($listFK) !== 2)
            return false;

        $result = array();
        foreach ($listFK as $fk)
            $result[$fk->getFullName()] = $fk->getOriginTable();

        return $result;
    }

}

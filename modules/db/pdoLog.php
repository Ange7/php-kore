
    protected $_logStream;


    protected function _initializeAudit()
    {
        if( kore::$conf->db_queryFileLog )
            $this->enableLoggingToFile(kore::$conf->db_queryFileLog);
    }


    public function disableLogging()
    {
        if( $this->_logStream ){
            fclose($this->_logStream);
            $this->_logStream = null;
        }
    }

    public function enableLoggingToStream( $stream )
    {
        $this->disableLogging();
        $this->_logStream = $stream;
    }

    public function enableLoggingToFile( $fileName )
    {
        return $this->enableLoggingToStream(fopen($fileName, 'a'));
    }




    public function logQuery( $type, $reference, $statement )
    {
        if( $this->logging === NULL ) return;

        $line = date('Y/m/d H:i:s').' '.$type.' #'.$reference.' ';
        if( is_string($statement) ){
            $line .= trim(preg_replace("#\\s+#", ' ', $statement));
        } else {
            $line .= json_encode($statement);
        }
        $line .= PHP_EOL;
        fwrite($this->logging, $line);
    }


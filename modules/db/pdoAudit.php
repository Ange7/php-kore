<?php
/**
 * Audit des connexions PDO { @link kore_db_pdoAudit }.
 *
 * @package db
 */


/**
 * Etablie une connexion à une base de données via PDO, et
 * surveille le bon fonctionnement des diverses requêtes
 * exécutées.
 *
 * @package db
 */
class kore_db_pdoAudit extends kore_db_pdo
{
    /**
     * Canal servant à la communication avec kore_db_pdoAuditStatement.
     * Seule méthode trouvée afin d'éviter les références cycliques entre PDO
     * et PDOStatement.
     * @var array
     */
    public static $channel = array();

    public $benchQueries = true;
    public $slowConnections = 0.5;
    public $slowQueries = 0.5;
    public $launchExplain = true;

    protected $_statementClassName = 'kore_db_pdoAuditStatement';

    protected $_id;

    static protected $_loggingStream;


    static public function logQuery( $type, $reference, $statement, $time = 0 )
    {
        if (!self::$_loggingStream) return;

        $time *= 1000;

        $line = date('Y/m/d H:i:s')." $type #$reference {$time}ms ";
        if (is_string($statement)) {
            $line .= trim(preg_replace("#\\s+#", ' ', $statement));
        } else {
            $line .= json_encode($statement);
        }
        $line .= PHP_EOL;
        fwrite(self::$_loggingStream, $line);
    }

    /**
     * Instanciation de la classe.
     *
     * @link http://fr.php.net/manual/fr/function.PDO-construct.php
     *
     * @param string  $dsn
     * @param string  $user
     * @param string  $password
     * @param array   $options
     */
    public function __construct( $dsn, $user = NULL, $password = NULL, $options = NULL, $identifier = NULL )
    {
        static $idInstance = 0;
        $this->_id = ++$idInstance;

        $infos = new stdClass;
        $infos->id = $this->_id;
        $infos->identifier = $identifier;

        self::$channel[$this->_id] = $infos;

        if( ( $tmp = kore::$conf->db_benchQueries ) !== NULL )
            $this->benchQueries = (bool) $tmp;
        if( ( $tmp = kore::$conf->db_auditSlowConnections ) !== NULL )
            $this->slowConnections = $tmp;
        if( ( $tmp = kore::$conf->db_auditSlowQueries ) !== NULL )
            $this->slowQueries = $tmp;
        if( ( $tmp = kore::$conf->db_launchExplain ) !== NULL )
            $this->launchExplain = (bool) $tmp;

        $defaultOptions = array(
            PDO::ATTR_STATEMENT_CLASS => array($this->_statementClassName,
                    array(get_class($this), $this->_id)),
            );

        if( is_array($options) )
            $defaultOptions = $options + $defaultOptions;

        /**
         * Les connexions persistantes ne sont pas compatibles avec
         * PDO::ATTR_STATEMENT_CLASS . Nous sommes donc obligés de les
         * désactiver.
         */
        if( isset($defaultOptions[PDO::ATTR_PERSISTENT]) )
            unset($defaultOptions[PDO::ATTR_PERSISTENT]);


        if( $this->slowConnections )
            $startTime = kore::time();
        parent::__construct($dsn, $user, $password, $defaultOptions, $identifier);


        /**
         * Vérifie que la connexion soit bien établie
         * (en cas d'erreur, PDO détruit l'objet).
         */
        if( !isset($this) )
            return;

        if( $this->slowConnections ) {
            $connexionTime = bcsub(kore::time(), $startTime, 6);

            if ($connexionTime >= $this->slowConnections)
                kore::$error->track("slow connection",
                        kore_error::SEVERITY_DEBUG, array(
                        'connection time' => $connexionTime,
                        'connection time limit' => $this->slowConnections,
                        'DSN' => $dsn,
                        'USER' => $user,
                        'options' => $options ));
        }

        $this->_initializeAudit();
    }

    public function __destruct()
    {
        unset(self::$channel[$this->_id]);
        parent::__destruct();
    }

    public function getChannel()
    {
        return self::$channel[$this->_id];
    }

    /**
     *
     *
     */
    protected function _initializeAudit()
    {
        if (kore::$conf->db_queryFileLog and !self::$_loggingStream)
            self::$_loggingStream = fopen(kore::$conf->db_queryFileLog, 'a');
    }

    protected function _controlQuery($function, $statement, $params = null)
    {
        if( $params !== null ) {
            $this->_controlParams($function, $params);

            if( preg_match_all('#(:[a-zA-Z][a-zA-Z0-9]*)\\b#', $statement, $matches) ){
                $matches = array_count_values($matches[1]);
                foreach( $matches as $bindParameter => $nbOccurences ){
                    if ($nbOccurences > 1)
                        throw new InvalidArgumentException("parameter present multiple times in the statement [$bindParemeter]");
                }
            }
        }
        $error = false;

        if( strpos($statement, 'SQL_NO_CACHE') === false ){
            if( preg_match('#^\\s+#', $statement) )
                $error = 'queries should not start by spaces to be cacheable';
            elseif( preg_match('#^select.*curdate\\(\\s*\\)#i', $statement) )
                $error = 'queries should not use CURDATE() to be cacheable';
        }

        if ($error !== false)
            kore::$error->track("SQL: $error", kore_error::SEVERITY_NOTICE,
                    $statement);

        if (substr($statement, 0, 2) !== '--'){
            $backtrace = debug_backtrace();

            $caller = null;
            foreach($backtrace as $line){
                if (!empty($line['class'])){
                    if (substr($line['class'], 0, 8) === 'kore_db_')
                        continue;
                    $caller = $line['class'].'::'.$line['function'];
                } elseif (!empty($line['function'])) {
                    $caller = $line['function'];
                } else {
                    $caller = $line['file'];
                }
                break;
            }

            if ($caller)
                $statement = "-- $caller\n$statement";
        }

        return $statement;
    }

    protected function _controlParams($function, $params)
    {
        if (!is_array($params))
            kore::$error->track("SQL: params given to $function() must be an array",
                    null, $params);

        if (empty($params))
            kore::$error->track("SQL: $function() called with empty params");
    }

    protected function _controlQueryTime($function, $statement, $startTime, $reference = 0)
    {
        $time = bcsub(kore::time(), $startTime, 6);

        self::logQuery($function, $reference, $statement, $time);

        if ($time < $this->slowQueries) return;

        /**
         * Requête lente, à tracer.
         * TODO : à compléter
         */

    }


    public function exec( $statement )
    {
        $statement = $this->_controlQuery(__FUNCTION__, $statement);

        $bench = $this->_benchQuery(__FUNCTION__, $statement);
        $startTime = kore::time();
        $nbAffected = parent::exec($statement);
        $this->_controlQueryTime(__FUNCTION__, $statement, $startTime);

        return $nbAffected;
    }

    public function query( $statement, $mode = PDO::FETCH_ASSOC, $modeOpt = null, $ctorargs = null )
    {
        $statement = $this->_controlQuery(__FUNCTION__, $statement);

        $bench = $this->_benchQuery(__FUNCTION__, $statement);
        $startTime = kore::time();
        if ($ctorargs !== null)
            $stmt = parent::query($statement, $mode, $modeOpt, $ctorargs);
        elseif ($modeOpt !== null)
            $stmt = parent::query($statement, $mode, $modeOpt);
        else
            $stmt = parent::query($statement, $mode);

        $this->_controlQueryTime(__FUNCTION__, $statement, $startTime);

        return $this->_configureStatement($stmt, $startTime, $bench);
    }

    public function prepare( $statement, $options = array() )
    {
        $statement = $this->_controlQuery(__FUNCTION__, $statement);

        $bench = $this->_benchQuery(__FUNCTION__, $statement);
        $startTime = kore::time();
        if (!($stmt = parent::prepare($statement, $options)))
            return false;

        $this->_controlQueryTime(__FUNCTION__, $statement, $startTime,
                $stmt->getId());

        return $this->_configureStatement($stmt);
    }

    /**
     * Format the statement to allow better benchmark visibility
     *
     * @param  string  $statement
     * @return string
     */
    static public function buildBenchStatement($statement)
    {
        /*
         * Remove all spaces.
         */
        $statement = trim(preg_replace('#\\s+#', ' ', $statement));

        /*
         * Replace all strings and numbers by a "?" char.
         */
        $statement = preg_replace("#'[^']+'( ?, ?'[^']+')*#", '?', $statement);
        return preg_replace("#(\\b)[0-9]+(?:.[0-9]+)#", '$1?', $statement);
    }

    /**
     * Track the execution time of the query.
     *
     * @param  string $function
     * @param  string $statement
     * @return kore_bench
     */
    protected function _benchQuery($function, $statement)
    {
        if (!$this->benchQueries)
            return null;

        $class = 'db_'.$this->identifier.':'.$this->_id;
        $statement = $function.' '.self::buildBenchStatement($statement);

        return kore::$debug->benchInit($class, $statement);
    }


    protected function _configureStatement( $pdoStatement, $bench = null )
    {
        if( !$pdoStatement instanceOf kore_db_pdoAuditStatement )
            return $pdoStatement;

        if( $bench and !$pdoStatement->currentBench )
            $pdoStatement->currentBench = $bench;

        $pdoStatement->benchQueries = $this->benchQueries;


        return $pdoStatement;
    }

    public function beginTransaction()
    {
        self::logQuery('transaction', 0, __FUNCTION__);
        return parent::beginTransaction();
    }

    public function commit()
    {
        $startTime = kore::time();

        $res = parent::commit();

        $time = bcsub(kore::time(), $startTime, 6);
        self::logQuery('transaction', 0, __FUNCTION__, $time);
        return $res;
    }

    public function rollBack()
    {
        $startTime = kore::time();

        $res = parent::rollBack();

        $time = bcsub(kore::time(), $startTime, 6);
        self::logQuery('transaction', 0, __FUNCTION__, $time);
        return $res;
    }

}



/*














    /**
     * Crée un identifiant approximatif de la requête.
     *
     * @param  $string
     * @return $string
     * /
    static public function _buildQueryId( $string )
    {
        return strtolower(trim(preg_replace('#\\s+#', ' ', $string)));
    }

    private function _initStatement( $stmt )
    {
        $stmt->doAudit = $this->doAudit;
        $stmt->benchQueries = $this->benchQueries;

        if( $this->ignoreNextExplain )
             $stmt->forceExplain = false;
        else $stmt->forceExplain = $this->launchExplain;

        $stmt->skipExplainAlert = $this->skipExplainAlert;

        $this->ignoreNextExplain = false;
        $this->skipExplainAlert = false;

        $stmt->forceProfiling = $this->launchProfiling;
    }

    /**
     * Execute une requête et retourne un objet contenant le résultat.
     *
     * $params peut être passé afin de passer les paramètres d'entrée de
     * la requête.
     *
     * @param string  $statement
     * @param array   $params
     * @return kore_db_pdoAuditStatement
     * /
    public function query( $statement, $params = NULL )
    {
        if( is_array($params) and count($params) > 0 ) {
            $stmt = $this->prepare($statement, $params);
            if( ( $result = $stmt->execute($params) ) === false )
                return false;

        } else {

            $eventData = kore_db_pdoAuditStatement::_eventBeforeExecute();
            $stmt = parent::query($statement, $params);
            if( $stmt === false ) return false;

            kore_db_pdoAuditStatement::_eventAfterExecute($eventData, $stmt, $this);
        }

        return $stmt;
    }

    /**
     * Prépare une requete à l'éxécution.
     *
     * @param string  $statement
     * @param array   $options
     * @return kore_db_pdoAuditStatement
     * /
    public function prepare( $statement, $options = array() )
    {
        $stmt = parent::prepare($statement, $options);
        if( $stmt === false ) return false;

        $this->_initStatement($stmt);


        return $stmt;
    }

    /**
     * Appelle vraiment la méthode lastInsertId() de PDO.
     *
     * @return integer
     * /
    public function realLastInsertId()
    {
        $this->lastInsertId = parent::lastInsertId();
    }

    /**
     * Retourne la valeur enregistrée comme "lastInsertId"
     *
     * @param  string  $name
     * @return integer
     * /
    public function lastInsertId( $name = NULL )
    {
        return $this->lastInsertId;
    }

    /**
     * Lance le profiling de la requête.
     *
     * @return array
     * /
    public function doProfiling()
    {
        $profilingResult = array();

        return $profilingResult ;
    }

    /**
     * Lance l'explain de la requête.
     *
     * @param  string  $queryString
     * @param  array   $params
     * @return array
     * /
    public function doExplain( $queryString, $params )
    {
        $explain = $this->getExplainQuery($queryString, $params);
        if( ! $explain ) return false;

        $explainResult = false;

        if( ( $result = $this->query($explain->query, $explain->params) ) !== false ) {
            $explainResult = array(
                'alert' => false,
                'list'  => $result->fetchAll( PDO::FETCH_OBJ ),
                );
        }

        return $explainResult;
    }

    /**
     * Formate un résultat d'explain.
     *
     * @param  array  $explainList
     * @return string
     * /
    public function formatExplain( $explainList )
    {
        if(!is_array($explainList)) return $explainList;

        $cols = array();

        foreach( $explainList as $row ) {
            foreach( $row as $colName => $value ){
                $len = strlen( $value );
                if( !isset( $cols[ $colName ] ) )
                    $cols[ $colName ] = strlen( $colName );

                if( $len > $cols[ $colName ] )
                    $cols[ $colName ] = $len;
            }
        }

        $lineSep = '+';
        foreach( $cols as $colSize )
            $lineSep .= str_repeat( '-', 2 + $colSize ) . '+';
        $lineSep .= PHP_EOL;

        $output = $lineSep;
        $output .= '|';
        foreach( $cols as $colName => $colSize )
            $output .= ' '.str_pad( $colName, $colSize ).' |';
        $output .= PHP_EOL;

        $output .= $lineSep;

        foreach( $explainList as $row ) {
            $output .= '|';
            foreach( $row as $colName => $value ){
                $colSize = $cols[$colName];
                $output .= ' '.str_pad( $value, $colSize ).' |';
            }
            $output .= PHP_EOL;
        }
        $output .= $lineSep;

        return $output;
    }

    /**
     * Génère une requête "explain" pour la requête donnée,
     * quand c'est possible.
     *
     * @param  string  $queryString
     * @param  array   $params
     * @return object
     * /
    protected function getExplainQuery( $queryString, $params )
    {
        $able = false;

        $newQueryString = trim(preg_replace('#\\s+#', ' ', $queryString));
        $newParams = $params ;

        if( preg_match( '#^select #i', $newQueryString ) ) {
            $able = true;
        } elseif( preg_match( '#^(insert|replace)( ignore| delayed)? into (`[^`]+`|\\S+)( \\([^)]+\\))? ([ (]*select.*)(on\\s+duplicate\\s+key\\s+update.*)?$#iU', $newQueryString, $match ) ) {
            $able = true;
            $newQueryString = trim( $match[5] );
        } elseif( preg_match( '#^create( temporary)? table .* as ([ (]*select .*)$#i', $newQueryString, $match ) ) {
            $newQueryString = trim( $match[2] );
            $able = true;
        } elseif( preg_match( '#update (\\S+) set (.*) (where.*)#i', $newQueryString, $match ) ) {
            $newQueryString = "select * from {$match[1]} {$match[3]}";

            $nbArgs = substr_count( $newQueryString, '?' );
            if( $nbArgs < count( $newParams ) and is_int(key($newParams)) ) {
                $newParams = array_slice( $newParams, -1 * $nbArgs );
            }
            $able = true;
        }

        if( ! $able ) return false;

        /**
         * Corrige les eventuels problèmes de paramêtres bindés.
         * /
        if( count($newParams) > 0 and !is_int(key($newParams)) ) {
            $usedArgs = array();
            if( preg_match_all( '#:(\\w+)#', $newQueryString, $match ) ) {
                $usedArgs = array_flip( $match[1] );
            }

            foreach( $newParams as $key => $value ){
                if( substr($key, 0, 1) === ':' )
                    $name = substr($key, 1);
                else $name = $key;

                if(!isset($usedArgs[$name]))
                    unset($newParams[$key]);
            }
        }

        if( $this->explainPrefix !== NULL )
            $prefix = $this->explainPrefix;
        else {
            switch( $this->getAttribute(PDO::ATTR_DRIVER_NAME) ){
                case 'mysql':  $prefix = 'explain extended '; break;
                case 'sqlite': $prefix = 'explain query plan '; break;
                default:
                    $prefix = 'explain ';
            }
        }

        $obj = new stdClass();
        $obj->query = $prefix . $newQueryString;
        $obj->params = $newParams;

        return $obj;
    }

}

*/

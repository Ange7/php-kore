<?php

/**
 * Classe de gestion des différentes connexions SQL.
 *
 * @see kore
 * @see kore_dbLoader
 *
 * @package db
 */
class kore_db_config
{
    /**
     * Liste des connexions actives.
     *
     * @var array
     */
    protected $_list = array();

    /**
     * Liste d'objets à conserver tant que les bases sont ouvertes.
     *
     * @var array
     */
    protected $_keepedPool = array();

    /**
     * Indique si un closeAll() a déjà été fait ou non.
     *
     * @var boolean
     */
    protected $_shouldBeClosed = false;

    /**
     * Liste d'instances ayant déclenchées un warning à cause du closeAll.
     *
     * @var array
     */
    protected $_closeAllWarning = array();

    /**
     * Handle some checks and cleanup on destruction.
     *
     */
    public function __destruct()
    {
        $this->closeAll();

        /*
         * During the shutdown process, we really don't care about destruction
         * order.
         */
        $this->_shouldBeClosed = false;
    }

    /**
     * Retourne les options PDO par défaut.
     *
     * @param  string $driver
     * @return array
     */
    public function getDefaultOptions( $driver )
    {
        $dbOptions = array(PDO::ATTR_EMULATE_PREPARES => false);

        if ($driver === 'mysql'){
            $dbOptions[PDO::MYSQL_ATTR_DIRECT_QUERY] = false;
            $dbOptions[PDO::MYSQL_ATTR_USE_BUFFERED_QUERY] = false;
        }

        /*
         * En mode CGI il est généralement intéressant d'utiliser des connexions
         * persistentes.
         */
        if (kore::$conf->sapiName === 'cgi')
            $dbOptions[PDO::ATTR_PERSISTENT] = true;

        return $dbOptions;
    }

    /**
     * Essaye d'utiliser le fichier de configuration MySQL (~/.my.cnf) pour
     * obtenir les paramètres de connexion à la base de données.
     *
     * @param  array $dbOptions
     * @return array
     */
    public function addOption_mysqlUserConf($dbOptions = array())
    {
        /*
         * MySQLnd doesn't handle .cnf files. So do NOT try to use this
         * feature if not available.
         */
        if (!defined('PDO::MYSQL_ATTR_READ_DEFAULT_FILE'))
            return $dbOptions;

        if ($userInfos = posix_getpwuid(posix_getuid())){
            $confPath = $userInfos['dir'].'/.my.cnf';
            if (file_exists($confPath))
                $dbOptions[PDO::MYSQL_ATTR_READ_DEFAULT_FILE] = $confPath;
        }

        $dbOptions[PDO::MYSQL_ATTR_READ_DEFAULT_GROUP] = 'php';

        return $dbOptions;
    }

    /**
     * Retourne le nom du profil à utiliser en fonction du nom d'une classe.
     * Entre autre utilisée par kore_db_mapping_element et kore_db_mapping_list
     *
     * @param  string $className
     * @param  string $mode
     * @return string
     */
    public function getProfilNameFromClassName( $className, $mode = 'w' )
    {
        return 'default';
    }

    /**
     * Ouvre une connexion SQL selon le profil $dbProfil.
     *
     * Méthode à surcharger pour permettre la configuration des différents
     * profils de connexion aux bases de données.
     *
     * @param  string  $dbProfil
     * @return kore_db_pdo
     */
    public function newInstance( $dbProfil )
    {
        $dbOptions = $this->addOption_mysqlUserConf(
                $this->getDefaultOptions('mysql'));

        return new kore_db_pdo('mysql::host=localhost:dbname='.$dbProfil, null,
                null, $dbOptions, $dbProfil);
    }

    /**
     * Retourne la connexion SQL correspondant au profil $dbProfil.
     * Si elle ci n'est pas encore ouverte, alors
     * {@link kore_db_config::newInstance()} est appelée.
     *
     * @param  string $dbProfil
     * @return kore_db_pdo
     */
    public function instance( $dbProfil )
    {
        return $this->$dbProfil;
    }

    public function __get( $dbProfil )
    {
        $bench = kore::$debug->benchInit('db', 'instance');
        $bench->setFinalStatus($dbProfil);

        /**
         * Si une instance est recrée après un appel à closeAll, trace un
         * warning.
         */
        if( $this->_shouldBeClosed and ! kore::$conf->db_allowReconnection ){

            if (kore::$conf->db_stopOnReconnection)
                throw new LogicException("trying to reconnect to [$dbProfil] after closeAll()");

            if (!isset($this->_closeAllWarning[$dbProfil])){
                kore::$error->track("reconnection to [$dbProfil] after closeAll()",
                        kore_error::SEVERITY_INFO);
                $this->_closeAllWarning[$dbProfil] = true;
            }
        }

        $this->$dbProfil = NULL;

        if ( ( $instance = $this->newInstance($dbProfil) ) !== NULL ) {

            $this->_list[$dbProfil] = true;

        } else {

            $this->_list[$dbProfil] = false;

            $instance = new kore_db_dummy();
        }

        $this->$dbProfil = $instance;
        return $instance;
    }

    /**
     * Indique si la connexion $dbProfil est active.
     *
     * @param  string  $dbProfil
     * @return boolean
     */
    public function isActive( $dbProfil )
    {
        return ( isset($this->_list[$dbProfil]) and
                 $this->_list[$dbProfil] !== false );
    }

    /**
     * Cloture une connexion SQL.
     *
     * Remarque : il s'agit ici de détruire uniquement l'instance de la classe
     * afin que le destructeur soit appelé ; toutefois s'il reste des références
     * de cette instance en mémoire, ce destructeur ne sera pas appelé pour
     * autant.
     *
     * @see kore_db_config::closeAll()
     * @param string $dbProfil
     */
    public function close( $dbProfil )
    {
        if ( isset($this->_keepedPool[$dbProfil]) )
            unset($this->_keepedPool[$dbProfil]);

        if ( isset($this->_list[$dbProfil]) ) {
            if (method_exists($this->_list[$dbProfil], 'close'))
                $this->_list[$dbProfil]->close();

            unset($this->_list[$dbProfil]);
            unset($this->$dbProfil);
        }
    }

    /**
     * Cloture toutes les connexions SQL.
     *
     * Remarque : il s'agit ici de détruire uniquement les instances des classes
     * afin que les destructeurs soient appelés ; toutefois s'il reste des
     * références de ces instances en mémoire, les destructeurs ne seront pas
     * appelés pour autant.
     *
     * @see kore_db_config::close()
     */
    public function closeAll()
    {
        // Purge le cache d'instance de kore_db_schema_*
        if(class_exists('kore_db_schema_schema', false))
            kore_db_schema_schema::purgeInstanceCache();

        // Detruit toutes les références
        $this->_keepedPool = array();

        // Détruit toutes les instances actives
        foreach( $this->_list as $dbProfil => $tmp ) {
            $this->$dbProfil->_close();
            unset($this->$dbProfil);
        }
        $this->_list = array();

        // A partir de là toutes les instances devraient être détruites.
        $this->_shouldBeClosed = true;
    }

    /**
     * Retourne la liste des instances actuelles.
     *
     * @return array
     */
    public function instanceList()
    {
        return array_keys($this->_list);
    }

    /**
     * Conserve en mémoire une référence d'un objet, et l'associe éventuellement
     * à une instance de base de données.
     *
     * Le but étant que le destructeur de l'object $object soit appelé juste
     * avant la fermeture de la connexion $dbProfil. Cela permet de différer
     * certains traitements non prioritaires.
     */
    public function keepReference( $object, $dbProfil = NULL )
    {
        if( !isset($this->_keepedPool[$dbProfil]) )
            $this->_keepedPool[$dbProfil] = array();

        array_push($this->_keepedPool[$dbProfil], $object);
    }

    /**
     * Indique si un closeAll() a déjà été fait ou non.
     *
     * @param  $newStatus
     * @return boolean
     */
    public function shouldBeClosed( $newStatus = null )
    {
        $currentState = $this->_shouldBeClosed;

        if ($newStatus !== null)
            $this->_shouldBeClosed = (bool) $newStatus;

        return $currentState;
    }

    /**
     * Trace la fermeture d'une instance.
     *
     * @param  $dbProfil
     */
    public function eventClosed($dbProfil)
    {
        if ($this->_shouldBeClosed and !kore::$conf->db_allowReconnection and
            !isset($this->_closeAllWarning[$dbProfil])){

            kore::$error->track("connection [$dbProfil] was not closed by closeAll()");
            $this->_closeAllWarning[$dbProfil] = true;
        }
    }

    protected function tryConnect($listDSN, $user, $pass, $options, $class = 'kore_db_pdo')
    {
        if(!is_array($listDSN)) $listDSN = array($listDSN);

        $instance = null;
        $pdoMessage = 'unknown error'; $pdoCode = 1;
        foreach ($listDSN as $DSN){
            try {
                $instance = new $class($DSN, $user, $pass, $options);
                break;

            } catch ( PDOException $e ) {
                $pdoMessage = $e->getMessage();
                $pdoCode = $e->getCode();

                kore::$error->track('PDO (connection) : '.$pdoMessage,
                        kore_error::SEVERITY_INFO,
                        array('listDSN' => $listDSN, 'errorDSN' => $DSN));
            }
        }

        if( $instance === null )
            throw new PDOException($pdoMessage, $pdoCode);

        return $instance;
    }
}

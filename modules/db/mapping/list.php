<?php

class kore_db_mapping_list extends kore_collection
{
    /**
     * Classe commune à tous les objets de la collection.
     */
    const CLASS_ITEM = 'kore_db_mapping_element';

    /**
     * Colonne contenant le nom de la classe. Si renseignée fetchAll() sera
     * appelée avec PDO::FETCH_CLASSTYPE.
     */
    const CLASS_IN_COLUMN = '';

    /**
     * Retourne le nom et l'alias de la table associée à la classe.
     *
     * @return array
     */
    static protected function _getTable()
    {
        $classItem = static::CLASS_ITEM;

        return array($classItem::TABLE_NAME, $classItem::TABLE_ALIAS);
    }

    /**
     * Retourne les condition d'une requête en fonction des filtres passés en
     * paramètre. Étant donné qu'on utilise une requête paramétrée, le résultat
     * est sous la forme [ $query, $params ]
     *
     * @param  array $filters
     * @return array
     */
    static protected function _getFiltersQuery(kore_db_pdo $db, array $filters = null)
    {
        $query = '';
        $params = array();

        /*
         * Applique les éventuels filtres.
         */
        if ($filters) {
            $count = 0;
            foreach ($filters as $colName => $value){
                if ($count++ === 0)
                    $query .= "\nwhere ";
                else
                    $query .= "\nand ";

                $query .= $db->quoteKeyword($colName);
                if ($value === null) {
                    $query .= ' is null';
                } else {
                    $paramName = 'FILTER'.$count;
                    $query .= ' = :'.$paramName;
                    $params[$paramName] = $value;
                }
            }
        }

        return array($query, $params);
    }

    /**
     * Retourne la liste des colonnes à utiliser pour initialiser la collection.
     *
     * @return string
     */
    static protected function _getDefaultCols()
    {
        $classItem = static::CLASS_ITEM;

        if (!static::CLASS_IN_COLUMN)
            return $classItem::MAIN_COLS;

        return static::CLASS_IN_COLUMN.' as _className, '.$classItem::MAIN_COLS;
    }

    /**
     * Retourne la liste de tous les éléments correspondant aux filtres, et en
     * tenant éventuellement compte de la pagination.
     *
     * @param  array $filters
     * @param  mixed $sort
     * @param  kore_pagination $pagination
     * @return kore_db_mapping_list
     */
    static public function getAll(array $filters = null, $sort = null, kore_pagination $pagination = null)
    {
        $classItem = static::CLASS_ITEM;

        $list = new static;
        $db = $list->_getReadDb();

        list($filters, $params) = static::_getFiltersQuery($db, $filters);

        $baseQuery = "from ".$classItem::getFullTableName($db)."\n$filters";

        $query = $baseQuery;
        if ($sort)
            $query .= "\n" . kore_db_sort::fromMixed($sort, $db);

        if ($pagination) {
            if ($row = $db->selectFirst("select count(*) as nb ".$baseQuery, $params)) {
                $pagination->registerNumberOfItems($row['nb']);
                $query .= $pagination->getSQLLimit();
            }
        }

        if ($stmt = $db->keepPrepare("select ".static::_getDefaultCols()." ".$query))
            $list->_fetchStatement($stmt, $params);
        return $list;
    }

    /**
     * Retourne le nombre d'enregistrements correspondant aux filtres.
     *
     * @param  array $filters
     * @return number
     */
    static public function countAll(array $filters = null)
    {
        $classItem = static::CLASS_ITEM;

        $list = new static;
        $db = $list->_getReadDb();

        list($filters, $params) = static::_getFiltersQuery($db, $filters);

        $query = "from ".$classItem::getFullTableName($db)."\n$filters";

        if ($row = $db->selectFirst("select count(*) as nb ".$query, $params))
            return (int) $row['nb'];
        return false;
    }

    /**
     * Return an instance of the current class, then fetch a query.
     *
     * @param  string $query
     * @param  array $params
     * @return kore_db_mapping_list
     */
    static protected function _instanciateFromQuery($query, array $params = null)
    {
        $collection = new static;

        if ($params){
            if ($stmt = $collection->_getReadDb()->keepPrepare($query))
                $collection->_fetchStatement($stmt, $params);
        } else {
            $collection->_fetchQuery($query);
        }

        return $collection;
    }

    /**
     * Renvoi la connexion vers la base de données, dans le but d'y écrire des
     * données.
     *
     * @return kore_db_pdo
     */
    protected function _getWriteDb()
    {
        return kore::$db->{kore::$db->getProfilNameFromClassName(get_class(
                $this), 'w')};
    }

    /**
     * Renvoi la connexion vers la base de données, dans le but d'y lire des
     * données.
     *
     * @return kore_db_pdo
     */
    protected function _getReadDb()
    {
        /*
         * Ugly hack : jusqu'à maintenant _getReadDb() faisait appel à
         * _getWriteDb() si non surchargée.
         *
         * On *doit* conserver ce comportement par compatibilité, mais on veut
         * aussi avoir la possibilité d'utiliser la configuration centralisée
         * via getProfilNameFromClassName().
         *
         * La solution adoptée est donc la suivante : si la méthode
         * getProfilNameFromClassName() retourne 'default' on suppose qu'elle
         * n'est pas exploitée et que par conséquent on souhaite utiliser
         * l'ancien mécanisme, c'est à dire _getWriteDb().
         */
        $dbProfil = kore::$db->getProfilNameFromClassName(get_class($this), 'r');

        if ($dbProfil === 'default')
            return $this->_getWriteDb();

        return kore::$db->$dbProfil;
    }

    /**
     * Importe le résultat d'une requête dans la collection, et retourne le
     * nombre de lignes trouvées.
     *
     * @param  string $query
     * @param  array  $inputParameters
     * @return integer
     */
    protected function _fetchQuery($query, array $inputParameters = null)
    {
        if ($inputParameters !== null){
            if (!($stmt = $this->_getReadDb()->prepare($query)))
                return false;

            return $this->_fetchStatement($stmt, $inputParameters);
        }

        $res = $this->_getReadDb()->query($query);
        if (!$res)
            return false;

        return $this->_fetchAll($res);
    }

    /**
     * Importe le résultat d'un PDOStatement dans la collection, et retourne le
     * nombre de lignes trouvées.
     *
     * @param  PDOStatement $stmt
     * @param  array  $inputParameters
     * @return integer
     */
    protected function _fetchStatement(PDOStatement $stmt, array $inputParameters = null)
    {
        if (!$stmt->execute($inputParameters))
            return false;

        return $this->_fetchAll($stmt);
    }

    /**
     * Importe un PDOStatement déjà exécuté.
     *
     * @param  PDOStatement $stmt
     * @return integer
     */
    protected function _fetchAll(PDOStatement $stmt)
    {
        /*
         * Ici on utilise fetchAll afin de pouvoir profiter du FETCH_CLASS
         */
        $count = 0;

        if (static::CLASS_IN_COLUMN)
            $fetchStyle = PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE;
        else
            $fetchStyle = PDO::FETCH_CLASS;

        foreach($stmt->fetchAll($fetchStyle, static::CLASS_ITEM) as $item){
            $count++;
            $this->attach($item);
        }
        return $count;
    }

    /**
     * Importe le résultat d'un PDOStatement dans la collection, ainsi que des
     * données liées, puis retourne le nombre de lignes trouvées.
     *
     * @param  PDOStatement|string $stmt
     * @param  array  $inputParameters
     * @param  array  $keys
     * @param  array  $cols
     * @param  callable $dataCallback
     * @return integer
     */
    protected function _fetchRelation($stmt, array $inputParameters = null, $keys, array $cols, $dataCallback = null)
    {
        if(!($stmt instanceOf PDOStatement)) {
            if (!($stmt = $this->_getReadDb()->prepare($stmt)))
                return false;
        }

        if (!$stmt->execute($inputParameters))
            return false;

        $keys = (array) $keys;

        $callback = array(static::CLASS_ITEM, 'fromId');

        $count = 0;
        $relationData = null;
        $haveCols = (count($cols) > 0);
        foreach ($stmt as $row){
            /*
             * Construit la clé.
             */

            $k = array();
            foreach ($keys as $key) {
                $k[$key] = $row[$key];
                unset($row[$key]);
            }

            /*
             * Extrait les données portées par la relation.
             */
            if ($haveCols === true){
                if ($dataCallback !== null) {
                    $relationData = call_user_func($dataCallback, $row);
                } else {
                    $relationData = array();
                    foreach($cols as $colName){
                        $relationData[$colName] = $row[$colName];
                        unset($row[$colName]);
                    }
                    $relationData = $this->_fetchRelationData($relationData);
                }
            }

            /*
             * Instancie l'objet.
             */
            if (count($k) > 1)
                $object = call_user_func_array($callback, [$k]);
            else
                $object = call_user_func_array($callback, $k);
            $object->prefetchData($row, true);

            /*
             * Ajoute l'objet à la collection.
             */
            $this->attach($object, $relationData);
        }

        return $count;
    }

    /**
     * Construit les données qui seront portées par la relation.
     *
     * @see _fetchRelation
     * @param  array $row
     * @return mixed
     */
    protected function _fetchRelationData($row)
    {
        return (object) $row;
    }

    /**
     * Importe dans la collection des propriétés issues d'un SELECT SQL.
     *
     * @param  string  $query
     * @param  array   $params
     * @param  string  $colKey      nom de la colonne du select dans la valeur servirade clé dans $instances.
     * @param  boolean $setExists   indique si l'objet doit être marqué comme existant.
     * @param  kore_db_pdo $db
     * @return integer nombre de lignes renvoyées par le select.
     */
    protected function _fetchQueryInCurrent( $query, $params, $colKey, $setExists = false, kore_db_pdo $db = null )
    {
        if(!$db) $db = $this->_getReadDb();

        if ($isMultiKey = (strpos($colKey, ',') !== false))
            $aKey = explode(',', $colKey);

        $nbLines = 0;
        if (isset($params))
            $res = $db->q($query, $params);
        else
            $res = $db->query($query);

        if ($setExists)
            $exists = true;
        else
            $exists = null;

        if ($res !== false) {
            $res = $res->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res as $row) {
                ++$nbLines;

                if ($isMultiKey === false){
                    $instanceId = $row[$colKey];
                } else {
                    $instanceId = '';
                    foreach ($aKey as $colName)
                        $instanceId .= $row[$colName].'|';
                }

                if(!isset($this->_list[$instanceId]))
                    continue;

                $instance = $this->_list[$instanceId];
                $instance->prefetchData($row, $exists);
            }
        }

        return $nbLines;
    }

    /**
     * Tente de charger un grand nombre d'objet simultanément.
     *
     * @param  string  $key
     * @param  string  $query
     * @param  array   $params
     * @param  integer $maxItem
     * @param  string  $inListFct
     * @param  boolean $setExists
     * @param  kore_db_pdo $db
     * @return integer
     */
    public function prefetchQuery( $key, $query, $params = null, $maxItem = 255, $inListFct = null, $setExists = false, kore_db_pdo $db = null )
    {
        if (!$db) $db = $this->_getReadDb();

        $queries = $db->buildInList(array_keys($this->_list), $maxItem, $inListFct);

        $nbFetched = 0;

        foreach ($queries as $in){
            $q = sprintf($query, $in);
            $p = $params;

            $nbFetched += $this->_fetchQueryInCurrent($q, $p, $key, $setExists, $db);
        }

        return $nbFetched;
    }

    /**
     * Précharge directement les MAIN_COLS d'une collection.
     *
     * @param  kore_db_pdo $db
     * @return number
     */
    public function prefetchMainCols(kore_db_pdo $db = null)
    {
        if (!$db) $db = $this->_getReadDb();

        $class = static::CLASS_ITEM;
        $cols  = static::_getDefaultCols();
        $key   = $class::PRIMARY_KEY;

        $baseQuery = "select ".$cols." from ".$class::getFullTableName($db).
                "\nwhere ";

        $nbFetched = 0;
        foreach ($this->asClauses($db) as $clause){
            $query = $baseQuery . $clause;

            $nbFetched += $this->_fetchQueryInCurrent(
                    $query, null, $key, true, $db);
        }
        return $nbFetched;
    }

    /**
     * Convert the current collection in SQL clauses.
     *
     * @param  kore_db_pdo $db
     * @return array
     */
    public function asClauses(kore_db_pdo $db = null)
    {
        if (!$db) $db = $this->_getReadDb();

        $clauses = array();

        $class = static::CLASS_ITEM;
        $key   = $class::PRIMARY_KEY;

        if (strpos($key, ',') === false){
            $query = $db->quoteKeyword($key) . ' in (%s)';

            foreach ($db->buildInList(array_keys($this->_list), 255) as $in)
                $clauses[] = sprintf($query, $in);

        } else {
            $idxClauses = 0;
            $clauses[$idxClauses] = '';
            $nbItemInClause = 0;
            $limit = 100;

            $aKey = array();
            foreach (explode(',', $key) as $colName)
                $aKey[$colName] = $db->quoteKeyword($colName);

            foreach ($this->_list as $item){
                if ($nbItemInClause++)
                    $clauses[$idxClauses] .= "\n\tor ";
                elseif (!isset($clauses[$idxClauses]))
                    $clauses[$idxClauses] = '';

                $cond = '';
                foreach ($aKey as $colName => $escapedCol){
                    if ($cond !== '') $cond .= ' and ';
                    $cond .= $escapedCol .'='. $db->quoteValue($item->$colName);
                }

                $clauses[$idxClauses] .= "($cond)";
                if ($nbItemInClause >= $limit){
                    ++$idxClauses;
                    $nbItemInClause = 0;
                }
            }

            foreach ($clauses as $idx => $clause)
                $clauses[$idx] = "($clause)";
        }

        return $clauses;
    }

}

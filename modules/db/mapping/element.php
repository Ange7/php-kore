<?php
/**
 *
 *
 */

/**
 *
 */
abstract class kore_db_mapping_element
{
    static private $_cacheProperties = array();
    static private $_cacheInstances  = array();

    const TABLE_NAME = null;
    const TABLE_ALIAS = null;
    const PRIMARY_KEY = 'id';

    const MAIN_COLS = '';
    const CLASS_COLLECTION = 'kore_db_mapping_list';

    protected $_exists;

    public    $_originalData = array();
    protected $_modifiedData;

    /**
     * Retourne le nom de la table, tel qu'il devrait être utilisé dans un FROM.
     *
     * @param  kore_db_pdo $db
     * @return string
     */
    static public function getFullTableName($db)
    {
        $t = $db->quoteKeyword(static::TABLE_NAME);
        if (static::TABLE_ALIAS)
            $t .= ' '.$db->quoteKeyword(static::TABLE_ALIAS);
        return $t;
    }

    /**
     * Retourne le nom faisant référence à la table dans le cas où
     * getFullTableName() aurait été utilisé.
     *
     * @param  kore_db_pdo $db
     * @return string
     */
    static public function getTableAlias($db)
    {
        if (static::TABLE_ALIAS)
            return $db->quoteKeyword(static::TABLE_ALIAS);
        return $db->quoteKeyword(static::TABLE_NAME);
    }

    /**
     * Retourne la liste des propriétés de la classe.
     *
     * Note : le résultat est placé en cache durant toute l'exécution du script.
     *
     * @return array
     */
    static public function getProperties()
    {
        $class = get_called_class();
        if (!isset(self::$_cacheProperties[$class]))
            self::$_cacheProperties[$class] = static::_getProperties();

        return self::$_cacheProperties[$class];
    }

    /**
     * Retourne la liste des propriétés de la classe associée au commentaire
     * défini en base de données, le tout de manière automatique.
     *
     * @return array
     */
    static public function getDocProperties()
    {
        $obj = new static;
        $schema = kore_db_schema_schema::fromCurrent($obj->_getReadDb());
        $table = $schema->getTable(static::TABLE_NAME);

        $props = array();
        $columns = $table->getColumns();
        foreach($columns as $columnName => $column) {
            $column->fetchProperties(array('COLUMN_COMMENT'));
            $props[$columnName] = $column->comment;
        }

        return $props;
    }


    /**
     * Retourne une propriété de la classe.
     *
     * @param  string $name
     * @return stdClass
     * @throws DomainException
     */
    static public function getProperty($name)
    {
        $properties = static::getProperties();

        if (!isset($properties[$name]))
            throw new DomainException("unknown property [$name] in class ".get_called_class());

        return $properties[$name];
    }

    /**
     * Retourne le commentaire d'une propriété de la classe.
     *
     * @param  string $name
     * @return string
     * @throws DomainException
     */
    static public function getDocProperty($name)
    {
        $properties = static::getDocProperties();

        if (!isset($properties[$name]))
            throw new DomainException("unknown documentary property [$name] in class ".get_called_class());

        return $properties[$name];

    }

    /**
     * Retourne la liste des propriétés de la classe.
     * Par défaut kore_db_mapping_element retrouvera de lui même les priorités
     * de la table, toutefois pour des raisons de performance il est préférable
     * de surcharger cette méthode afin de définir soit même ces propriétés.
     *
     * @return array
     */
    static protected function _getProperties()
    {
        $obj = new static;
        $schema = kore_db_schema_schema::fromCurrent($obj->_getReadDb());
        $table = $schema->getTable(static::TABLE_NAME);

        return kore_db_mapping_auto::detectProperties($table);
    }

    /**
     * Retourne une instance existante d'un objet à partir de son identifiant,
     * ou bien la crée.
     *
     * @param  integer/string $key
     * @param  boolean $isNewInstance
     * @return kore_db_mapping_element
     */
    static protected function _deduplicatedInstance($key, &$isNewInstance)
    {
        $class = get_called_class();
        if (isset(self::$_cacheInstances[$class][$key])) {
            $isNewInstance = false;
            return self::$_cacheInstances[$class][$key];
        }

        $isNewInstance = true;

        if (!isset(self::$_cacheInstances[$class]))
            self::$_cacheInstances[$class] = array();

        $obj = new $class();
        self::$_cacheInstances[$class][$key] = $obj;

        return $obj;
    }

    /**
     * Register an object in cache, but only if it where not already present.
     *
     * @param  string $key
     * @param  kore_db_mapping_element $instance
     * @return boolean
     */
    static protected function _registerInstance($key, kore_db_mapping_element $instance)
    {
        $class = get_called_class();
        if (isset(self::$_cacheInstances[$class][$key]))
            return false;

        self::$_cacheInstances[$class][$key] = $instance;
        return true;
    }

    /**
     * Cleanup internal properties cache
     *
     * @param boolean $allClasses
     */
    static public function purgePropertiesCache($allClasses = false)
    {
        if($allClasses) {
            self::$_cacheProperties = array();
        } else {
            $class = get_called_class();
            if(isset(self::$_cacheProperties[$class]))
                unset(self::$_cacheProperties[$class]);
        }
    }

    /**
     * Cleanup internal instance cache
     *
     * @param boolean $allClasses
     */
    static public function purgeInstanceCache($allClasses = false)
    {
        if($allClasses)
            self::$_cacheInstances = array();
        else self::$_cacheInstances[get_called_class()] = array();
    }

    /**
     * Return current contents of the instance cache, for debugging purpose.
     *
     * @param  string $classMask
     * @return array
     */
    static public function extractInstanceCache($classMask = null)
    {
        if ($classMask === null)
            $classMask = get_called_class();

        $output = array();
        foreach (self::$_cacheInstances as $className => $cache){
            if (fnmatch($classMask, $className))
                $output[$className] = $cache;
        }

        return $output;
    }

    /**
     * Indique si les instances de la classe courante sont à conserver en
     * mémoire ou non.
     * Par défaut ce comportement n'est actif que si PHP est utilisé via HTTP et
     * kore::$conf->response_autoCloseDB est activé.
     *
     * Pour forcer ce comportement, il suffit de définir
     * kore::$conf->db_mapping_keepInMemory à true ou false.
     *
     * @return boolean
     */
    static public function keptInMemory()
    {
        return kore::$conf->get('db_mapping_keepInMemory',
                (kore::$conf->response_autoCloseDB !== false and
                        kore::$conf->sapiName !== 'cli'));
    }

    /**
     * Crée une instance de l'objet, à partir de son identifiant.
     *
     * @param  mixed  $id
     * @return kore_db_mapping_element
     */
    static public function fromId( $id )
    {
        if (static::keptInMemory()) {
            $obj = static::_deduplicatedInstance($id, $isNewInstance);
        } else {
            $obj = new static;
            $isNewInstance = true;
        }

        if ($isNewInstance){
            $p = static::getProperty(static::PRIMARY_KEY);
            $obj->_originalData[static::PRIMARY_KEY] = $obj->_castToProperty(
                    $id, $p->cast);
        }

        return $obj;
    }


    /**
     * Instanciation de l'objet, non publique afin de forcer le passage par des
     * "factories" comme le fromId() ci dessus.
     */
    protected function __construct()
    {
        /*
         * Prise en charge du fetchAll(PDO::FETCH_CLASS)
         */
        if (isset($this->_modifiedData)) {
            $data = $this->_modifiedData;
            $this->_modifiedData = null;
            $this->prefetchData($data, true);
        }
    }

    /**
     * Converti l'objet courant en chaine de caractère, pour débugage.
     *
     * @return string
     */
    public function __toString()
    {
        $string = 'object '.get_class($this);

        /*
         * En production on n'affiche rien d'autre, par précaution.
         */
        if (kore::$debug->getEnvironment() === 'dev'){
            $string .= PHP_EOL;
            foreach (static::getProperties() as $name => $p){
                $n = $name;
                if ($p->cast)
                    $n .= " ({$p->cast})";
                elseif (isset($p->rel))
                    $n .= " ({$p->rel})";

                $string .= "\t".str_pad($n, 32, ' ')."\t";

                if (!isset($this->$name))
                    $string .= "is not set";
                else {
                    if (is_scalar($this->$name))
                        $string .= '('.gettype($this->$name).') '.$this->$name;
                    elseif (is_object($this->$name))
                        $string .= get_class($this->$name);
                    else
                        $string .= gettype($this->$name);

                    if (isset($this->_modifiedData[$name]))
                        $string .= ' *';
                }

                $string .= PHP_EOL;
            }
        }

        return $string;
    }

    /**
     * Renvoi la connexion vers la base de données, dans le but d'y écrire des
     * données.
     *
     * @return kore_db_pdo
     */
    protected function _getWriteDb()
    {
        return kore::$db->{kore::$db->getProfilNameFromClassName(get_class(
                $this), 'w')};
    }

    /**
     * Renvoi la connexion vers la base de données, dans le but d'y lire
     * des données.
     *
     * @return kore_db_pdo
     */
    protected function _getReadDb()
    {
        /*
         * Ugly hack : jusqu'à maintenant _getReadDb() faisait appel à
         * _getWriteDb() si non surchargée.
         *
         * On *doit* conserver ce comportement par compatibilité, mais on veut
         * aussi avoir la possibilité d'utiliser la configuration centralisée
         * via getProfilNameFromClassName().
         *
         * La solution adoptée est donc la suivante : si la méthode
         * getProfilNameFromClassName() retourne 'default' on suppose qu'elle
         * n'est pas exploitée et que par conséquent on souhaite utiliser
         * l'ancien mécanisme, c'est à dire _getWriteDb().
         */
        $dbProfil = kore::$db->getProfilNameFromClassName(get_class($this), 'r');

        if ($dbProfil === 'default')
            return $this->_getWriteDb();

        return kore::$db->$dbProfil;
    }

    /**
     * Retourne un tableau associatif contenant les clés de l'objet, afin de
     * manipuler la BDD.
     *
     * @return array
     */
    protected function _getPrimaryKey()
    {
        return array(
                static::PRIMARY_KEY => $this->_originalData[static::PRIMARY_KEY]);
    }

    /**
     * Retourne la clé de l'objet (utilisée par exemple dans les collections).
     *
     * @return mixed
     */
    public function getKey()
    {
        return $this->{static::PRIMARY_KEY};
    }

    /**
     * On wakeUp, register the instance in cache, if not yet instanced.
     */
    public function __wakeup()
    {
        if (static::keptInMemory())
            static::_registerInstance($this->getKey(), $this);
    }

    /**
     * Renvoi la valeur d'une propriété, et si elle n'est pas encore connue va
     * chercher l'information en base de données.
     *
     * @param  string  $property
     * @return mixed
     */
    public function __get($property)
    {
        if (isset($this->_modifiedData) and
            array_key_exists($property, $this->_modifiedData) === true)
            return $this->_modifiedData[$property];
        if (isset($this->_originalData[$property]) or
                array_key_exists($property, $this->_originalData) === true)
            return $this->_originalData[$property];

        if ($this->_exists === false) return null;

        $properties = static::getProperties();

        if (!isset($properties[$property])){
            kore::$error->track("unknown property [$property] in class ".get_class($this));
            return NULL;
        }

        $p = $properties[$property];

        if (isset($p->read)){
            if (($fct = $p->read) === false) {
                kore::$error->track("error while reading [$property] in class ".get_class($this));
                return null;
            }
            $this->$fct($property);

            if (isset($this->_originalData[$property]))
                return $this->_originalData[$property];
            return null;
        }

        /*
         * S'il s'agit d'une relation gérée de manière automatique, on délègue
         * cette tâche.
         */
        if (isset($p->rel))
            return $this->_getRelation($property, $p);
    }

    /**
     * Prend en charge le chargement des relations de manière automatique.
     *
     * @param string   $propName
     * @param stdClass $p
     */
    protected function _getRelation($propName, $p)
    {
        if( $p->relT === '1N' )
            return $this->_originalData[$propName] =
                    call_user_func(array($p->rel, $p->fct), $this);

        $params = array();

        foreach( $p->params as $param ) {
            if ( $this->$param === null ) {
                $this->_originalData[$propName] = null;
                return null;
            }

            $params[$param] = $this->$param;
        }

        if( $p->relT === '11' ){
            /*
             * Par compatibilité avec d'anciens scripts, on maintient la
             * possibilité d'utiliser fromKey()
             */
            if (count($params) > 1 and method_exists($p->rel, 'fromKey'))
                $fct = 'fromKey';
            else
                $fct = 'fromId';

            return $this->_originalData[$propName] =
                    call_user_func_array(array($p->rel, $fct), $params);

        } else {
            $fct = '_loadRel_' . $propName;
            return $this->_originalData[$propName] =
                    $this->$fct($p->rel, $params, $p->cols);
        }
    }

    /**
     * Modifie la valeur d'une propriété, si elle le permet.
     *
     * @param string  $property
     * @param mixed   $value
     */
    public function __set($property, $value)
    {
        /*
         * Détecte le cas où les priorités sont directement renseignées via
         * PDO::fetch() ou PDO::fetchAll()
         */
        if (empty($this->_originalData) and $this->_exists === null){
            $this->_modifiedData[$property] = $value;
            return;
        }

        $p = static::getProperty($property);

        if (($function = $p->write ) === false) {
            kore::$error->track("not allowed to modify property [$property] in class ".get_class($this));
            return;
        }

        if ($function === true)
            $this->_modifiedData[$property] = $value;
        else $this->$function($property, $value);
    }

    /**
     * Vérifie l'existence d'une propriété.
     *
     * @param  string  $property
     * @return boolean
     */
    public function __isset($property)
    {
        return array_key_exists($property, $this->_originalData)
            or (isset($this->_modifiedData) and
                array_key_exists($property, $this->_modifiedData))
            or isset($this->$property);
    }

    /**
     * Efface la valeur d'une ou plusieurs propriétés depuis le cache
     * _originalData
     *
     * @param  string  $property1
     * @param  string  $propertyN
     */
    protected function _eraseFromCache($properties)
    {
        $args = func_get_args();
        foreach ($args as $arg)
            if (array_key_exists($arg, $this->_originalData) === true)
                unset($this->_originalData[$arg]);
    }

    /**
     * Charge les principaux champs de l'objet (cf méthode _getProperties() )
     */
    protected function _load()
    {
        /*
         * Cette requête n'est pas sensée retourner plus d'une ligne. Si ce
         * n'est pas le cas, alors elle est erronée.
         */
        if (($importedLines = $this->_importFromKey()) > 1)
            throw new RuntimeException(get_class($this).'::'.__FUNCTION__."() should not fetch more than one row");

        /*
         * Si la requête ne retourne aucun enregistrement, alors l'objet
         * n'existe pas.
         */
        $this->_exists = ( $importedLines > 0 );
    }

    /**
     * Vérifie que l'objet existe en base.
     *
     * @return boolean
     */
    public function exists()
    {
        if ($this->_exists === NULL)
            $this->_load();

        return $this->_exists;
    }

    /**
     * Déclenche l'écriture dans l'objet de tout un lot de propriétés
     *
     * @param  array $data
     */
    public function setAll( $data )
    {
        foreach ($data as $property => $value)
            $this->__set($property, $value);
    }

    /**
     * Pré-rempli les valeurs des différentes propriétés à partir de données
     * fiables.
     *
     * Il s'agit ici de gagner en performance en évitant de faire des SELECT
     * superflus si on a la possibilité de récupérer les données autrement.
     * A n'appeler que si les données viennent elles aussi de la base
     * de données !
     *
     * @param  array   $data
     * @param  boolean $setExists   Indique si l'objet doit être marqué comme existant.
     */
    public function prefetchData( $data, $setExists = false )
    {
        if ($this->_exists === false) return NULL;
        if ($setExists === true) $this->_exists = true;

        $this->_fetchRow($data);
    }

    /**
     * Importe dans l'objet un tableau ou objet issu d'une source fiable (=BDD).
     *
     * @param  array $row
     */
    protected function _fetchRow($row)
    {
        $props = static::getProperties();

        foreach ($row as $property => $value) {
            if (!isset($props[$property]))
                continue;

            if ($value === null or $props[$property]->cast === false)
                $this->_originalData[$property] = $value;
            else
                $this->_originalData[$property] = $this->_castToProperty($value,
                        $props[$property]->cast);
        }
    }


    /**
     * Import dans l'objet des propriétés à partir d'une clé.
     *
     * @param  array   $key
     * @param  string  $cols
     * @return integer nombre de lignes renvoyées par le select.
     */
    protected function _importFromKey(array $key = null, $cols = null)
    {
        $db = $this->_getReadDb();

        if ($key === null)
            $key = $this->_getPrimaryKey();

        if ($cols === null)
            $cols = static::MAIN_COLS;

        $stmt = $db->keepPrepare(
               "select $cols
                from ".static::getFullTableName($db)."
                where ".$db->buildBindList($key, ' and '));

        if (!$stmt) return false;

        return $this->_importFromStatement($stmt, $key);
    }

    /**
     * Importe dans l'objet des propriétés issues d'un SELECT SQL.
     *
     * @param  string  $query
     * @param  array   $params
     * @return integer nombre de lignes renvoyées par le select.
     */
    protected function _importFromQuery( $query, $params = null )
    {
        $db = $this->_getReadDb();

        $nbLines = 0;

        if (isset($params))
            $res = $db->q($query, $params);
        else
            $res = $db->query($query);

        if ($res !== false) {
            $res = $res->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res as $row) {
                ++$nbLines;
                $this->_fetchRow($row);
            }
        }

        return $nbLines;
    }

    /**
     * Importe dans l'objet des propriétés issues d'une requête préparée.
     *
     * @param  PDOStatement $stmt
     * @param  array   $params
     * @return integer nombre de lignes renvoyées par le select.
     */
    protected function _importFromStatement( PDOStatement $stmt, $params )
    {
        $nbLines = 0;
        if (($stmt->execute($params)) !== false) {
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res as $row) {
                ++$nbLines;
                $this->_fetchRow($row);
            }
        }

        return $nbLines;
    }

    /**
     * Importe dans une collection d'objets des propriétés issues d'un SELECT
     * SQL.
     *
     * @param  string  $query
     * @param  array   $params
     * @param  array   $instances   collection d'objets à remplir.
     * @param  string  $colKey      nom de la colonne du select dans la valeur servirade clé dans $instances.
     * @param  boolean $setExists   indique si l'objet doit être marqué comme existant.
     * @return integer nombre de lignes renvoyées par le select.
     */
    protected function _importFromMultiRowQuery( $query, $params, array $instances, $colKey, $setExists = false )
    {
        $db = $this->_getReadDb();

        $nbLines = 0;
        if (isset($params))
            $res = $db->q($query, $params);
        else
            $res = $db->query($query);

        if ($res !== false) {
            $res = $res->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res as $row) {
                ++$nbLines;
                if(!isset($instances[$row[$colKey]]))
                    continue;

                $instance = $instances[$row[$colKey]];
                $instance->_fetchRow($row);
                if ($setExists)
                    $instance->_exists = true;
            }
        }

        return $nbLines;
    }

    /**
     * Tente de charger un grand nombre d'objet simultanément.
     *
     * @param  array   $objList
     * @param  string  $key
     * @param  string  $query
     * @param  array   $params
     * @param  integer $maxItem
     * @param  string  $inListFct
     * @return array
     */
    protected function _multiLoad( array $objList, $key, $query, $params = null, $maxItem = 255, $inListFct = null )
    {
        $objList[$this->_originalData[$key]] = $this;

        /*
         * Marque tous les objets comme non existant.
         */
        foreach ($objList as $obj) $obj->_exists = false;

        $db = $this->_getReadDb();
        $queries = $db->buildInList(array_keys($objList), $maxItem, $inListFct);

        foreach ($queries as $in){
            $q = sprintf($query, $in);
            $p = $params;

            $this->_importFromMultiRowQuery($q, $p, $objList, $key, true);
        }

        /*
         * Etabli la liste des objets non trouvés.
         */
        $dontExists = array();
        foreach ($objList as $k => $obj)
            if (!$obj->_exists)
                $dontExists[$k] = $obj;

        /*
         * Retourne la liste des objets qui n'ont pas été retrouvés.
         */
        return $dontExists;
    }


    /**
     * Tente de créer (insérer) l'objet en base de données, de manière
     * automatique.
     *
     * @param  string  $table  nom de la table dans laquelle insérée les données.
     * @param  array   $params données à insérer.
     * @param  mixed   $useAutoIncrement indique si la valeur de l'autoIncrement doit être récupérée pour l'ID.
     * @return boolean
     */
    protected function _createAuto($table, $params, $useAutoIncrement)
    {
        $db = $this->_getWriteDb();

        if ($table === null)
            $table = static::TABLE_NAME;

        $cols = '';
        $values = '';
        foreach ($params as $key => $value) {
            if ($cols !== ''){
                $cols .= ', ';
                $values .= ', ';
            }
            $cols .= $db->quoteKeyword($key);
            $values .= ':'.$key;
        }

        /*
         * TODO : rendre ce code indépendant de MySQL
         */
        return $this->_createSQL(
           "insert ignore into ".$db->quoteKeyword($table)."
                ($cols) values ($values)",
            $params, $useAutoIncrement);
    }

    /**
     * Tente de créer l'objet en base de données, à partir d'une requête SQL
     * fournie.
     *
     * @param  string  $query
     * @param  array   $params
     * @param  mixed   $useAutoIncrement indique si la valeur de l'autoIncrement doit être récupérée pour l'ID.
     * @return boolean
     */
    protected function _createSQL($query, $params, $useAutoIncrement)
    {
        $this->_exists = false;
        $db = $this->_getWriteDb();

        $stmt = $db->keepPrepare($query);

        /*
         * On vérifie que la ligne ait bien été insérée
         */

        if ($stmt->execute($params) and $stmt->rowCount()) {
            $this->_exists = true;

            if ($useAutoIncrement){
                if ($useAutoIncrement === true)
                    $useAutoIncrement = static::PRIMARY_KEY;

                $this->_originalData[$useAutoIncrement] =
                        (int) $db->lastInsertId();

            }

            $this->prefetchData($params);
        }

        return $this->_exists;
    }

    /**
     * Applique en base de données les modifications qui ont été faites sur
     * l'objet, si toutefois il y en a.
     *
     * @return  boolean
     */
    public function write()
    {
        /*
         * Rien à faire, sort de suite.
         */
        if (!isset($this->_modifiedData))
            return true;

        $props = static::getProperties();

        $toWrite = array();
        foreach ($this->_modifiedData as $property => $value) {
            if (array_key_exists($property, $this->_originalData) === false or
                $this->_originalData[$property] !== $value){

                if (isset($props[$property]))
                    $cast = $props[$property]->cast;
                else
                    $cast = false;

                $toWrite[$property] = $this->_castToSQL($value, $cast);
            }
        }

        $result = true;

        if (count($toWrite) === 0)
            $this->_modifiedData = null;
        else {
            $result = $this->_write($toWrite);

            /*
             * On retire du cache les données qui viennent d'être modifiées, vu
             * qu'elles ne sont plus fiables.
             */
            foreach ($toWrite as $property => $value){
                if (array_key_exists($property, $this->_originalData) === true)
                    unset($this->_originalData[$property]);
            }

            /*
             * A moins qu'on ait rencontré une erreur (et que _write() ait
             * explicitement retourné false), on efface les données en attente
             * d'écriture.
             */
            if ($result !== false)
                $this->_modifiedData = null;
        }

        return $result;
    }

    /**
     * Enregistre les données en base.
     *
     * @param   array $toWrite
     * @return  boolean
     */
    protected function _write($toWrite)
    {
        $db = $this->_getWriteDb();

        return $db->update(static::TABLE_NAME, $toWrite,
                $this->_getPrimaryKey());
    }

    /**
     * Supprime l'objet de la base de données.
     *
     * @return  boolean
     */
    public function delete()
    {
        $db = $this->_getWriteDb();

        $result = $db->delete(static::TABLE_NAME, $this->_getPrimaryKey());

        if ($result !== false){
            $this->_exists = false;
            $this->_modifiedData = null;
        }

        return $result;
    }


    /**
     * Force le type d'une données.
     *
     * @param  mixed $value
     * @param  string $type
     * @return mixed
     */
    protected function _castToProperty($value, $type)
    {
        if ($value === null or $type === false)
            return $value;

        switch ($type){
            case 'bool':
            case 'boolean':
            case 'int':
            case 'integer':
            case 'float':
            case 'double':
            case 'string':
                settype($value, $type);
                break;
            case 'array':
            case 'object':
                if (is_string($value))
                    $value = unserialize($value);
                break;
            case 'date':
                if (!$value instanceOf kore_date)
                    $value = kore_dateCache::fromSQL($value);
                break;
            case 'time':
                $value = kore_date::fromTime($value);
                break;
            case 'datetime':
                $value = kore_date::fromSQL($value);
                break;
            case 'timestamp':
                $value = kore_date::fromTimestamp($value);
                break;
        }

        return $value;
    }

    /**
     * Force le type d'une données.
     *
     * @param  mixed $value
     * @param  string $type
     * @return mixed
     */
    protected function _castToSQL($value, $type)
    {
        if ($value === null or $type === false)
            return $value;

        switch ($type){
            case 'bool':
            case 'boolean':
                $value = (int)$value;
                break;
            case 'array':
            case 'object':
                $value = serialize($value);
                break;
            case 'date':
            case 'datetime':
                $value = $value->sqltime;
                break;
            case 'timestamp':
                $value = $value->timestamp;
                break;
        }

        return $value;
    }


    /**
     * Crée une propriété.
     *
     * @param  string $readFunction   nom de la fonction a utiliser en lecture.
     * @param  mixed  $writeFunction  nom de la fonction a utiliser en écriture (si = false, alors écriture interdite).
     * @param  string $castType       nom du type de données pour typage.
     *
     * @return stdClass
     */
    static public function _newProperty($readFunction = '_load', $writeFunction = false, $castType = false )
    {
        $p = new stdClass();
        $p->read = $readFunction;
        $p->write = $writeFunction;
        $p->cast = $castType;

        return $p;
    }

    /**
     * Crée une relation 1..1 .
     *
     * @param  string  $objectName
     * @param  mixed   $localParamNames
     *
     * @return stdClass
     */
    static protected function _newRelation11($objectName, $localParamNames)
    {
        $r = new stdClass();
        $r->write = false;
        $r->cast = false;
        $r->rel = $objectName;
        $r->relT = '11';
        $r->params = (array) $localParamNames;

        return $r;
    }

    /**
     * Crée une relation 1..N.
     *
     * @param  string  $collectionName
     * @param  string  $fctName
     *
     * @return stdClass
     */
    static protected function _newRelation1N($objectName, $fctName)
    {
        $r = new stdClass();
        $r->write = false;
        $r->cast = false;
        $r->rel = $objectName;
        $r->relT = '1N';
        $r->fct = $fctName;

        return $r;
    }

    /**
     * Crée une relation N..M.
     *
     * @param  string  $collectionName
     * @param  mixed   $localParamNames
     * @param  string  $relationCols
     *
     * @return stdClass
     */
    static protected function _newRelationNM($objectName, $localParamNames, $relationCols = null)
    {
        $r = new stdClass();
        $r->write = false;
        $r->cast = false;
        $r->rel = $objectName;
        $r->relT = 'NM';
        $r->params = (array) $localParamNames;
        if( $relationCols === null )
            $r->cols = array();
        else
            $r->cols = preg_split('#\\s*,\\s*#', $relationCols);

        return $r;
    }


    /**
     * Recopie les propriétés d'un objet kore_db_mapping_element vers un second.
     *
     * @param  kore_db_mapping_element $element
     * @return kore_db_mapping_element
     */
    protected function _copyTo(kore_db_mapping_element $element)
    {
        $element->_exists = $this->_exists;
        $element->_originalData = $this->_originalData;
        $element->_modifiedData = $this->_modifiedData;

        return $element;
    }

}

<?php

abstract class kore_db_mapping_elementHash extends kore_db_mapping_element
{
    static protected $_lateLoadId = array();
    static protected $_lateLoadHash = array();
    static protected $_cacheIdFromHash = array();

    static public function cancelLateLoading()
    {
        $nbEntries = 0;
        $class = get_called_class();

        if (isset(self::$_lateLoadId[$class])) {
            $nbEntries += count(self::$_lateLoadId[$class]);
            unset(self::$_lateLoadId[$class]);
        }

        if (isset(self::$_lateLoadHash[$class])) {
            $nbEntries += count(self::$_lateLoadHash[$class]);
            unset(self::$_lateLoadHash[$class]);
        }

        return $nbEntries;
    }

    static public function fromId($id)
    {
        $class = get_called_class();

        if (isset(self::$_lateLoadId[$class][$id]))
            return self::$_lateLoadId[$class][$id];

        $obj = parent::fromId($id);

        if (!isset(self::$_lateLoadId[$class]))
            self::$_lateLoadId[$class] = array($id => $obj);
        elseif (is_array(self::$_lateLoadId[$class]))
            self::$_lateLoadId[$class][$id] = $obj;

        return $obj;
    }

    static public function fromHash($hash)
    {
        $class = get_called_class();

        if (isset(self::$_lateLoadHash[$class][$hash]))
            return self::$_lateLoadHash[$class][$hash];

        $obj = new static;
        $obj->_originalData['hash'] = $hash;

        if (!isset(self::$_lateLoadHash[$class]))
            self::$_lateLoadHash[$class] = array($hash => $obj);
        elseif (is_array(self::$_lateLoadHash[$class]))
            self::$_lateLoadHash[$class][$hash] = $obj;

        return $obj;
    }

    static protected function _getProperties()
    {
        return array(
            'id'    => self::_newProperty('_loadFromHash', false, 'int'),
            'hash'  => self::_newProperty('_loadFromId'),
            );
    }

    /**
     * Pour compatibilité avec les classes existantes, on laisse une méthode
     * _load() classique, pour laquelle on ne peut pas avoir le nom de la
     * propriété recherchée (cf conflit de déclaration avec kore_db_mapping_element).
     *
     * @see kore_db_mapping_element::_load()
     */
    protected function _load()
    {
        $this->_loadFromId('unknown?');
    }

    abstract protected function _queryLoadFromMultiId();
    abstract protected function _queryLoadFromMultiHash();

    protected function _loadFromId($property)
    {
        $class = get_class($this);

        /*
         * On s'assure que le $this->id soit toujours chargé.
         */
        $this->id;

        /*
         * Si l'utilisation de $this->id ci dessus a déclenché un _loadFromHash()
         * il y a de grandes chances qu'on ait déjà l'info qu'on cherchait.
         */
        if (array_key_exists($property, $this->_originalData))
            return;

        if( !isset(self::$_lateLoadId[$class]) )
            self::$_lateLoadId[$class] = array($this->id => $this);

        if( !is_array(self::$_lateLoadId[$class]) )
            $toLoad = array();
        else
            $toLoad = & self::$_lateLoadId[$class];

        $this->_multiLoad($toLoad, 'id', $this->_queryLoadFromMultiId());

        if (is_array(self::$_lateLoadId[$class]))
            self::$_lateLoadId[$class] = array();
    }

    /**
     * TODO : à remplacer par une propriété statique en PHP 5.3
     */
    protected function _getSQLHashFunction()
    {
        return null;
    }

    protected function _loadFromHash($property)
    {
        $class = get_class($this);

        /**
         * Gère un cache hash => id
         */
        if( !isset(self::$_cacheIdFromHash[$class]) )
            self::$_cacheIdFromHash[$class] = array();
        elseif( is_array(self::$_cacheIdFromHash[$class]) ){
            if( isset(self::$_cacheIdFromHash[$class][$this->hash]) ){
                $this->_originalData['id'] =
                        self::$_cacheIdFromHash[$class][$this->hash];
                $this->_exists = true;
                if( isset(self::$_lateLoadHash[$class][$this->hash]) )
                    unset(self::$_lateLoadHash[$class][$this->hash]);

                /*
                 * Le cache n'a permis de récupérer que l'id de l'objet, il faut
                 * maintenant récupérer la propriété recherchée.
                 */
                if ($property!=='id')
                    $this->_loadFromId();

                return;
            }
        }

        if( !is_array(self::$_lateLoadHash[$class]) )
            $toLoad = array();
        else
            $toLoad = & self::$_lateLoadHash[$class];

        $objToCreate = $this->_multiLoad($toLoad, 'hash',
                $this->_queryLoadFromMultiHash(), null, 255,
                $this->_getSQLHashFunction());

        foreach ($toLoad as $obj)
            if (isset($obj->_originalData['id']))
                self::$_cacheIdFromHash[$class][$obj->hash] =
                        $obj->_originalData['id'];

        $toLoad = array();

        if( count($objToCreate) > 0 ){
            $this->_multiCreate($objToCreate);
            $this->_multiLoad($objToCreate, 'hash',
                $this->_queryLoadFromMultiHash(), null, 255,
                $this->_getSQLHashFunction());

            foreach ($objToCreate as $obj)
                if (isset($obj->_originalData['id']))
                    self::$_cacheIdFromHash[$class][$obj->hash] =
                            $obj->_originalData['id'];
        }
    }

    abstract protected function _multiCreate( $objList );

    /**
     * Surcharge de fetchrow permettant de supprimer le contenu de _originalData
     * si id est défini et ainsi de préserver un peu de mémoire.
     * @see kore_db_mapping_element::_fetchRow()
     */
    protected function _fetchRow($row)
    {
        if (isset($row['id']))
            $this->_originalData = array();

        parent::_fetchRow($row);
    }
}

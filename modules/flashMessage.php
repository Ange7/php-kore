<?php
/**
 * Class which helps to manage «flash messages», ie transient messages, by using
 * cookies.
 *
 * Be carefull : since messages are stored on client browser, they can be
 * altered or forged. So don't put serialized items in it.
 */
class kore_flashMessage
{
    /**
     * Message internal id
     *
     * @var string
     */
    private $_key;

    /**
     * Message contents
     *
     * @var string
     */
    public $message;

    /**
     * Type of message.
     *
     * @var integer
     */
    public $type;

    /**
     * Creation time of the message.
     *
     * @var integer
     */
    public $creationTime;

    /**
     * Build the cookie name
     *
     * @param  string $messageKey
     * @return string
     */
    static public function getCookieName($messageKey = null)
    {
        $baseName = kore::$conf->get('flashMessage_cookieName', 'fsh');

        if ($messageKey !== null)
            return $baseName . "[$messageKey]";

        return $baseName;
    }

    /**
     * Indicate how many flash messages we received.
     *
     * @return number
     */
    static public function haveMessages()
    {
        $cookieMessage = static::getCookieName();
        if (empty($_COOKIE[$cookieMessage]) or
                !is_array($_COOKIE[$cookieMessage]))
            return 0;
        return count($_COOKIE[$cookieMessage]);
    }

    /**
     * Return the next message, then drop it.
     * If there was no message, return null.
     *
     * @return NULL|kore_flashMessage
     */
    static public function popMessage()
    {
        if (!static::haveMessages())
            return null;

        $cookieMessage = static::getCookieName();

        $return = null;

        while ($return === null) {
            $next = each($_COOKIE[$cookieMessage]);
            if ($next === false)
                return null;

            list($messageKey, $messageContents) = $next;

            $m = new static();
            $m->_key = $messageKey;
            $m->message = gzuncompress(base64_decode($messageContents));

            $values = unpack('Ntime/Ctype/Ncrc/ncount',
                    base64_decode(strtr($messageKey, '-_', '+/')));

            /*
             * Check that the message corresponds to the CRC.
             * Note : it's not a security check.
             */
            if ($values['crc'] !== crc32($m->message))
                continue;

            $m->type = $values['type'];
            $m->creationTime = $values['time'];

            $m->drop();

            $return = $m;
        }

        return $return;
    }

    /**
     * Return (and drop) all previous messages.
     *
     * @return array
     */
    static public function popMessages()
    {
        $list = array();
        while ($message = static::popMessage())
            $list[] = $message;
        return $list;
    }

    /**
     * Create a new message, and send it to the browser. Here the $type is a
     * 8bit number (between 0 to 255). The $path and $domain parameters allow to
     * restrict the cookie availability.
     *
     * @param  string $message
     * @param  integer $type
     * @param  string $path
     * @return kore_flashMessage
     */
    static public function add($message, $type = 0, $path = null, $domain = null)
    {
        $m = new static();
        $m->message = $message;
        $m->type = ($type & 0xFF);
        $m->creationTime = kore::requestTime();
        $m->write($path, $domain);

        return $m;
    }

    /**
     * Build the message key. Here we try to build a uniq key, which should stay
     * compact.
     *
     * @return string
     */
    protected function _getKey()
    {
        static $count = 0;

        if (!isset($this->_key)){
            $this->_key = rtrim(strtr(base64_encode(pack('NCNn',
                    $this->creationTime, $this->type, crc32($this->message),
                    $count++)), '+/', '-_'), '=');
        }

        return $this->_key;
    }

    /**
     * Write (send) the current message to the browser
     *
     * @param  string $path
     * @param  string $domain
     * @return boolean
     */
    public function write($path = null, $domain = null)
    {
        return $this->_sendCookie(base64_encode(gzcompress($this->message)),
                $path, $domain);
    }

    /**
     * Drop the cookie corresponding to the current message
     *
     * @param  string $path
     * @param  string $domain
     * @return boolean
     */
    public function drop($path = null, $domain = null)
    {
        return $this->_sendCookie(false, $path, $domain);
    }

    /**
     * Send the cookie
     *
     * @param  string $value
     * @param  string $path
     * @param  string $domain
     * @return boolean
     */
    protected function _sendCookie($value, $path = null, $domain = null)
    {
        $this->_disableHTTPCache();

        $cookieName = static::getCookieName($this->_getKey());

        if ($path === null)
            $path = kore::$conf->get('flashMessage_defaultPath', '/');
        if ($domain === null)
            $domain = kore::$conf->flashMessage_defaultDomain;

        $secure   = kore::$conf->flashMessage_secureCookie;
        $httpOnly = kore::$conf->flashMessage_httpOnly;

        return setcookie($cookieName, $value, 0, $path, $domain, $secure,
                $httpOnly);
    }

    /**
     * If we use flash message, browser (and proxies) must not keep this page in
     * cache.
     */
    protected function _disableHTTPCache()
    {
        kore::$conf->response_httpExpire  = -1;
        kore::$conf->response_httpLimiter = 'private';
    }

}
<?php

class kore_template_compiler_executeStatic
{
    protected $_context;

    public function setContext( kore_template_context $context )
    {
        $this->_context = $context;
    }

    public function execute( $code, $contextLine, $contextName = null )
    {
        if( $this->_context === NULL )
            $this->_context = new kore_template_context();

        $code = '?'.">\n" . $code . '<?php return true;' ;

        ob_start();
        $c = $this->_context;
//        unset($this);
        $return = eval($code);
        if( $return === true )
            $return = ob_get_clean();
        else {
            ob_end_clean();

//            die($code);

            throw new kore_template_compiler_exception(
                    'error during static execution',
                    $contextLine, $contextName);
        }

        return $return;
    }

}

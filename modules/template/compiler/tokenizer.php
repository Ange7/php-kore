<?php
class kore_html_templateCompilerTokenizer
{
    protected $buffer;
    protected $startOffset;

    public $modifiers = array();
    public $ignoreStatment = false;
    public $tokens = array();
    public $length = 1;
    public $linefeed = false;

    protected $confPHPOperators = array( 'and', 'or', 'xor' );
    protected $confFakePHPOperators = array( 'eq', 'ne', 'gt', 'lt', 'ge', 'le' );
    protected $confPHPConstants = array( 'true', 'false', 'NULL', 'PHP_EOL' );
    protected $confSymbolsWichForcesNewToken = array( '(', ')', '[', ']', ';' );

    public function __construct( & $buffer, $startOffset = 0 )
    {
        $this->buffer = & $buffer;
        $this->startOffset = $startOffset;

        $this->confPHPOperators = array_flip( $this->confPHPOperators );
        $this->confFakePHPOperators = array_flip( $this->confFakePHPOperators );
        $this->confPHPConstants = array_flip( $this->confPHPConstants );
        $this->confSymbolsWichForcesNewToken = array_flip( $this->confSymbolsWichForcesNewToken );

        $this->parse();
    }

    protected function parse()
    {
        $tokenIdx = 0;

        $i = $this->startOffset;
        $endOfBuffer = strlen( $this->buffer );
        $endWasFound = false;

        $currentTokenType = '';
        $string = false;
        $escape = false;
        $bracketsStack = array();

        while( ++ $i < $endOfBuffer )
        {
            $char = $this->buffer{$i};
            $escaped = $escape;
            $escape = false;

            if( $char === $string )
            {
                $this->tokens[ $tokenIdx ]->contents .= $char ;

                if( ! $escaped )
                {
                    $string = false ;

                    if( preg_match( '#\\$[a-zA-Z0-9_]#', $this->tokens[ $tokenIdx ]->contents ) )
                    {
                        throw new Exception( 'variable found in a string enclosed by double quotes' );
                    }
                }
            }
            elseif( $string )
            {
                if( $char === '\\' and ! $escaped )
                    $escape = true ;

                $this->tokens[ $tokenIdx ]->contents .= $char ;
            }
            elseif( $char === '\'' or $char === '"' )
            {
                $currentTokenType = 'string' ;

                $this->tokens[ ++$tokenIdx ] = new kore_html_templateCompilerToken( $currentTokenType, '', $char );

                $string = $char ;

                if( $char === '"' )
                    $this->tokens[ $tokenIdx ]->subtype = 'parsed';
            }
            elseif( $char === '{' or $char === '$' or $char === '`' )
            {
                throw new Exception( 'character "'.$char.'" is not allowed in templates statments' );
            }
            elseif( $char === '}' )
            {
                $endWasFound = $i - $this->startOffset ;
                break;
            }
            elseif( $char === "\n" )
            {
                $this->ignoreStatment = true;
                break;
            }
            elseif( preg_match( '#^[a-zA-Z0-9_]$#', $char ) )
            {
                if( $currentTokenType === 'word' )
                    $this->tokens[ $tokenIdx ]->contents .= $char ;
                else
                {
                    $currentTokenType = 'word';

                    $this->tokens[ ++$tokenIdx ] = new kore_html_templateCompilerToken( $currentTokenType, '', $char );
                }
            }
            elseif( $char === ':' )
            {
                if( $currentTokenType === 'semicolon' )
                    $this->tokens[ $tokenIdx ]->contents .= $char ;
                else
                {
                    $currentTokenType = 'semicolon';

                    $this->tokens[ ++$tokenIdx ] = new kore_html_templateCompilerToken( $currentTokenType, '', $char );
                }
            }
            elseif( preg_match( '#^\\s$#', $char ) )
            {
//                if( $currentTokenType !== 'symbol' )
                    $currentTokenType = '';
            }
            else // Symbols
            {
                if( ( $currentTokenType !== 'symbol' )
                    or isset( $this->confSymbolsWichForcesNewToken[ $char ] )
                    or isset( $this->confSymbolsWichForcesNewToken[ $this->tokens[ $tokenIdx ]->contents ] ) )
                {
                    $currentTokenType = 'symbol';
                    $this->tokens[ ++$tokenIdx ] = new kore_html_templateCompilerToken( $currentTokenType, '', $char );
                }
                else $this->tokens[ $tokenIdx ]->contents .= $char ;

                if( $char === '(' or $char === '[' )
                {
                    array_push( $bracketsStack, $char );
                }
                elseif( $char === ')' )
                {
                    $endStack = end( $bracketsStack );

                    if( $endStack === '(' )
                        array_pop( $bracketsStack );
                    elseif( $endStack )
                        throw new Exception( '"'.$char.'" found while expecting "]"' );
                    else
                        throw new Exception( 'unexpected "'.$char.'" found' );
                }
                elseif( $char === ']' )
                {
                    $endStack = end( $bracketsStack );

                    if( $endStack === '[' )
                        array_pop( $bracketsStack );
                    elseif( $endStack )
                        throw new Exception( '"'.$char.'" found while expecting ")"' );
                    else
                        throw new Exception( 'unexpected "'.$char.'" found' );
                }

            }

        }

        if( $this->ignoreStatment )
            return ;

        if( $string )
            throw new Exception( 'end of string not found (started by ['.$string.'] )' );
        elseif( ! $endWasFound )
            throw new Exception( 'end of statment not found' );
        elseif( count( $bracketsStack ) > 0 )
        {
            $char = array_pop( $bracketsStack );
            if( $char === '(' )
                $needed = ')';
            else $needed = ']';

            throw new Exception( '"'.$needed.'" required' );
        }

        // check tokens
        $searchModifiers = true;
        $tokenIdx = 0 ;
        $previousToken = NULL;
        $currentToken = NULL;

        while( isset( $this->tokens[ ++ $tokenIdx ] ) )
        {
            if( $currentToken !== NULL )
                $previousToken = $currentToken;

            $currentToken = $this->tokens[ $tokenIdx ];

            if( isset( $this->tokens[ $tokenIdx + 1 ] ) )
                $nextToken = $this->tokens[ $tokenIdx + 1 ];
            else $nextToken = NULL;

            if( $searchModifiers and $currentToken->type === 'word'
                                 and $nextToken !== NULL
                                 and $nextToken->contents === ':' )
            {
                $this->modifiers[ $currentToken->contents ] = true;

                // remove current and next token
                $currentToken = NULL ;
                unset( $this->tokens[ $tokenIdx ] );
                unset( $this->tokens[ $tokenIdx + 1 ] );
                $tokenIdx++;

                continue;
            }
            else $searchModifiers = false;

            if( $currentToken->type === 'word' )
            {
                $currentToken->type = 'variable';
                $currentToken->subtype = '';

                if( is_numeric( $currentToken->contents ) )
                {
                    $currentToken->type = 'numeric';
                }
                elseif( isset( $this->confPHPOperators[ $currentToken->contents ] ) )
                {
                    $currentToken->type = 'symbol';
                    $currentToken->subtype = 'operator';
                }
                elseif( isset( $this->confFakePHPOperators[ $currentToken->contents ] ) )
                {
                    $currentToken->type = 'symbol';
                    $currentToken->subtype = 'fake_operator';
                }
                elseif( isset( $this->confPHPConstants[ $currentToken->contents ] ) )
                {
                    $currentToken->type = 'variable';
                    $currentToken->subtype = 'constant';
                }
                else
                {
                    $isFromObject = false ;

                    if( $previousToken !== NULL )
                    {
                        if( $previousToken->contents === '::' or $previousToken->contents === '.' )
                        {
                            $isFromObject = true;
                            $currentToken->type = 'property';
                        }
                    }

                    if( $nextToken !== NULL )
                    {
                        $firstChar = substr( $nextToken->contents, 0, 1 );
                        if( $firstChar === '(' )
                        {
                            if( $isFromObject )
                                $currentToken->type = 'method';
                            else $currentToken->type = 'function';
                        }
                        elseif( $firstChar === '[' )
                        {
                            $currentToken->subtype = 'array';
                        }
                    }
                }
            }
            elseif( $currentToken->contents === '.' )
            {
                if( $previousToken === NULL )
                    throw new Exception( 'parse error on "."' );

                if( $previousToken->type === 'symbol' )
                {
                    // skip error
                }
                elseif ( $previousToken->type === 'variable' or $previousToken->type === 'property' )
                    $previousToken->subtype = 'object';
                else
                    throw new Exception( 'parse error on "."' );
            }
            elseif( $currentToken->contents === '->' )
            {
                throw new Exception( 'please use "." to access object properties and methods instead of "->"' );
            }
            elseif( $currentToken->type === 'semicolon' )
            {
                if( $currentToken->contents === '::' and $previousToken !== NULL and
                    ( $previousToken->type === 'variable' or $previousToken->type === 'property' ) )
                    $this->previousToken->subtype = 'class';
                else
                {
                    throw new Exception( 'parse error on "'.$currentToken->contents.'"' );
                }
            }

            if( $currentToken->type !== 'symbol' and $previousToken !== NULL
                and ( $previousToken->type !== 'symbol' and $previousToken->type !== 'semicolon' ) )
            {
                throw new Exception( 'parse error near '.$currentToken->type.' "' . $currentToken->contents .'" ' );
            }
        }

        $this->length = $endWasFound + 1;

        if( substr( $this->buffer, $this->length, 1 ) === "\n" )
        {
          $this->linefeed = true;
        }
    }

}

class kore_html_templateCompilerToken
{
    public $type = '';
    public $subtype  = '';
    public $contents = '';

    function __construct( $type, $subtype, $contents )
    {
        $this->type = $type ;
        $this->subtype = $subtype ;
        $this->contents = $contents ;
    }
}


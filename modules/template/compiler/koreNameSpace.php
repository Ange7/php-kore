<?php

class kore_template_compiler_koreNameSpace extends kore_template_compiler_step
{
    protected $_tagStack = array();
    protected $_bindLevel = 0;

    /**
     * Cette phase consiste à prendre en charge les attributs de balises XML
     * utilisant le namespace "kore".
     * Ces attributs seront simplement remplacés par des tags classiques du
     * moteur de template, de type {xxx} afin d'être traités plus tard dans une
     * autre phase.
     *
     * @return string
     */
    public function getResult()
    {
        $newContents = '';
        $contentsLen = strlen($this->_contents);

        $offset = 0 ;
        while(($newOffset = strpos($this->_contents, '<', $offset)) !== false) {

            $newContents .= substr($this->_contents, $offset, $newOffset -
                    $offset);
            $offset = $newOffset;

            $ignoreTag = false;
            $deleteTag = false;

            if( substr($this->_contents, $offset + 1, 8) === '![CDATA[' ) {
                $ignoreTag = true;
                $tagName = 'cdata';
                $tagEnd = ']]>';
                $tagStartLength = 9;
                $tagEndLength = strlen($tagEnd);

                /*
                 * Recherche la fermeture de balise.
                 */
                $endPos = strpos($this->_contents, $tagEnd, $offset + 1);

            } elseif( substr($this->_contents, $offset + 1, 6) === '!--[if' ){
                $tagName = 'iecond';
                $tagEnd = ']>';
                $tagStartLength = 0;
                $tagEndLength = 0;

                /*
                 * Recherche la fermeture de balise.
                 */
                $endPos = strpos($this->_contents, $tagEnd, $offset + 1);

            } elseif( substr($this->_contents, $offset + 1, 7) === '![endif' ){
                $tagName = 'iecond';
                $tagEnd = ']-->';
                $tagStartLength = 0;
                $tagEndLength = 0;

                /*
                 * Recherche la fermeture de balise.
                 */
                $endPos = strpos($this->_contents, $tagEnd, $offset + 1);

            } elseif( substr($this->_contents, $offset + 1, 3) === '!--' ) {
                $deleteTag = true;
                $tagName = 'comments';
                $tagEnd = '-->';
                $tagStartLength = 4;
                $tagEndLength = strlen($tagEnd);

                /*
                 * Recherche la fermeture de balise.
                 */
                $endPos = strpos($this->_contents, $tagEnd, $offset + 1);

            } else {
                $tagName = 'unknown';
                $tagEnd = '>';
                $tagStartLength = 1;
                $tagEndLength = strlen($tagEnd);

                if( preg_match('#^([/?!]|(?:{hidden:}\\?))?([\\w:]+)#s',
                    substr($this->_contents, $offset + 1, 32), $match)) {

                    $tagName = $match[2];

                } else {
                    /*
                     * TODO : il semblerait qu'il ne s'agisse pas d'un tag HTML.
                     * Ne fait que tracer l'erreur pour conserver la
                     * compatibilité, mais le traitement devrait être stoppé.
                     */
                    kore::$error->track("template: symbole < utilisé en dehors d'un tag XHTML",
                            null, substr($this->_contents, $offset-50, 100),
                            $this->_sourceName, $this->searchLine($offset));

                    $newContents .= '<';
                    $offset++;
                    continue;
                }

                /*
                 * Recherche la fin du tag
                 *
                 * TODO : lorsque la compatibilité ci dessus ne sera plus
                 * assurée, un simple strpos pour trouver la fin du tag sera
                 * amplement suffisant (et probablement nettement plus rapide).
                 */
                $inString = false;
                $endPos = false;

                $i = $offset + $tagStartLength - 1;

                while( ++ $i < $contentsLen ) {
                    $char = $this->_contents{$i};

                    if( $inString !== false ) {
                        if( $char === $tagEnd ) {
                            /*
                             * TODO : ce caractêre ne devrait pas être présent
                             * ici. Ne fait que tracer l'erreur pour conserver
                             * la compatibilité, mais le traitement devrait être
                             * stoppé.
                             */
                            kore::$error->track("template: symbole > utilisé au sein d'un tag XHTML",
                                    null, null, $this->_sourceName, $this->searchLine($i));
                        }

                        if( $char === $inString )
                            $inString = false;

                    } elseif( $char === '{' ) {
                        $inString = '}';

                    } elseif( $char === '"' or $char === '\'' ) {
                        $inString = $char ;

                    } elseif( substr($this->_contents, $i, $tagEndLength) ===
                            $tagEnd ) {

                        $endPos = $i;
                        break;
                    }
                }
            }

            if( $endPos === false )
                $this->throwException('fin du tag <'.$tagName.'> non trouvée',
                        $offset);

            /*
             * Extrait le contenu de la balise
             */
            $tagContents = substr($this->_contents, $offset + $tagStartLength,
                    $endPos - $offset - $tagStartLength);

            if( $deleteTag ) {
                /*
                 * Ignore simplement ce tag.
                 */
            } elseif( $ignoreTag ) {
                $newContents .= '<![CDATA['.$tagContents.']]>';
            } elseif( $tagName === 'comments' ){
                $newContents .= '<!--'.$tagContents.'-->';
            } elseif( $tagName === 'iecond' ){
                $newContents .= $tagContents;
            } else {
                $tagName = $tagName;

                $open = true; $close = true;

                if( $tagContents{0} === '/' )
                    $open = false ;
                elseif( $tagContents{0} === '?' or
                    substr($tagContents, 0, 10) === '{hidden:}?' )
                    ;
/*                elseif( $tagName === 'iecond' ){
                    if( substr($tagContents, 0, 3) === '!--' )
                        $close = false;
                    else
                        $open = false;
                }*/
                elseif( $tagContents{strlen($tagContents) - 1} !== '/' )
                    $close = false;

                $charAfterTagPos = ($endPos + $tagEndLength);
                if( $charAfterTagPos < $contentsLen )
                    $returnAfterTag = $this->_contents{$charAfterTagPos};
                else $returnAfterTag = false;

                $selfClosed = ($open and $close);

                if( $open ) $newContents .= $this->openXHTMLTag($tagContents,
                        $tagName, $offset, $selfClosed, $returnAfterTag);
                if( $close ) $newContents .= $this->closeXHTMLTag($tagName,
                        $offset, $selfClosed, $returnAfterTag);
            }

            /*
             * Place le pointeur après la balise, pour continuer l'analyse.
             */
            $offset = $endPos + $tagEndLength;
        }

        foreach( $this->_tagStack as $tagInfos ) {
            if( $tagInfos->useKore ) {
                $this->throwException(
                    'le tag <' . $tagInfos->name .'> utilise des attributs ' .
                    'kore et n\'a pas été fermé avant la fin du script',
                    $tagInfos->offset );
            }
        }

        $newContents .= substr($this->_contents, $offset);

        return $newContents;
    }

    /**
     * Recense l'ouverture d'un tag XML
     *
     * @param string  $tagContents
     * @param string  $tagName
     * @param integer $offset
     * @return string
     */
    protected function openXHTMLTag( $tagContents, $tagName, $offset, $selfClosed, $followedByAReturn )
    {
        $params = trim(substr($tagContents, strlen($tagName) + 1));

        $tagInfos = new stdClass();
        $tagInfos->name = $tagName;
        $tagInfos->useKore = false;
        $tagInfos->offset  = $offset;
        $tagInfos->isBind  = false;

        $inTag = '';
        $preOpenTag   = '';
        $postOpenTag  = '';
        $preCloseTag  = '';
        $postCloseTag = '';

        $previousBindLevel = $this->_bindLevel;

        if (preg_match_all('#\\bkore:(\\w+)\\s*=\\s*("([^"]*)"|\'([^\']*)\')#',
            $params, $matches, PREG_SET_ORDER)) {

            $tagInfos->useKore = true;

            foreach( $matches as $match ) {

                $type = $match[1];
                if( isset($match[4]) )
                    $param = trim($match[4]);
                else $param = trim($match[3]);

                if( $type === 'bind' ){
                    $tagInfos->isBind = true;

                    $inTag .= "{static:if:method_exists($param,'inTag')}" .
                              "{pre:$param.inTag()}{endif:}";

                    $preOpenTag .= "{static:if:!isset($param)}" .
                             "tag [$tagName] bind to an unset variable : " .
                             "{endif:}" .
                             "{static:if:method_exists($param,'filterTag')}" .
                             "{static:hidden:_registerBind($param)}" .
                             "{endif:}" .
                             "{static:if:method_exists($param,'preOpenTag')}" .
                             "{pre:$param.preOpenTag()}{endif:}";
                    $postOpenTag = '{static:if: method_exists( ' . $param .
                            ', \'postOpenTag\' )}{pre: ' . $param .
                            '.postOpenTag()}{static:end:}' . $postOpenTag;
                    $preCloseTag .= '{static:if: method_exists( ' . $param .
                            ', \'preCloseTag\' )}{pre: ' . $param .
                            '.preCloseTag()}{static:end:}';
                    $postCloseTag = '{static:if: method_exists( ' . $param .
                            ', \'postCloseTag\' )}{pre: ' . $param .
                            '.postCloseTag()}{static:end:}' . $postCloseTag;

                    $this->_bindLevel++;

                 } else {
                    $preOpenTag .= '{'.$type.':'.$param.'}';
                    $postCloseTag = '{end'.$type.':}' . $postCloseTag;
                }

                $tagContents = str_replace($match[0], $inTag, $tagContents);
            }
        }

        if( $previousBindLevel > 0 ){
            /*
             * TODO : mettre en place un "vrai" parseur de propriétés.
             * On peut probablement utiliser SimpleXML pour ça.
             */
/*
			preg_match_all('#\\b((\\w+:)?\\w+)\\s*=\\s*("([^"]*)"|\'([^\']*)\')#S',
				$tagContents, $matches, PREG_SET_ORDER) );


            $tagProperties = var_export( explode(' ', $tagContents), true );
            $inTag = "{static:if:_checkFilters('inTag','$tagName',$tagProperties)!==NULL}".
                    "{pre:_checkFilters('inTag','$tagName',$tagProperties)}".
                    "{endif:}$inTag";
            $preOpenTag = "{static:if:_checkFilters('preOpenTag','$tagName',$tagProperties)!==NULL}".
                    "{pre:_checkFilters('preOpenTag','$tagName',$tagProperties)}".
                    "{endif:}$preOpenTag";
            $postOpenTag = "$postOpenTag{static:if:_checkFilters('postOpenTag','$tagName',$tagProperties)!==NULL}".
                    "{pre:_checkFilters('postOpenTag','$tagName',$tagProperties)}".
                    "{endif:}";
            $preCloseTag = "{static:if:_checkFilters('preCloseTag','$tagName',$tagProperties)!==NULL}".
                    "{pre:_checkFilters('preCloseTag','$tagName',$tagProperties)}".
                    "{endif:}$preCloseTag";
            $postCloseTag = "$postCloseTag{static:if:_checkFilters('postCloseTag','$tagName',$tagProperties)!==NULL}".
                    "{pre:_checkFilters('postCloseTag','$tagName',$tagProperties)}".
                    "{endif:}";
*/
        }

        $tagInfos->preTag  = $preCloseTag;
        $tagInfos->postTag = $postCloseTag;

        /*
         * Recense ce tag dans la pile.
         */
        array_push($this->_tagStack, $tagInfos);

        /*
         * Parce que le tag PHP "absorbe" les retours chariots qui le
         * suivent, ajoute un espace pour éviter cela et ne pas perturber la
         * mise en page.
         */
        if( $tagInfos->useKore and !$selfClosed and $followedByAReturn )
            $postOpenTag .= ' ';

        return $preOpenTag .'<'. $tagContents .'>'. $postOpenTag ;
    }

    /**
     * Recense la fermeture d'un tag XML
     *
     * @param string  $tagName
     * @param integer $offset
     * @param boolean $selfClosed
     * @return string
     */
    protected function closeXHTMLTag( $tagName, $offset, $selfClosed, $followedByAReturn )
    {
        if ($selfClosed)
            $tagContents = '';
        else $tagContents = '</' . $tagName . '>';

        /*
         * Si la fermeture de tag ne correspond à aucun tag, ou bien à un tag
         * différent alors ignore cette fermeture et sort de suite.
         */
        $tagInfos = end($this->_tagStack);
        if( $tagInfos === false or ( $tagInfos->name !== $tagName ) )
            return $tagContents;

        if( $tagInfos = array_pop($this->_tagStack) ) {
            $tagContents = $tagInfos->preTag .$tagContents. $tagInfos->postTag;

            /*
             * Parce que le tag PHP "absorbe" les retours chariots qui le
             * suivent, ajoute un espace pour éviter cela et ne pas perturber la
             * mise en page.
             */
            if ($tagInfos->useKore and $followedByAReturn)
                $tagContents .= ' ';

            if ($tagInfos->isBind)
                $this->_bindLevel--;
        }

        return $tagContents;
    }

}

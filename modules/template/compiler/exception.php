<?php

class kore_template_compiler_exception extends Exception
{
    protected $_contextName;
    protected $_contextLine;

    public function __construct( $message, $contextLine, $contextName = NULL )
    {
        parent::__construct( $message );

        $this->line = $contextLine;
        $this->file = $contextName;

        $this->_contextLine = $contextLine;
        $this->_contextName = $contextName;
    }

    public function getContextName()
    {
        return $this->_contextName;
    }
    public function getContextLine()
    {
        return $this->_contextLine;
    }
/*
    public function __toString()
    {
        /**
         * TODO : surcharger cette méthode.
         * /
        parent::__toString();
    }*/
}

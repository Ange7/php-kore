<?php

abstract class kore_template_compiler_step
{
    /**
     * Configuration de la compilation.
     *
     * @var kore_template_config
     */
    protected $_config;

    /**
     * Configuration du context.
     *
     * @var kore_template_context
     */
    protected $_context;

    /**
     * Contenu du template.
     *
     * @var string
     */
    protected $_contents = NULL;

    /**
     * Nom de la source du template.
     *
     * @var string
     */
    protected $_sourceName;

    /**
     * Crée un instance de kore_template_compiler_step afin d'exécuter une
     * étape de la compilation.
     *
     * @param  kore_template_config $config
     * @param  kore_template_context $context
     * @return kore_template_compiler_step
     */
    public function __construct( kore_template_config $config, kore_template_context $context )
    {
        $this->_config = $config;
        $this->_context = $context;

        $this->initialize();
    }

    public function initialize()
    {
        /**
         * Ne fait rien, mais peut être facilement surchargée afin de déclencher
         * une initialisation lors de l'instanciation.
         */
    }

    public function setContents( $contents, $name )
    {
        $this->_contents = $contents;
        $this->_sourceName = $name;
    }

    protected function searchLine( $offset )
    {
        $data = substr($this->_contents, 0, $offset);
        return ( 1 + substr_count($data, "\n" ));
    }

    protected function throwException( $message, $offset = 0 )
    {
        if( $offset > 0 )
            $errorLine = $this->searchLine($offset);
        else $errorLine = 0;

        throw new kore_template_compiler_exception($message, $errorLine,
            $this->_sourceName);
    }

    abstract public function getResult();

}
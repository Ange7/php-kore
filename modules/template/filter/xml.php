<?php
class kore_template_filter_xml implements kore_template_filter
{
    private $_schema = null;

    private $_errors = array();

    public function setSchema($schemaUrl)
    {
        $this->_schema = $schemaUrl;
    }

    /**
     * Procède à la validation du document
     *
     * @param kore_template_config $config
     * @param kore_template_context $context
     * @param string $contents
     */
    public function filter(kore_template_config $config,
        kore_template_context $context, $contents)
    {
        $encoding = ($config->charset === 'UTF-8') ? 'utf8' : 'latin1';

        $this->_errors = array();

        /*
         * XMLReader n'a pas de moyen "propre" de récupérer ses erreurs, on
         * détourne donc les erreurs vers notre propre méthode.
         */
        $htmlErrors = ini_set('html_errors', false);
        $previousErrorHandler = set_error_handler(array($this, '_catchError'));

        $xml = new XMLReader();
        $xml->xml($contents, $encoding);

        if (! $this->_schema)
            $xml->setParserProperty(XMLReader::VALIDATE, true);
        else
            $xml->setSchema($this->_schema);

        while ($xml->read());

        /*
         * On restaure la gestion des erreurs comme elle était.
         */
        if ($previousErrorHandler)
            set_error_handler($previousErrorHandler);
        if ($htmlErrors)
            ini_set('html_errors', true);

        $isValid = $xml->isValid();



        if (!$isValid) {
            header('Content-type: text/html');

            echo '<h1>XML error</h1>' . PHP_EOL;
            echo '<ul>';
            $lines = array();
            foreach( $this->_errors as $error ){
                $msg = $error->getMessage();
                $msg = str_replace('XMLReader::read(): ', '', $msg);

                $msg = htmlspecialchars($msg);

                if (preg_match('#:([0-9]+): #', $msg, $match)){
                    $lines[$match[1]] = true;
                    $msg = preg_replace('#(:)([0-9]+)(: )#',
                            '$1<a href="#$2" title="aller à la ligne $2">$2</a>$3',
                            $msg);
                }
                echo "<li>".$msg."</li>" . PHP_EOL;
            }
            echo '</ul>';

            echo '<ol>';
            foreach (explode("\n", $contents) as $idx => $line){
                $numLine = ($idx + 1);

                $style = '';
                if (isset($lines[$numLine]))
                    $style = ' style="font-weight:bold;color:red"';

                echo '<li id="'.$numLine.'"'.$style.'>'.
                        htmlspecialchars($line).'</li>' . PHP_EOL;
            }
            echo '</ol>';
            exit;
        }

        $xml->close();

        $this->_errors = array();

        return $contents;

    }

    public function _catchError($errno, $errstr, $errfile = NULL, $errline = NULL, $errcontext = NULL)
    {
        /*
         * Convert the $errno (= PHP error level) to a severity constant.
         */
        switch ($errno) {
            case E_ERROR:
            case E_USER_ERROR:
                $severity = kore_error::SEVERITY_ERROR;
                break;
            case E_NOTICE:
            case E_USER_NOTICE:
                $severity = kore_error::SEVERITY_NOTICE;
                break;
            case E_STRICT:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
                $severity = kore_error::SEVERITY_INFO;
                break;
            case E_WARNING:
            case E_USER_WARNING:
            default:
                $severity = kore_error::SEVERITY_WARNING;
        }

        $this->_errors[] = kore::$error->instanciateError($errstr, $severity,
                $errcontext, $errfile, $errline, $errno);
    }

}
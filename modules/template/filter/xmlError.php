<?php
class kore_template_filter_xmlError extends kore_error_reporter
{
    private $_list = array();

    public function report( array $destinations, kore_error_message $error )
    {
//        if( $error->severityCode <= kore_error::ALERT )
//            return parent::report($destinations, $error);

        $this->_list[] = $error;
    }

    public function getErrors()
    {
        return $this->_list;
    }
}
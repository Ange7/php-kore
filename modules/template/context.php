<?php
class kore_template_context extends kore_template_contextNode
{
    public function __construct()
    {
    }

    public function beforeHashComputation()
    {
    }

    public function afterHashComputation()
    {
    }


    /**
     * Concatène ensemble tous les arguments passés.
     * Le "." servant aux objets dans les templates, cette fonction est pour le
     * moment le seul moyen de faire une concaténation.
     *
     * TODO: trouver un moyen de prendre ça en charge au niveau du parseur du
     * moteur de template, afin d'éviter cette "bidouille".
     *
     * @param string $1
     * @param string $2
     * @param string ...
     * @param string $N
     *
     * @return string
     */
    public function concat()
    {
        $args = func_get_args();
        return implode('', $args);
    }

    /**
     * Retourne la page courante { @link kore_request_http::getPage }.
     *
     * @return string
     */
    public function getPage()
    {
        return kore_request_http::getPage();
    }

    /**
     * Retourne la date courant
     *
     * @return kore_date
     */
    public function now()
    {
        return kore_date::fromRequest();
    }

    /**
     * Encode une chaine afin d'être utilisée dans du JS
     *
     * @param string  $string
     * @param string  $quote
     * @return string
     */
    public function escapeJS( $string, $quote = '\'' )
    {
        return str_replace(
                array('\\', "\r", "\n", $quote),
                array('\\\\', '\\r', '\\n', '\\'.$quote),
                $string);
    }

    /**
     * Encode une chaine afin d'être utilisée dans des templates HTML
     *
     * @param  string $string
     * @param  number $encodeOption
     * @return string
     */
    public function escapeHTML( $string, $encodeOption = ENT_COMPAT )
    {
        if ($this->_conf->encodeEntities)
            return htmlentities($string, $encodeOption, $this->_conf->charset);

        return htmlspecialchars($string, $encodeOption, $this->_conf->charset);
    }

    /**
     * Retourne les informations de débugage (si actif).
     *
     * @return object
     */
    public function getDebugInfos()
    {
        if (!kore::$debug->isEnabled())
            return NULL;

        $debug = new stdClass();
        $debug->messages   = kore::$debug->getMessages();

        $debug->checkpoints = kore::$debug->benchGetCheckpoints();
        $debug->benchmarks  = kore::$debug->benchGetResults();

        return $debug;
    }

    /**
     * Récupère la date de modification d'un fichier, selon son URL ; les URL
     * non locales sont ignorées. Si le chemin commence par un slash alors fait
     * la recherche depuis kore::getWebDir(), sinon depuis kore::getScriptDir().
     *
     * @param  string $url
     * @return timestamp
     */
    protected function getFileTime( $url, $updatePageModificationTime = true, $trackError = true )
    {
        if( substr($url, 0, 1) === '/' )
            $url = kore::getWebDir() . ltrim($url, '/');
        else $url = kore::getScriptDir() . $url;

        if( ( $time = @ filemtime($url) ) === false ) {
            if ($trackError)
                kore::$error->track("template: file not found [$url]");
        } elseif( $updatePageModificationTime )
            kore_response_http::updateLastModificationTime($time);

        return $time;
    }

    /**
     * Recense une feuille de style à utiliser sur la page.
     *
     * @param string  $url
     * @param string  $title
     * @param string  $media
     * @param boolean $enableVersionning Indique si le timestamp de modification
     *                      sera intégré dans l'URL : permet une bien meilleure
     *                      mise en cache (avec un temps d'expiration assez
     *                      long), mais nécessite une règle de rewriting
     *                      spécifique.
     * @param mixed   $ieOnly            'IE 6' pour charger cette feuille
     *                      uniquement avec IE 6, 'IE' pour toutes les versions,
     *                      ou encore 'lt IE 7' pour les versions précédentes
     *                      à IE 7.
     * @param boolean $ignoreIfNotExists    Empêche l'ajout du tag si le fichier
     *                      n'éxiste pas.
     *
     */
    public function addStylesheet( $url, $title = NULL, $media = NULL, $enableVersionning = true, $ieOnly = false, $ignoreIfNotExists = false )
    {
        if( $enableVersionning === true ) {
            $tmpUrl = substr($url, 0, 7);
            if( $tmpUrl !== 'http://' and $tmpUrl !== 'https:/' ) {

                $time = $this->getFileTime($url, true, !$ignoreIfNotExists);
                if( $time === false && $ignoreIfNotExists )
                   return;

                if( $time !== false and ($pos = strrpos($url, '.')) !== false )

                    $url = substr($url, 0, $pos) .'.'.$time.'.'.
                            substr($url, $pos+1);
            }
        }

        if( ! isset($this->_include->stylesheets) )
            $this->_include->stylesheets = array();

        $this->_include->stylesheets[$url] = array(
            'title' => $title,
            'media' => $media,
            'ieonly' => $ieOnly,
            );
    }

    public function addStylesheetIfExists( $url, $title = NULL, $media = NULL, $enableVersionning = true, $ieOnly = false )
    {
        $this->addStylesheet($url, $title, $media, $enableVersionning, $ieOnly, true);
    }

    /**
     * Recense un script à utiliser sur la page.
     *
     * @param string  $url
     * @param string  $type Si non renseigné le type "text/javascript" sera utilisé.
     * @param boolean $enableVersionning    Indique si le timestamp de modification sera intégré
     *                                         dans l'URL : permet une bien meilleure mise en cache
     *                                         (avec un temps d'expiration assez long), mais necessite
     *                                         une règle de rewriting spécifique.
     * @param array   $attribs
     * @param boolean $ignoreIfNotExists    Empêche l'ajout du tag si le fichier
     *                      n'éxiste pas.
     */
    public function addScript( $url, $type = NULL, $enableVersionning = true, $attribs = NULL, $ignoreIfNotExists = false )
    {
        if( $enableVersionning === true ) {
            $tmpUrl = substr($url, 0, 7);
            if( $tmpUrl !== 'http://' and $tmpUrl !== 'https:/' ) {

                $time = $this->getFileTime($url, true, !$ignoreIfNotExists);
                if( $time === false && $ignoreIfNotExists )
                   return;

                if( $time !== false and ($pos = strrpos($url, '.')) !== false )

                    $url = substr($url, 0, $pos) .'.'.$time.'.'.
                            substr($url, $pos+1);
            }
        }

        if( $type === NULL )
            $type = 'text/javascript';

        if( ! isset($this->_include->scripts) )
            $this->_include->scripts = array();

        $this->_include->scripts[$url] = array('type' => $type);

        if( is_array($attribs) )
            $this->_include->scripts[$url] += $attribs;
    }

    public function addScriptIfExists( $url, $type = NULL, $enableVersionning = true, $attribs = NULL )
    {
        $this->addScript($url, $type, $enableVersionning, $attribs, true);
    }

    /**
     * If $value is not NULL, returns $value ; otherwise returns $otherValue.
     *
     * TODO: replace this on compilation by a true condition code.
     *
     * @param  mixed $value
     * @param  mixed $otherValue
     * @return mixed
     */
    public function ifnull($value, $otherValue)
    {
        if ($value === null)
            return $otherValue;
        return $value;
    }

    /**
     * If $value is not NULL, returns $otherValue ; otherwise returns NULL.
     *
     * TODO: replace this on compilation by a true condition code.
     *
     * @param  mixed $value
     * @param  mixed $otherValue
     * @return mixed
     */
    public function ifnotnull($value, $otherValue)
    {
        if ($value !== null)
            return $otherValue;
        return $value;
    }

    /**
     * Return NULL if $value == $secondValue, otherwise returns $value.
     *
     * TODO: replace this on compilation by a true condition code.
     *
     * @param  mixed $value
     * @param  mixed $secondValue
     * @return mixed
     */
    public function nullif($value, $secondValue)
    {
        if ($value == $secondValue)
            return null;
        return $value;
    }

    /**
     * If $condition is TRUE, returns $value, otherwise returns $otherValue.
     *
     * TODO: replace this on compilation by a true condition code.
     *
     * @param  boolean $condition
     * @param  mixed $value
     * @param  mixed $otherValue
     * @return mixed
     */
    public function ifthen($condition, $value, $otherValue = null)
    {
        if ($condition)
            return $value;
        return $otherValue;
    }

}

class kore_template_contextNode
{
    private $_internalId;

    public function __construct( $id = 'root' )
    {
        $this->_internalId = $id;
    }

    public function __get( $name )
    {
        $this->$name = new self($this->_internalId.'.'.$name);

        return $this->$name;
    }

    public function __call( $methodName, $args )
    {
        if (kore::$conf->template_triggerIfMethodNotExists !== false)
            kore::$error->track("template: method doesn't exits [$methodName]",
                    kore_error::SEVERITY_NOTICE, array(
                    'node' => $this->_internalId,
                    'method' => $methodName,
                    'args' => $args,
                    ));

        return new self($this->_internalId.'::'.$methodName.'()');
    }

    public function __toString()
    {
        if (kore::$conf->template_triggerIfPropertyNotExists !== false)
            kore::$error->track("template: property is not set [{$this->_internalId}]",
                    kore_error::SEVERITY_NOTICE);

        return "undefined({$this->_internalId})";
    }
}

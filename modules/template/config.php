<?php
class kore_template_config
{
    /**
     * Paramètres dynamiques : ces paramètres sont directement utilisé dans la
     * construction du nom du template compilé. Hors shortOpenTag, ils peuvent
     * donc être directement modifiés, sans risque.
     *
     * ************************************************************************
     */

    /**
     * Indique si PHP utilise actuellement les short_open_tag ou non.
     *
     * @var boolean
     */
    protected $_shortOpenTag;

    /**
     * Indique si le moteur de rendu HTML doit être utilisé (par défaut : oui).
     *
     * @var boolean
     */
    public $htmlParser = true;

    /**
     * Langue utilisée ; principalement utile lorsque des traductions sont
     * nécessaires.
     *
     * @var string
     */
    public $lang;

    /**
     * Charset utilisé.
     *
     * @var string
     */
    public $charset;

    /**
     * ContentType utilisé.
     *
     * @var string
     */
    public $contentType;

    /**
     * Indique si le moteur de template doit automatiquement désactiver
     * l'encodage des entities dans le cas de l'UTF8.
     *
     * @var boolean
     */
	public $encodeEntitiesIfUTF8 = false;

    /**
     * Active l'encodage des accents.
     *
     * @var boolean
     */
    public $encodeEntitiesIfNotUTF8 = true;



    /**
     * Paramètres de compilation, non dynamiques. En cas de modification de ces
     * paramètres le template _doit_ être recompilé, par le biais d'une mise à
     * jour de la version de la config {@see getVersion()};
     *
     * ************************************************************************
     */


    protected $_minifyTemplateCode = true;
    protected $_minifyTemplateNonPhpCode = true;

    protected $_allowedFunctions = array(
            'isset', 'empty', 'array', 'list', 'each',
            'is_bool', 'is_null', 'is_float', 'is_int', 'is_string',
            'is_object', 'is_array', 'is_a', 'is_numeric',
            'doubleval', 'floatval', 'intval', 'boolval', 'strval',
            'strlen', 'count', 'substr',
            'var_dump', 'print_r',
            'rawurlencode',
            'implode', 'explode',
            'number_format', 'round', 'floor', 'ceil', 'abs',
            'flush',
            'method_exists',
            'mb_strtolower', 'mb_strtoupper', 'mb_substr', 'mb_strlen',
            'ucfirst', 'ucwords', 'strtolower', 'strtoupper', 'lcfirst',

            /*
             * i18n functions
             */
            '_', 'gettext', 'ngettext', 'dcgettext', 'dcngettext', 'dgettext',
            'dngettext', 'printf', 'sprintf',
            );

    protected $_fakeOperators = array(
            'eq' => '==', 'ne' => '!=',
            'gt' => '>', 'lt' => '<', 'ge' => '>=', 'le' => '<=',
            'shr' => '>>', 'shl' => '<<',
            );

    // http://en.wikipedia.org/wiki/HTML_element
    protected $_dontAddLineFeedAfterCloseTag = array(
            'em', 'strong', 'q', 'cite', 'dfn', 'abbr', 'acronym',
            'code', 'samp', 'kbd', 'var',
            'sub', 'sup', 'del', 'ins',
            'a', 'img', 'br', 'map', 'area', 'object', 'param',
            'embed', 'noembed',
            'span',
            'b', 'i', 'big', 'small', 'tt',
            );






    /**
     * Paramètres d'exécution. Ils sont traités à chaque exécution, et n'ont
     * donc généralement pas besoin de recompiler le template.
     *
     * Toutefois si l'affichage est modifié, penser à en tenir compte dans le
     * calcul de l'ETag.
     *
     * ************************************************************************
     */

    /**
     * Liste de callbacks à appeler à la fin du traitement du template afin d'en
     * modifier le rendu.
     *
     * @var array
     */
    protected $_outputFilters = array();


    /**
     * Initialisation de la configuration.
     */
    public function __construct()
    {
        $this->_shortOpenTag = (bool) ini_get('short_open_tag');

        $this->lang = kore::$conf->get('lang', 'en');
        $this->charset = kore::$conf->response_charset;
        $this->contentType = kore::$conf->response_contentType;

    }

    /**
     * Retourne la version "statique" de cette configuration.
     *
     * @return string
     */
    public function getFullVersion()
    {
        if( kore::$conf->template_useSVNRevisionAsConfigVersion )
            return '$Rev$';

        return md5(serialize($this));
    }

    /**
     * Retourne la version "dynamique" de cette configuration.
     *
     * @return string
     */
    public function getPartialVersion()
    {
        $options = array(
            '_shortOpenTag',
            'htmlParser',
            'lang',
            'charset',
            'contentType',
            'encodeEntitiesIfUTF8',
            'encodeEntitiesIfNotUTF8',
            );

        $id = '';

        $confMask = 0; $optOffset = 0;

        foreach( $options as $optionName ) {
            $value = $this->$optionName;

            if( ! is_bool($value) )
                $id .= '.' . str_replace('/', '_', $value);
            elseif( $value === true )
                $confMask |= pow(2, $optOffset++);
        }

        return dechex($confMask) . $id;
    }


    public function __get($name)
    {
        $name = '_' . $name;
        if (isset($this->$name))
            return $this->$name;
        else
            kore::$error->track("template: unkown config variable [$name]",
                    kore_error::SEVERITY_NOTICE, $this);
        return NULL;
    }

    public function addOutputFilter(kore_template_filter $filter)
    {
        $this->_outputFilters[] = $filter;
    }

    public function getOutputFilters() {
        return $this->_outputFilters;
    }

    public function haveOutputFilter()
    {
        return ( count($this->_outputFilters) > 0 );
    }

    /**
     * Indicate if we need to encode entities
     *
     * @return boolean
     */
    public function encodeEntities()
    {
        $charset = str_replace('-', '', strtolower($this->charset));
        if( $charset === 'utf8' )
            return $this->encodeEntitiesIfUTF8;

        return $this->encodeEntitiesIfNotUTF8;
    }

}

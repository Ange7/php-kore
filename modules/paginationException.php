<?php
/**
 * Classe prenant en charge les exceptions issues du module kore_pagination.
 */
class kore_paginationException extends Exception
{
    /*
     * Contiendra l'URL corrigée et donc à utiliser en cas d'exception
     */
    protected $_redirectUrl;

    public function __construct($message, $redirectUrl)
    {
        $this->_redirectUrl = $redirectUrl;

        parent::__construct($message);
    }

    public function getRedirectUrl()
    {
        return $this->_redirectUrl;
    }
}
<?php

class kore_cache_file extends kore_cache_common
{
    const FLAG_COMPRESSED = 64;
    const FLAG_SERIALIZED = 128;

    protected $_tmpFile;

    protected function _getRelativeName($key)
    {
        $fileName = md5($key);
        return 'kcf-'.substr($fileName, 0, 3).'/'.substr($fileName, 3).'.kc';
    }

    protected function _getFileName($key)
    {
        return kore::getStorePath($this->_getRelativeName($key));
    }

    protected function _getTmpFile()
    {
        if( $this->_tmpFile === null )
            $this->_tmpFile =  kore::$conf->storagePath .'.kcf.'. kore::getUniqPid();

        return $this->_tmpFile;
    }

    protected function _decodeHeader($header)
    {
        if( strlen($header) !== 5 )
            return false;
        return unpack('C1flags/L1time', $header);
    }

    public function exists($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $filePath = $this->_getFileName($key);

        if( !$fp = @fopen($filePath, 'rb') )
            return false;
        $header = @fread($fp, 5);
        @fclose($fp);

        if( ($header = $this->_decodeHeader($header)) === false )
            return false;

        return !$this->_isExpired($header['time']);
    }

    protected function _isExpired($expirationTime)
    {
        $referenceTime = kore::requestTime();
        if( kore::$conf->cache_randomizeExpiration )
            $referenceTime -= rand(0, kore::$conf->cache_randomizeExpiration);
        return ( $expirationTime < $referenceTime );
    }

    public function get($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $filePath = $this->_getFileName($key);
        $data = @file_get_contents($filePath);
        if( $data === false or
            ($header = $this->_decodeHeader(substr($data, 0, 5))) === false ){
            $bench->setFinalStatus('not found');
            return false;
        }

        if( $this->_isExpired($header['time']) ){
            $bench->setFinalStatus('expired');
            return false;
        }

        if( $header['flags'] & self::FLAG_COMPRESSED )
            $data = @gzuncompress(substr($data, 5));
        else
            $data = substr($data, 5);

        if( $header['flags'] & self::FLAG_SERIALIZED )
            $data = @unserialize($data);

        return $data;
    }

    public function set($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        $flags = 0;

        if( !is_string($value) ){
            $flags |= self::FLAG_SERIALIZED;
            $value = serialize($value);
        }
        if( strlen($value) > 2043 ){
            $flags |= self::FLAG_COMPRESSED;
            $value = gzcompress($value);
        }

        $expirationTime = time() + $ttl;

        $value = pack('CL', $flags, $expirationTime) . $value;

        return kore::store($this->_getRelativeName($key), $value, 'kcf');
    }

    public function delete($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $filePath = $this->_getFileName($key);
        return @unlink($filePath);
    }

    public function deleteAll()
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $toDo = array();
        if( $dir = opendir(kore::$conf->storagePath) ){
            while( ( $file = readdir($dir) ) !== false ){
                if( $file === '.' or $file === '..' )
                    continue;
                if( preg_match('#^kcf-[a-f0-9]{3}$#', $file) ){
                    $fullPath = kore::$conf->storagePath . $file;
                    $newName = $fullPath . '.deleting.' . kore::getUniqPid();
                    if( @rename($fullPath, $newName) )
                        $toDo[] = $newName;
                }

            }
            closedir($dir);
        }

        foreach( $toDo as $fullPath )
            $this->_deltree(kore::$conf->storagePath . $file, true);
    }

    protected function _deltree($path, $removeDirs = false)
    {
        if( $dir = opendir($path) ){
            while( ( $file = readdir($dir) ) !== false ){
                if( $file === '.' or $file === '..' )
                    continue;
                $fullpath = $path.'/'.$file;
                if( !is_dir($fullpath) )
                    @unlink($fullpath);
                else self::_deltree($fullpath, $removeDirs);
            }
            closedir($dir);

            if( $removeDirs === true )
                @rmdir($path);
        }
    }

}

<?php

abstract class kore_cache_engine
{
    /**
     * Identiant interne du fichier de cache
     *
     * @var string
     */
    protected $cacheId;



    /**
     * Ajoute un petit délai aléatoire lors de la vérification de l'expiration
     * afin d'éviter que plusieurs reconstructions du cache soient lancées
     * simultanément.
     */
    protected $option_randomizeExpiration = NULL;

    /**
     * Test d'expiration du cache en fonction de la durée de validité
     *
     * @var integer
     */
    protected $option_lifeTime = NULL;

    /**
     * Test d'expiration du cache en fonction de la date de création
     *
     * @var string
     */
    protected $option_dateMask = NULL;

    /**
     * Indique si la date de modification de la page doit être mise à
     * jour en fonction de la date de modification du cache.
     *
     * @see kore_response_http
     * @var boolean
     */
    protected $option_updatePageDate = true;

    /**
     * Indique si la fonction get() doit vérifié l'expiration du cache.
     *
     * @var boolean
     */
    protected $checkExpiration = false ;

    /**
     * Date de dernière modification du cache.
     *
     * @var time
     */
    protected $modificationTime = NULL;

    /**
     * Liste des options authorisées.
     *
     * @var array
     */
    protected $authorizedOptions = array(
            'lifeTime' => NULL,
            'dateMask' => NULL,
            'randomizeExpiration' => NULL,
            'updatePageDate' => NULL,
            );

    /**
     * Création d'un cache
     *
     * @param string $cacheId	Identifiant du cache
     * @param array $options 	Options de mise en cache { @link kore_cache_disk::setOptions() }
     */
    public function __construct( $cacheId, $options = NULL )
    {
        $this->cacheId = $cacheId;

        $this->option_randomizeExpiration =
            kore::$conf->get('cache_defaultRandomizeExpiration', 5);

        $this->option_updatePageDate = ( kore::$conf->sapiName !== 'cli' and
                kore::$conf->response_httpUseModificationTime );

        if( $options !== NULL ) $this->setOptions( $options );
    }

    /**
     * Changement des options de mise en cache.
     * $options peut contenir ces valeurs :
     *     - lifeTime : entier, indique le nombre de secondes de validité
     *                  maximale du cache.
     *     - dateMask : masque utilisé avec la fonction date(), indiquant la
     *                  "clé" de validité du cache.
     *     - randomizeExpiration : indique si l'heure d'expiration du cache doit
     *                  être légèrement différée pour éviter de déclencher de
     *                  nombreuses reconstructions de cache simultanées.
     *     - updatePageDate : indique que la date de modification de la page
     *                  doit etre mise à jour en fonction de la date de
     *                  modification du cache (par défaut : true).
     *
     * @param array $options
     * @return boolean
     */
    public function setOptions( $options )
    {
        $return = true ;

        foreach( $options as $key => $value ) {
            if( array_key_exists($key, $this->authorizedOptions) ) {
                $opt = 'option_'.$key;
                $this->$opt = $value ;

            } else {
                kore::$error->track(get_class($this).' : unknown option [$key]');

                $return = false ;
            }
        }

        $this->checkExpiration = ( $this->option_lifeTime !== NULL or
                                   $this->option_dateMask !== NULL );

        return $return ;
    }

    /**
     * Retourne la date de dernière modification du cache.
     *
     * @return timestamp
     */
    public function getModificationTime()
    {
        return $this->modificationTime;
    }

    /**
     * Met à jour la date de dernière modification du cache.
     *
     * @param timestamp $time
     */
    protected function setModificationTime( $time )
    {
        $this->modificationTime = $time;

        if( $this->option_updatePageDate )
            self::updatePageDate( $this->modificationTime );
    }

    /**
     * Vérifie que le cache ne soit pas arrivé à expiration.
     *
     * @return boolean true si le cache est valide, false sinon.
     */
    public function checkExpires()
    {
        if( ( $cacheTime = $this->getModificationTime() ) === false )
            return false;

        /**
         * Pour éviter un appel système supplémentaire on évite d'utiliser
         * time() ; bien que cela puisse induire une mauvaise détection de
         * l'expiration d'un cache dans le cas de scripts vraiment longs.
         */
        $currentTime = kore::requestTime();

        /**
         * Décale légèrement l'heure courrante de manière alléatoire
         */
        if( $this->option_randomizeExpiration > 0 ){
            $currentTime -= rand(0, $this->option_randomizeExpiration);
        }

        if( $this->option_lifeTime !== NULL
            and $currentTime > $cacheTime + $this->option_lifeTime ) {

            return false;
        }

        if( $this->option_dateMask !== NULL and
            ( date( $this->option_dateMask, $currentTime ) !==
              date( $this->option_dateMask, $cacheTime ) ) ) {

            return false;
        }

        return true;
    }

    /**
     * Recupère le contenu du cache.
     *
     * Si $checkForExpiration est à true, alors vérifie aussi que
     * le cache ne soit pas arrivé à expiration.
     *
     * Retourne false si le cache n'existe pas ou est expiré.
     *
     * @param boolean $checkForExpiration
     * @return mixed
     */
    abstract public function get( $checkForExpiration = true );

    /**
     * Ecrit des données dans le cache.
     *
     * Si $options est renseigné alors les critères d'expirations du
     * cache seront ajouté dans le cache lui même, afin d'être appliqué
     * lors de la lecture si aucun autre critère n'est précisé.
     *
     * Retourne le nombre d'octets écrits ou bien false en cas d'erreur.
     *
     * @param mixed $contents
     * @param array $options
     * @return integer
     */
    abstract public function set( & $contents, $options = NULL );

    /**
     * Efface le cache $cacheId ou bien toute l'arborescence s'il s'agit
     * d'un dossier.
     *
     * Par défaut les dossiers sont vidés mais pas supprimés (pour éviter
     * les problèmes d'accès concurrents), ce comportement peut être changé
     * en affectant true à $removeDirs.
     *
     * @param string $cacheId
     */
    abstract public function remove( $options = NULL );

    /**
     * Met à jour la date de dernière modification de la page
     * { @link kore_response_http }.
     *
     * @param timestamp $time
     */
    protected static function updatePageDate( $time )
    {
        if( ( kore::$conf->sapiName !== 'cli' ) and $time !== false ) {
            kore_response_http::updateLastModificationTime( $time );
        }
    }

}

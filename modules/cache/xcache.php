<?php

class kore_cache_xcache extends kore_cache_common
{
    public function exists($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return xcache_isset($key);
    }

    public function get($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $res = xcache_get($key);
        if( $res === false )
            $bench->setFinalStatus('not found');
        return $res;
    }

    public function set($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        $bench = kore::$debug->benchInit('apc write', $key);
        return xcache_set($key, $value, $ttl);
    }

    public function delete($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return xcache_unset($key);
    }

    public function deleteAll()
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return xcache_unset_by_prefix('');
    }

    public function inc($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return xcache_inc($key, $step);
    }

    public function dec($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return xcache_dec($key, $step);
    }
}

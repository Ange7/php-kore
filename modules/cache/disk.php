<?php
/**
 * Module permettant le stockage de données sur le disque.
 *
 * @package  cache
 *
 * @global string  kore::$conf->storagePath     dossier où les fichiers seront stockés.
 * @global integer kore::$conf->storageDirMod   droits utilisés pour la création des dossiers.
 * @global integer kore::$conf->storageFileMod  si défini, droits appliqué au fichier après création
 *                                          (si possible, utilisez plutôt umask() à la place).
 * @global integer kore::$conf->storageFileGroup si défini, droits appliqué au fichier après création.
 */

/**
 * Stockage de données sur le disque
 *
 * @package cache
 */
class kore_cache_disk extends kore_cache_engine
{
    /**
     * Dossier de stockage des fichiers de cache.
     * (importé de kore::$conf->storagePath )
     *
     * @var string
     */
    protected $storagePath;

    /**
     * Indique si le contenu doit être automatiquement sérialisé.
     * (utilisé uniquement pour set() ).
     *
     * @var boolean
     */
    protected $option_serialize = true;

    /**
     *
     * @var boolean
     */
    protected $option_removeDirs = false;

    /**
     * Indique si le contenu doit être compressé ou non (nécessite serialize).
     * Si option_compress == null, alors laisse la classe décider de compresser
     * ou non ce cache.
     *
     * @var boolean
     */
    protected $option_compress = null;

    /**
     * Indique si le contenu doit être dédupliqué sur le disque.
     *
     * @var boolean
     */
    protected $option_deduplication = false;

    /**
     * Création d'un cache
     *
     * @param string $cacheId	Identifiant du cache
     * @param array $options 	Options de mise en cache { @link kore_cache_disk::setOptions() }
     */
    public function __construct( $cacheId, $options = NULL )
    {
        $cacheId = self::checkId($cacheId);

        $this->authorizedOptions['serialize'] = NULL;
        $this->authorizedOptions['removeDirs'] = NULL;
        $this->authorizedOptions['compress'] = NULL;
        $this->authorizedOptions['deduplication'] = NULL;

        parent::__construct($cacheId, $options);

        $this->storagePath = kore::$conf->storagePath;
    }

    /**
     * Nettoye le $cacheId de tout caractère spécial, afin de s'assurer que le cache ne sorte
     * pas de la zone de stockage.
     *
     * @param string $cacheId
     * @return string
     */
    static protected function checkId( $cacheId )
    {
        return substr( str_replace( array('/./','/../','//'),
                                    '/',
                                    '/' . $cacheId . '/' ), 1, -1 );
    }

    /**
     * Scinde un $cacheId en N (= $level) sous dossiers.
     *
     * @param  string  $cacheId
     * @param  integer $level
     * @param  boolean $right Indique qu'il faut commencer par les caractères de droite
     * @return string
     */
    public static function splitId( $cacheId, $level=1, $right = false, $partLength = 2 )
    {
        $len = strlen($cacheId);

        if( $len <= $level * $partLength ){
            kore::$error->track("cacheId too small to split [$cacheId]",
                    null, array('level'=>$level, 'partLength'=>$partLength));
            return $cacheId;
        }

        $directory = '';
        for( $i = 0; $i < $level; $i++ ){

            if( $right === true ) {
                $directory .= '/' . substr($cacheId, -$partLength);
                $cacheId = substr($cacheId, 0, -$partLength);
            } else {
                $directory .= '/' . substr($cacheId, 0, $partLength);
                $cacheId = substr($cacheId, $partLength);
            }
        }

        return $directory .'/'. $cacheId ;
    }

    /**
     * Crée les dossiers necessaires à la création du fichier de cache.
     *
     */
    public function createDirs()
    {
        if( ( $pos = strrpos( $this->cacheId, '/' ) ) === false )
            $directory = $this->storagePath ;
        else $directory = $this->storagePath . substr($this->cacheId, 0, $pos);

        @ mkdir($directory, kore::$conf->storageDirMod, true);
    }

    /**
     * Retourne le chemin du fichier correspondant au fichier de cache.
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->storagePath . $this->cacheId;
    }

    /**
     * Retourne la date de dernière modification du cache.
     *
     * @return timestamp
     */
    public function getModificationTime()
    {
        if( $this->modificationTime === NULL ) {
            $this->modificationTime = @ filemtime($this->storagePath .
					$this->cacheId);

            if( $this->option_updatePageDate )
                self::updatePageDate($this->modificationTime);
        }

        return $this->modificationTime;
    }


    /**
     * Recupère le contenu du cache.
     *
     * Si $checkForExpiration est à true, alors vérifie aussi que
     * le cache ne soit pas arrivé à expiration.
     *
     * Retourne false si le cache n'existe pas ou est expiré.
     *
     * @param boolean $checkForExpiration
     * @return mixed
     */
    public function get( $checkForExpiration = true )
    {
        $bench = kore::$debug->benchInit('cache read (disk)', $this->cacheId);

        $cachePath = $this->storagePath . $this->cacheId;
        $data = @ file_get_contents( $cachePath );

        if( $data === false )
            $size = NULL;
        else {
            $size = strlen($data);
            $data = @ unserialize($data);
        }

        if( $data === false or !isset($data[1]) ) {

            if( $size === NULL )
                $bench->setFinalStatus('not found');
            else {
                $bench->setFinalStatus('corrupted');

                kore::$error->track(__CLASS__.' : cache corrupted (unserialize)',
                        null, array('ID' => $this->cacheId));
            }

            return false;
        }

        $this->setModificationTime($data[1]);

        if( $checkForExpiration === true ) {

            /**
             * Applique les options du cache uniquement si aucune option
             * d'expiration n'a été fournie.
             */
            if( ( ! $this->checkExpiration ) and !empty($data[2]) )
                $this->setOptions($data[2]);

            if( $this->checkExpiration === true and
                $this->checkExpires() === false ) {

                $bench->setFinalStatus('expired');
                return false;
            }
        }

        /**
         * Vérifie la compression.
         */
        if( isset($data[3]) and $data[3] === true )
            $data[0] = unserialize(gzuncompress($data[0]));

        return $data[0];
    }

    /**
     * Ecrit des données dans le cache.
     *
     * Si $storedOptions est renseigné alors les critères d'expirations du cache
     * seront ajouté dans le cache lui même, afin d'être appliqué lors de la
     * lecture si aucun autre critère n'est précisé.
     *
     * Retourne le nombre d'octets écrits ou bien false en cas d'erreur.
     *
     * @param mixed $contents
     * @param array $storedOptions
     * @return integer
     */
    public function set( & $contents, $storedOptions = NULL )
    {
        $bench = kore::$debug->benchInit('cache write (disk)', $this->cacheId);

        static $tmpFile = NULL;
        if( $tmpFile === NULL )
            $tmpFile =  $this->storagePath .'.kore.'. kore::getUniqPid();

        $currentTime = time();
        $contentsHash = null;

        if( isset($storedOptions['serialize']) )
            $this->option_serialize = $storedOptions['serialize'];

        if( ! $this->option_serialize )
            $realContents = & $contents ;
        else {
            if( $storedOptions !== NULL ){
                $authorizedOptions = array(
                    'lifeTime' => NULL,
                    'dateMask' => NULL,
                    'randomizeExpiration' => NULL,
                    );
                foreach( $storedOptions as $key => $value ){
                    if(! array_key_exists( $key, $authorizedOptions ) ) {
                        kore::$error->track(__CLASS__." : not allowed option while storing [$key]",
                                null, array(
                                'ID' => $this->cacheId,
                                'storedOptions' => $storedOptions,
                                'authorizedOptions' => $authorizedOptions));

                        unset($storedOptions[$key]);
                    }
                }
            }

            $realContents = null;
            if( $this->option_compress === null and
                kore::$conf->cache_compressTrigger !== null ){

                $realContents = serialize($contents);
                if( strlen($realContents)>=kore::$conf->cache_compressTrigger )
                    $this->option_compress = true;
            }

            if( $this->option_compress !== true ){
                $realContents = array(&$contents);
            } else {
                if( $realContents === null )
                    $realContents = gzcompress(serialize($contents));
                else
                    $realContents = gzcompress($realContents);

                $realContents = array($realContents);
                $realContents[3] = true;
            }

            if( !empty($storedOptions) ) $realContents[2] = $storedOptions;

            /**
             * Si la déduplication est utilisée, calcul le hash de suite, avant
             * que la date de création ne soit insérée.
             */
            if( $this->option_deduplication )
                $contentsHash = md5(serialize($realContents));

            $realContents[1] = $currentTime;
            $realContents = serialize($realContents);
        }

        if( $this->option_updatePageDate )
            self::updatePageDate($currentTime);

        $result = false;

        $dedupFile = null;
        if( $this->option_deduplication !== false ){
            if( is_string($this->option_deduplication) )
                $dedupId = $this->option_deduplication;
            else {
                $dedupId = null;
                if( is_int($contents) )
                    $dedupId = 'i' . $contents;
                elseif( is_string($contents) )
                    $dedupId = 's' . $contents;
                elseif( is_array($contents) and ( count($contents) === 0 ) )
                    $dedupId = 'aEmpty';
                else {
                    if( $contentsHash === null ) $contentsHash = $realContents;
                    $dedupId = self::splitId($contentsHash);
                }
            }

            if( $dedupId !== null ){
                $bench->setFinalStatus('dedupId ' . $dedupId);

                $dedupId = '#dedup#/'.$dedupId;
                $dedupFile = $this->storagePath.$dedupId;

                if( @link($dedupFile, $tmpFile) ){
                    $result = @ rename($tmpFile, $this->storagePath
                            . $this->cacheId);
                    if( $result === false ){
                        $this->createDirs();
                        $result = @rename($tmpFile, $this->storagePath .
                                $this->cacheId);
                    }
                    if( $result === true )
                        return $result;
                    $dedupFile = null;
                }

                $tmpDedup = $tmpFile.'_d';
            }
        }

        if(! $fp = @ fopen($tmpFile, 'wb') )
            $errorMsg = 'can\'t open temp file';
        else {
            if( kore::$conf->storageFileMod )
                chmod($tmpFile, kore::$conf->storageFileMod);
            if( kore::$conf->storageFileGroup )
                chgrp($tmpFile, kore::$conf->storageFileGroup);

            $size = fwrite($fp, $realContents);
            fclose($fp);

            if( $dedupFile !== null and ! @link($tmpFile, $tmpDedup) )
                $dedupFile = null;

            $result = @ rename($tmpFile, $this->storagePath.$this->cacheId);
            if( $result === false ) {
                $this->createDirs();
                $result = @ rename($tmpFile, $this->storagePath.$this->cacheId);
            }

            if( $result !== true )
                $errorMsg = "can't move temp file";
            else {
                $result = $size;

                if( $dedupFile !== null ) {
                    /**
                     * TODO : enlever ce test une fois le système débuggé
                     */
                    $size = @filesize($dedupFile);
                    if( $size !== false and $size !== strlen($realContents) ) {
                        kore::$error->track("tentative de déduplication vers fichier inadapté",
                                null, array(
                                'tmpDedup' => $tmpDedup,
                                'dedupFile' => $dedupFile,
                                'cache' => $this,
                                ));
                    } elseif( !@rename($tmpDedup, $dedupFile) ){
                        $obj = new self($dedupId);
                        $obj->createDirs();
                        @rename($tmpDedup, $dedupFile) or @unlink($tmpDedup);
                    }
                }
            }
        }

        if ($result === false) {
            kore::$error->track(__CLASS__.": $errorMsg", null, array(
                    'ID'        => $this->cacheId,
                    'tmpFile'   => $tmpFile,
                    'PHP error' => kore::$error->getLastError(),
                    ));
            @unlink($tmpFile);
        }

        return $result;
    }

    /**
     * Efface le cache $cacheId ou bien toute l'arborescence s'il s'agit
     * d'un dossier.
     *
     * Par défaut les dossiers sont vidés mais pas supprimés (pour éviter
     * les problèmes d'accès concurrents), ce comportement peut être changé
     * en affectant true à $removeDirs.
     *
     * @param boolean $removeDirs
     */
    public function remove( $options = NULL )
    {
        if( $options !== NULL ) $this->setOptions( $options );

        return self::diskRemove( $this->cacheId, $this->option_removeDirs );
    }

    static protected function diskRemove( $cacheId, $removeDirs )
    {
        $cacheId = self::checkId( $cacheId );

        $path = kore::$conf->storagePath . $cacheId;

        if( ! is_dir($path) ) {
            @ unlink($path);
            return;
        }

        if( $removeDirs ){
            /**
             * S'il a été demandé d'effacer l'arborescence, commence par la
             * renommer sous un nom temporaire afin d'éviter tout problème
             * d'accès concurrent.
             */
            $suffix = '.kore.'. kore::getUniqPid();

            $oldPath = $path;
            $path .= $suffix;
            $cacheId .= $suffix;

            @ rename($oldPath, $path);
        }

        self::deltree($path, $removeDirs);
    }

    static private function deltree($path, $removeDirs)
    {
        if( $dir = opendir($path) ){
            while( ( $file = readdir($dir) ) !== false ){
                if( $file === '.' or $file === '..' )
                    continue;
                $fullpath = $path.'/'.$file;
                if( !is_dir($fullpath) )
                    @unlink($fullpath);
                else self::deltree($fullpath, $removeDirs);
            }
            closedir($dir);

            if( $removeDirs === true ) @rmdir($path);
        }
    }

}

<?php

class kore_cache_blackhole extends kore_cache_common
{
    public function get($key)
    {
        return false;
    }

    public function set($key, $value, $ttl = null)
    {
        return true;
    }

    public function delete($key)
    {
        return true;
    }

    public function deleteAll()
    {
        return true;
    }
}

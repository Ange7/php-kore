<?php

class kore_cache_memcache extends kore_cache_common
{
    /**
     *
     * @var memcached
     */
    protected $_mc;

    public function __construct( $memcache = null )
    {
        parent::__construct();

        if( $memcache !== null )
            $this->_mc = $memcache;
        else
            $this->_mc = new Memcache();

        if ($serverList = kore::$conf->kore_cache_memcache_servers) {
            foreach ($serverList as $server){
                $host = $server[0];
                $port = isset($server[1]) ? $server[1] : 11211;
                $persistent = isset($server[2]) ? $server[2] : false;
                $weight = isset($server[3]) ? $server[3] : 1;

                $this->addServer($host, $port, $persistent, $weight);
            }

            $this->_mc->setCompressThreshold(8000, 0.2);
        }

    }

    public function addServer( $host, $port = 11211, $persistent = true, $weight = 1, $timeout = 1, $retry_interval = 15, $status = true )
    {
        return $this->_mc->addServer($host, $port, $persistent, $weight, $timeout, $retry_interval, $status);
    }

    public function multiExists(array $keys)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'()');

        $results = array();
        foreach( $keys as $k => $v )
            $results[$k] = false;

        foreach( $this->_mc->get($keys) as $k => $v )
            $results[$k] = ( $v !== false );

        return $results;
    }

    public function get($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $res = $this->_mc->get($key);
        if( $res === false )
            $bench->setFinalStatus('not found');
        return $res;
    }

    public function multiGet(array $keys)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'()');

        return $this->_mc->get($keys);
    }

    public function set($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return $this->_mc->set($key, $value, 0, $ttl);
    }

    public function add($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return $this->_mc->add($key, $value, 0, $ttl);
    }

    public function delete($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->delete($key);
    }
    public function deleteAll()
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->flush();
    }

    public function inc($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->increment($key, $step);
    }

    public function dec($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->decrement($key, $step);
    }
}

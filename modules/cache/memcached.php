<?php

class kore_cache_memcached extends kore_cache_common
{
    /**
     *
     * @var memcached
     */
    protected $_mc;

    public function __construct( $memcached = null )
    {
        parent::__construct();

        if ($memcached !== null) {
            $this->_mc = $memcached;

        } elseif (kore::$conf->kore_cache_memcached_instance) {
            $this->_mc = kore::$conf->kore_cache_memcached_instance;

        } else {
            if (! ($serverList = kore::$conf->kore_cache_memcached_servers)) {
                $persistentId = 'kore';

            } else {
                /*
                 * Quand on utilise des connexions persistentes, Memcached
                 * semble conserver l'instance complète de la classe, y compris
                 * les serveurs.
                 * Il ne faut donc surtout pas refaire le addServers(), sous
                 * peine d'avoir un comportement très aléatoire ; ce qui du coup
                 * en empêche le changement.
                 *
                 * Afin de continuer à pouvoir changer la liste des serveurs en
                 * cas de besoin, Kore utilise donc un $persistentId basé sur la
                 * liste des serveurs.
                 */

                $persistentId = md5(serialize($serverList));
            }

            $this->_mc = new Memcached($persistentId);

            if (Memcached::HAVE_IGBINARY != false)
                $this->_mc->setOption(Memcached::OPT_SERIALIZER,
                        Memcached::SERIALIZER_IGBINARY);

            $this->_mc->setOption(Memcached::OPT_BINARY_PROTOCOL, true);

            $this->_mc->setOption(Memcached::OPT_CONNECT_TIMEOUT, 100);
            $this->_mc->setOption(Memcached::OPT_RETRY_TIMEOUT, 1);

            $this->_mc->setOption(Memcached::OPT_NO_BLOCK, true);
            $this->_mc->setOption(Memcached::OPT_RECV_TIMEOUT, 10000);
            $this->_mc->setOption(Memcached::OPT_SEND_TIMEOUT,  5000);

            $this->_mc->setOption(Memcached::OPT_LIBKETAMA_COMPATIBLE, true);

            /*
             * Comme indiqué plus haut, avec les connexions persistentes la
             * liste des serveurs est conservée d'une page à l'autre, il ne faut
             * donc pas ajouter de serveur s'ils ont déjà été définis.
             */
            if ($serverList and !$this->_mc->getServerList())
                $this->_mc->addServers($serverList);
        }
    }

    public function addServers( $servers )
    {
        return $this->_mc->addServers($servers);
    }

    public function multiExists(array $keys)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $results = array();
        foreach( $keys as $k => $v )
            $results[$k] = false;

        foreach( $this->_mc->getMulti($keys) as $k => $v )
            $results[$k] = ( $v !== false );

        return $results;
    }

    public function get($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        $res = $this->_mc->get($key);
        if( $res === false )
            $bench->setFinalStatus('not found');
        return $res;
    }

    public function multiGet(array $keys)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'()');

        return $this->_mc->getMulti($keys);
    }

    public function set($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return $this->_mc->set($key, $value, $ttl);
    }
    public function multiSet(array $values, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() ');
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return $this->_mc->setMulti($values, $ttl);
    }
    public function add($key, $value, $ttl = null)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);
        if( $ttl === null ) $ttl = kore::$conf->cache_defaultTTL;

        return $this->_mc->add($key, $value, $ttl);
    }

    public function delete($key)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->delete($key);
    }
    public function deleteAll()
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->flush();
    }

    public function inc($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->increment($key, $step);
    }
    public function dec($key, $step = 1)
    {
        $bench = kore::$debug->benchInit(__CLASS__, __FUNCTION__.'() '.$key);

        return $this->_mc->decrement($key, $step);
    }
}

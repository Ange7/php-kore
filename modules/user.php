<?php
/**
 * Prise en charge de l'identification d'un utilisateur.
 *
 * @package user
 */

/**
 * Classe représentant un utilisateur / membre pouvant s'identifier sur le site.
 *
 * Cette classe est fortement surchargeable afin de pouvoir adapter son
 * fonctionnement à un modèle de données différent.
 *
 * @package user
 */
class kore_user extends kore_db_mapping_element
{
    /**
     * Nom de la table dans laquelle sont stockés les utilisateurs.
     */
    const TABLE_NAME  = 'kore_user';

    /**
     * Nom de la table dans laquelle sont stockés les utilisateurs.
     */
    const TABLE_ALIAS = 'kore_user';

    /**
     * Nom de la colonne dans laquelle l'identifiant est stocké.
     */
    const PRIMARY_KEY = 'login';

    /**
     * Liste des champs principaux.
     */
    const MAIN_COLS = 'kore_user.login';

    /**
     * Classe permettant de gérer une collection d'utilisateurs.
     */
    const CLASS_COLLECTION = 'kore_user_userList';


    /**
     * Nom de la table dans laquelle sont stockés les mots de passe.
     * Peut être surchargée afin que les mots de passe ne soient pas stockés
     * dans la même table que les autres informations de l'utilisateur.
     */
    const TABLE_SHADOW = null;


    /**
     * Nom de la colonne dans laquelle le mot de passe (hashé) est stocké.
     */
    const PASS_COL = 'password';

    /**
     * Nom de la classe permettant de gérer les groupes d'utilisateur (hérite de
     * kore_user_group).
     */
    const CLASS_GROUP = 'kore_user_group';

    /**
     * Nom de la classe permettant de gérer les privileges (hérite de
     * kore_user_privilege).
     */
    const CLASS_PRIVILEGE = 'kore_user_privilege';


    const TABLE_REL_GROUP = 'kore_user_in_group';
    const KEY_REL_GROUP = 'login';

    const TABLE_REL_PRIVILEGE = 'kore_user_has_privilege';
    const KEY_REL_PRIVILEGE = 'login';

    /**
     * Nom de la variable de session pour l'identification à utiliser.
     *
     * Cette constante doit être surchargée si plusieurs systèmes
     * d'identifications utilisent kore_user
     */
    const SESSION_VAR_LOGIN = 'koreIdentifiedUser';

    /**
     * Utilisateur actuellement identifié.
     *
     * @var kore_user
     */
    static private $_currentUser = array();


    /**
     * Retourne l'utilisateur courant, ou null si aucun utilisateur est
     * identifié.
     *
     * @return kore_user
     */
    static public function getCurrentUser()
    {
        if (array_key_exists(static::SESSION_VAR_LOGIN, self::$_currentUser))
            return self::$_currentUser[static::SESSION_VAR_LOGIN];

        /*
         * Si l'utilisateur n'est pas identifié, on retourne null.
         */
        $user = null;
        self::$_currentUser[static::SESSION_VAR_LOGIN] = $user;

        /*
         * Si le cookie de session n'existe pas, on est certain qu'il n'y a pas
         * de session active ; donc pas la peine d'effectuer les traitements
         * liés à la session.
         */
        if (isset($_COOKIE[session_name()])) {
            if (kore::$session->isReadable() and
                isset(kore::$session->{static::SESSION_VAR_LOGIN})) {

                $idUser = kore::$session->{static::SESSION_VAR_LOGIN};

                $user = static::fromId($idUser);
                $user->_eventUserIdentifiedBySession();

                static::setCurrentUser($user);
            }
        }

        return $user;
    }

    /**
     * Vérifie que l'utilisateur courant possède le privilège en question, et
     * déclenche une exception si ce n'est pas le cas.
     *
     * @param  mixed $privilege
     */
    static public function currentUserNeedPrivilege( $privilege )
    {
        $user = static::getCurrentUser();

        if (!$user){
            $fakeUser = new static();
            $fakeUser->_exists = false;
            $fakeUser->_eventMustBeIdentified();

        } else {
            if (!$privilege instanceOf kore_user_privilege){
                $class = static::CLASS_PRIVILEGE;
                $privilege = $class::fromId($privilege);
            }

            if (!$user->hasPrivilege($privilege))
                $user->_eventHaveNotPrivilege($privilege);
        }
    }

    /**
     * Définie l'utilisateur comme étant l'utilisateur actuellement
     * identifié.
     *
     * @param kore_user $user
     */
    static public function setCurrentUser( kore_user $user )
    {
        self::$_currentUser[static::SESSION_VAR_LOGIN] = $user;

        /*
         * Changement d'identification ? Par précaution on purge le cache en
         * session.
         */
        static::clearSessionCache();

        kore::$session->{static::SESSION_VAR_LOGIN} = $user->getKey();
    }

    /**
     * Déconnecte l'utilisateur.
     */
    static public function disconnectCurrentUser()
    {
        static::clearSessionCache();
        unset(kore::$session->{static::SESSION_VAR_LOGIN});
        self::$_currentUser[static::SESSION_VAR_LOGIN] = null;
    }

    /**
     * Supprime les paramètres de l'utilisateur présents dans la session,
     * mais sans le déconnecter.
     */
    static public function clearSessionCache()
    {
    }


    static public function getTableKey($db)
    {
        return static::getTableAlias($db).'.'.
                $db->quoteKeyword(static::PRIMARY_KEY);
    }



    /**
     * Génère un salt de N caractères (parmis "./0-9A-Za-z"). Ceci est
     * particulièrement utile pour la fonction crypt().
     *
     * @param  integer $length
     * @return string
     */
    static public function makeSalt($length)
    {
        /*
         * Le paramètre $length est exprimé en caractères, encodés en base64.
         * On calcule donc le nombre de bytes nécessaires pour atteindre ce
         * chiffre.
         */
        $requiredChars = (int)ceil($length*6/8);

        if (function_exists('openssl_random_pseudo_bytes')) {
            $salt = openssl_random_pseudo_bytes($requiredChars);

        } elseif (function_exists('mcrypt_create_iv')) {
            $salt = mcrypt_create_iv($requiredChars, MCRYPT_DEV_URANDOM);

        } else {
            /*
             * Concatène autant de fois que nécessaire des caractères aléatoires
             * pour obtenir au moins $requiredChars caractères.
             */
            $salt = '';
            while (strlen($salt) < $requiredChars)
                $salt .= sha1(uniqid(rand(), true), true);
        }

        /*
         * Retourne un salt, n'utilisant que des caractères de type ./0-9A-Za-z
         */
        return str_replace(array('+', '='), array('.', '/'),
                base64_encode($salt));
    }

    /**
     * Génère le hash d'un mot de passe, avec salt aléatoire si celui ci n'est
     * pas fourni.
     *
     * @param  string $password
     * @param  string $salt
     * @return string
     */
    static public function crypt($password, $salt = null)
    {
        if ($salt === null)
            $salt = '$2a$12$'.static::makeSalt(22);
        return crypt($password, $salt);
    }


    static public function helperJoinRelGroup($db)
    {
        $rel = $db->quoteKeyword(static::TABLE_REL_GROUP);
        $targetClass = static::CLASS_GROUP;
        return array(
                "join $rel on ".static::getTableKey($db).
                " = $rel." . $db->quoteKeyword(static::KEY_REL_GROUP),
                "$rel." . $db->quoteKeyword($targetClass::KEY_REL_USER));
    }

    static public function helperJoinRelPrivilege($db)
    {
        $rel = $db->quoteKeyword(static::TABLE_REL_PRIVILEGE);
        $targetClass = static::CLASS_PRIVILEGE;
        return array(
                "join $rel on ".static::getTableKey($db).
                " = $rel." . $db->quoteKeyword(static::KEY_REL_PRIVILEGE),
                "$rel." . $db->quoteKeyword($targetClass::KEY_REL_USER));
    }

    /**
     * Crée un utilisateur (en base) et retourne l'instance de kore_user
     * associée.
     *
     * @param  string $idUser
     * @return kore_user
     */
    static public function create($idUser)
    {
        $obj = new static;

        /*
         * Vérifie la syntaxe de l'id avant d'instancier l'objet.
         */
        if (!$obj->checkIdSyntax($idUser))
            $obj->_eventRefusedLoginSyntax($idUser);

        $stmt = $obj->_stmtInsertUser();

        $stmtParams = array('idUser' => $idUser);
        if (!$stmt->execute($stmtParams))
            $this->_eventDBError($stmt, $stmtParams,
                    "error during user creation");

        $obj = static::fromId($idUser);
        $obj->_eventCreateUser();

        return $obj;
    }

    /**
     * Retourne une instance de kore_user en fonction de son identifiant.
     *
     * @param  string $idUser
     * @return kore_user
     */
    static public function fromId($idUser)
    {
        $obj = parent::fromId($idUser);

        /*
         * Vérifie la syntaxe de l'id avant de retourner l'objet.
         */
        if (!$obj->checkIdSyntax($idUser))
            $obj->_eventRefusedLoginSyntax($idUser);

        return $obj;
    }


    /**
     * Retourne la liste des propriétés de l'objet, ainsi que le nom de la
     * fonction permettant son chargement à la volée, un booléen indiquant si la
     * propriété est accessible en écriture, et enfin le type dans lequel
     * formater les données.
     *
     * @return array
     */
    static protected function _getProperties()
    {
        $group = static::CLASS_GROUP;
        $groupCollection = $group::CLASS_COLLECTION;

        $privilege = static::CLASS_PRIVILEGE;
        $privilegeCollection = $privilege::CLASS_COLLECTION;

        /*
         * Note : le mot de passe est volontaire exclus de cette liste. Le fait
         * de ne pas le mémoriser limite la probabilité de l'afficher par
         * inadvertance.
         */

        return array(
            /*
             * Identifiant de l'utilisateur (probablement la clé de la table).
             */
            static::PRIMARY_KEY    => static::_newProperty(false),

            /*
             * Relations avec d'autres classes.
             */
            'inGroups'         => static::_newRelation1N($groupCollection,     'fromUser'),
            'hasPrivileges'    => static::_newRelation1N($privilegeCollection, 'fromUser'),
            );
    }

    /**
     * Renvoi la connexion vers la base de données qui permet d'accéder au mot
     * de passe. Si non surchargée, il s'agit de la même connexion que
     * _getWriteDb().
     *
     * Cela permet donc d'utiliser une connexion spécifique pour les seules
     * méthodes réellement autorisées à manipuler le mot de passe, et interdire
     * l'accès à ce champ / cette table aux connexions ReadDb & WriteDb.
     *
     * @return kore_db_pdo
     */
    protected function _getAuthDb()
    {
        return $this->_getWriteDb();
    }

    /**
     * Charge les principaux champs de l'objet (cf méthode _getProperties() )
     */
    protected function _load()
    {
        try {
            parent::_load();
        } catch (RuntimeException $e) {
            $this->_eventSuspiciousException($e);
        }
    }

    /**
     * Retourne le nom de la table «shadow» à utiliser.
     *
     * @return string
     */
    protected function _getShadowTable()
    {
        if (static::TABLE_SHADOW === null)
            return static::TABLE_NAME;

        return static::TABLE_SHADOW;
    }


    /**
     * Crée le mot de passe de l'utilisateur.
     *
     * @param  string $password
     * @throws kore_user_exception
     */
    public function createPassword( $password = null )
    {
        if ($password === null)
            $password = static::makeSalt(24);

        /*
         * Vérifie que la syntaxe du mot de passe soit valide.
         */
        if (!$this->checkPasswordSyntax($password))
            $this->_eventRefusedPasswordSyntax($password);

        /*
         * Vérifie que le mot de passe soit suffisament complexe.
         */
        if (!$this->checkStrongPassword($password))
            $this->_eventPasswordTooWeak($password);

        $shadow = $this->_getShadowTable();

        /*
         * Si le mot de passe est dans la même table que le compte utilisateur,
         * alors un update suffit. Sinon il faut procéder à l'insert.
         */
        if (static::TABLE_NAME === $shadow)
            $stmt = $this->_stmtUpdatePassword();
        else
            $stmt = $this->_stmtInsertPassword();

        $stmt->execute(array(
            'idUser'         => $this->getKey(),
            'hashedPassword' => static::crypt($password),
            ));

        $this->_eventCreatePassword($password);

        return $password;
    }

    /**
     * Change le mot de passe de l'utilisateur.
     *
     * @param  string $password
     * @throws kore_user_exception
     */
    public function changePassword( $password )
    {
        /*
         * Vérifie que la syntaxe du mot de passe soit valide.
         */
        if (!$this->checkPasswordSyntax($password))
            $this->_eventRefusedPasswordSyntax($password);

        /*
         * Vérifie que le mot de passe soit suffisament complexe.
         */
        if (!$this->checkStrongPassword($password))
            $this->_eventPasswordTooWeak($password);

        $stmt = $this->_stmtUpdatePassword();

        $stmt->execute(array(
            'idUser'         => $this->getKey(),
            'hashedPassword' => static::crypt($password),
            ));

        $this->_eventChangedPassword($password);
    }

    /**
     * Vérifie la syntaxe de l'identifiant.
     *
     * @param  string $id
     * @return boolean
     */
    public function checkIdSyntax( $id )
    {
        $pattern = '#^[a-zA-Z0-9.?!,;\\-_&@\\\\/]{3,40}$#';
        return (bool) preg_match($pattern, $id);
    }

    /**
     * Vérifie la syntaxe du mot de passe : on accepte uniquement les chaines
     * comprises entre 1 et 4095 caractères, pour éviter un DoS sur les méthodes
     * de hashage.
     *
     * @param  string $password
     * @return boolean
     */
    public function checkPasswordSyntax( $password )
    {
        /*
         * On autorise uniquement les chaines.
         */
        if (!is_string($password))
            return false;

        $len = strlen($password);
        return ( ($len > 0) and ($len < 4096) );
    }

    /**
     * Vérifie que le mot de passe ne soit pas trop simple.
     *
     * Note : il est très probablement préférable d'avoir recours à l'extension
     * crack afin de s'assurer de la validité du mot de passe.
     * cf : http://fr.php.net/manual/en/function.crack-check.php
     *
     * @param  string $password
     * @return boolean
     */
    public function checkStrongPassword( $password )
    {
        return ( strlen($password) >= 8 );
    }

    /**
     * Vérifie qu'un mot de passe corresponde à celui qui est en base. De par
     * l'utilisation de crypt() avec un algorythme assez fort, ce traitement est
     * volontairement assez long (~ 300ms).
     *
     * Aussi, la méthode s'efforce de conserver ce temps d'exécution de manière
     * systématique, afin qu'un attaquant ne puisse pas distinguer les
     * différents cas.
     *
     * @param  string $password
     * @throws kore_user_exception
     */
    public function checkPasswordValidity( $password )
    {
        /*
         * On commence par vérifier l'existence de l'utilisateur.
         * Accessoirement, cela force le chargement du «bon» login, le fromId()
         * étant probablement insensible à la casse.
         */
        if (!$this->exists()){
            static::crypt('noop');
            $this->_eventUserUnknown();
        }

        /*
         * On vérifie la syntaxe du mot de passe afin de faire le moindre test.
         * En cas d'erreur, on se contente de générer une erreur «badPassword».
         */
        if (!$this->checkPasswordSyntax($password)){
            static::crypt('noop');
            $this->_eventUserBadPassword();
        }

        /*
         * On récupère le mot de passe (hashé) en base.
         */
        $stmt = $this->_stmtGetPassword();
        $params = array('idUser' => $this->getKey());

        $res = false;
        if (!$stmt->execute($params)){
            static::crypt('noop');
            $this->_eventDBError($stmt, $params, "DB error during auth");

        } else {
            $rows = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);

            if (($nbRows = count($rows)) > 1){
                /*
                 * Cette requête ne devrait retourner qu'une seule ligne. Si ce
                 * n'est pas le cas alors il s'agit probablement d'une injection
                 * SQL. Bien que peu probable avec PDO, dans le doute on ne va
                 * pas plus loin.
                 */

                static::crypt('noop');
                $this->_eventSuspiciousQueryResult($stmt, $params);
            }

            if ($nbRows !== 1) {
                static::crypt('noop');
                $this->_eventUserUnknown();

            } else {
                $storedPassword = current($rows);

                if (empty($storedPassword))
                    static::crypt('noop');
                else
                    $res = ($storedPassword === static::crypt($password, $storedPassword));

                if ($res === false)
                    $this->_eventUserBadPassword();
            }
        }

        /*
         * Normalement si on arrive ici c'est qu'on a été correctement identifié,
         * mais au cas où une des méthodes _event*() aurait été mal surchargée,
         * on re-teste la validité de l'identification.
         */
        if (!$res)
            throw new kore_user_exception("unexpected error during auth",
                    kore_user_exception::WRONG_CREDENTIAL);
    }


    /**
     * Indique si l'utilisateur possède le privilège en question.
     *
     * @param  mixed $privilege
     * @return boolean
     */
    public function hasPrivilege( $privilege )
    {
        if (!$privilege instanceOf kore_user_privilege){
            $class = static::CLASS_PRIVILEGE;
            $privilege = $class::fromId($privilege);
        }

        return $this->hasPrivileges->contains($privilege);
    }

    /**
     * Met à jour la liste des groupes auxquelles l'utilisateur appartient.
     */
    public function refreshGroups()
    {
        $this->_eraseFromCache('inGroups');
    }

    /**
     * Met à jour la liste des privileges de l'utilisateur.
     */
    public function refreshPrivileges()
    {
        $this->_eraseFromCache('hasPrivileges');
    }


    /**
     * Ajoute un privilege à l'utilisateur.
     *
     * @param kore_user_privilege $privilege
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function grant(kore_user_privilege $privilege, kore_date $startDate = null, kore_date $endDate = null)
    {
        $privilege->grantTo($this, $startDate, $endDate);
    }

    /**
     * Retire un privilege à l'utilisateur.
     *
     * @param kore_user_privilege $privilege
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function revoke(kore_user_privilege $privilege, kore_date $startDate = null, kore_date $endDate = null)
    {
        $privilege->revokeFrom($this, $startDate, $endDate);
    }




    /**
     * Retourne un PDOStatement permettant de récupérer uniquement le mot de
     * passe. Un seul paramètre sera passé : idUser
     *
     * @return PDOStatement
     */
    protected function _stmtGetPassword()
    {
        $db = $this->_getAuthDb();

        return $db->prepare(
           "select ".$db->quoteKeyword(static::PASS_COL)."
            from ".$db->quoteKeyword($this->_getShadowTable())."
            where ".$db->quoteKeyword(static::PRIMARY_KEY)." = :idUser");
    }

    /**
     * Retourne un PDOStatement permettant de créer le mot de passe.
     * Deux paramètres seront passés : idUser et hashedPassword
     *
     * @return PDOStatement
     */
    protected function _stmtInsertPassword()
    {
        $db = $this->_getAuthDb();

        return $db->prepare(
           "insert into ".$db->quoteKeyword($this->_getShadowTable())."
                (".$db->quoteKeyword(static::PRIMARY_KEY).",
                 ".$db->quoteKeyword(static::PASS_COL).")
            values (:idUser, :hashedPassword)");
    }

    /**
     * Retourne un PDOStatement permettant de modifier le mot de passe.
     * Deux paramètres seront passés : idUser et hashedPassword
     *
     * @return PDOStatement
     */
    protected function _stmtUpdatePassword()
    {
        $db = $this->_getAuthDb();

        return $db->prepare(
           "update ".$db->quoteKeyword($this->_getShadowTable())."
            set ".$db->quoteKeyword(static::PASS_COL)." = :hashedPassword
            where ".$db->quoteKeyword(static::PRIMARY_KEY)." = :idUser");
    }

    /**
     * Retourne un PDOStatement permettant de créer un utilisateur.
     * Un seul paramètre sera passé : idUser
     *
     * @param  array $additionnalCols
     * @return PDOStatement
     */
    protected function _stmtInsertUser(array $additionnalCols = array())
    {
        $db = $this->_getWriteDb();

        $cols = $db->quoteKeyword(static::PRIMARY_KEY);
        $params = ':idUser';

        foreach ($additionnalCols as $col){
            $cols .= ', ' . $db->quoteKeyword($col);
            $params .= ', :' . preg_replace('#[^A-Za-z0-9_\\-]#', '', $col);
        }
        return $db->prepare(
           "insert into ".$db->quoteKeyword(static::TABLE_NAME)."
                ($cols)
            values ($params)");
    }


    /**
     * Méthode appelée lors d'une erreur BDD durant l'identification.
     *
     * @param PDOStatement $stmt
     * @param array $stmtParams
     * @param string $message
     *
     * @throws kore_user_exception
     */
    protected function _eventDBError($stmt, $stmtParams, $message = null)
    {
        if ($message === null)
            $message = 'DB error';

        throw new kore_user_exception($message, kore_user_exception::SQL_ERROR);
    }

    /**
     * Méthode appelée lorsque qu'une requête retourne un résultat anormal.
     * Il s'agit probablement d'un problème de sécurité, à tracer ?
     *
     * @param PDOStatement $stmt
     * @param array $stmtParams
     *
     * @throws kore_user_exception
     */
    protected function _eventSuspiciousQueryResult($stmt, $stmtParams)
    {
        throw new kore_user_exception("bad query result",
                kore_user_exception::BAD_QUERY_RESULT);
    }

    /**
     * Méthode appelée lorsque qu'un traitement retourne un résultat anormal.
     * Il s'agit probablement d'un problème de sécurité, à tracer ?
     *
     * @param Exception $exception
     */
    protected function _eventSuspiciousException(Exception $exception)
    {
        /*
         * Laisse l'exception se poursuivre.
         */
        throw $exception;
    }

    /**
     * Méthode appelée lors de l'identification d'un utilisateur via session.
     */
    protected function _eventUserIdentifiedBySession()
    {
    }

    /**
     * Méthode appelée lors de l'identification d'un utilisateur par mot de
     * passe.
     */
    protected function _eventUserIdentifiedByPassword()
    {
    }

    /**
     * Méthode appelée lors de l'échec d'identification d'un utilisateur par mot
     * de passe, alors que l'utilisateur n'existe pas.
     */
    protected function _eventUserUnknown()
    {
        throw new kore_user_exception("Bad login or password",
                kore_user_exception::WRONG_CREDENTIAL);
    }

    /**
     * Méthode appelée lors de l'échec d'identification d'un utilisateur par mot
     * de passe.
     */
    protected function _eventUserBadPassword()
    {
        throw new kore_user_exception("Bad login or password",
                kore_user_exception::WRONG_CREDENTIAL);
    }

    /**
     * Méthode appelée lorsque qu'un login est refusé pour cause de syntaxe
     * invalide.
     *
     * @param  string $login
     * @throws kore_user_exception
     */
    protected function _eventRefusedLoginSyntax($login)
    {
        throw new kore_user_exception("id is not syntaxically valid",
                    kore_user_exception::BAD_ID_SYNTAX);
    }

    /**
     * Méthode appelée lorsqu'un mot de passe est refusé pour cause de
     * syntaxe invalide.
     *
     * @param  string $password
     * @throws kore_user_exception
     */
    protected function _eventRefusedPasswordSyntax($password)
    {
        throw new kore_user_exception("password is not syntaxically valid",
                kore_user_exception::BAD_PASSWORD_SYNTAX);
    }

    /**
     * Méthode appelée lorsqu'un mot de passe est refusé car trop simple.
     *
     * @param  string $password
     * @throws kore_user_exception
     */
    protected function _eventPasswordTooWeak($password)
    {
        throw new kore_user_exception("password is too weak",
                kore_user_exception::PASSWORD_TOO_WEAK);
    }

    /**
     * Méthode appelée lors de la création d'un utilisateur.
     */
    protected function _eventCreateUser()
    {
    }

    /**
     * Méthode appelée lorsque la page nécessite que l'utilisateur soit
     * identifié, et que ce n'est pas le cas.
     *
     * @throws kore_user_exception
     */
    protected function _eventMustBeIdentified()
    {
        throw new kore_user_exception("Must be identified",
                kore_user_exception::MUST_BE_IDENTIFIED);
    }

    /**
     * Méthode appelée lorsque l'utilisateur tente d'accéder à une fonction pour
     * laquelle il n'a pas les droits.
     *
     * @params kore_user_privilege $privilege
     * @throws kore_user_exception
     */
    protected function _eventHaveNotPrivilege( kore_user_privilege $privilege )
    {
        throw new kore_user_exception("Insuffisant privileges",
                kore_user_exception::INSUFFISANT_PRIVILEGES);
    }

    /**
     * Méthode appelée lors de la création du premier mot de passe.
     */
    protected function _eventCreatePassword($newPassword)
    {
    }

    /**
     * Méthode appelée lors du changement de mot de passe.
     */
    protected function _eventChangedPassword($newPassword)
    {
    }


}

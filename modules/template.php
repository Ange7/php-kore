<?php

class kore_template
{
    /**
     * Indique si le template a déjà été initialisé ou non.
     *
     * @var boolean
     */
    private $_initialized = false;

    /**
     * Nom du fichier de template à utiliser, s'il y en a un.
     *
     * @var string
     */
    protected $_templateFile;

    /**
     * Code du template à utiliser, plutôt qu'un fichier.
     *
     * @var string
     */
    protected $_templateCode;

    /**
     * Indique si le code du template est déjà compilé ou non.
     *
     * @var boolean
     */
    protected $_templateCodeIsCompiled = false;

    /**
     * Chemin de stockage du template source, s'il s'agit d'un fichier.
     *
     * @var string
     */
    private $_sourcePath;

    /**
     * Chemin de stockage du template compilé, si le template source est dans un
     * fichier.
     *
     * @var string
     */
    protected $_compiledPath;

    /**
     * Nom de la classe à utiliser par défaut pour la configuration du template.
     *
     * @var string
     */
    protected $_defaultConfigClass;

    /**
     * Nom de la classe à utiliser par défaut pour le contexte du template.
     *
     * @var string
     */
    protected $_defaultContextClass;

    /**
     * Configuration utilisée pour le traitement du template.
     *
     * @var kore_template_config
     */
    public $config;

    /**
     * Context dans lequel traiter le template.
     *
     * @var kore_template_context
     */
    public $context;


    /**
     * Crée une instance de kore_template
     *
     * @param $templateFile string
     * @param $context      kore_template_context / string
     * @param $config       kore_template_config / string
     * @return kore_template
     */
    public function __construct( $templateFile = NULL, $context = NULL, $config = NULL )
    {
        if( $templateFile !== NULL ){
            /**
             * Remplace les caractères pouvant nuire à la sécurité (ou encore à
             * l'efficacité du cache).
             */
            $templateFile = str_replace(array('/./', '../', '//'), '/',
                    $templateFile);

            /**
             * S'assure que seuls les fichiers prévus seront chargés par le
             * moteur de template.
             */
             if ( strpos($templateFile, '.tpl') === false )
                $templateFile .= '.tpl.html';

            $this->_templateFile = $templateFile;
        }

        if( $config === NULL ) {
            if( ( $config = $this->_defaultConfigClass ) === NULL )
                $config = kore::$conf->get('template_configClass',
                        'kore_template_config');
        }
        if( is_string($config) ) $config = new $config;
        $this->config = $config;

        if( $context === NULL ) {
            if( ( $context = $this->_defaultContextClass ) === NULL )
                $context = kore::$conf->get('template_contextClass',
                        'kore_template_context');
        }
        if( is_string($context) ) $context = new $context;
        $this->context = $context;

        kore::$conf->get('template_lifeTime', 86400*7);

        /**
         * Inclus de suite les fichiers nécessaire au bon fonctionnement du
         * template. Ainsi d'autres fichiers pourront facilement être ajoutés
         * par la suite, et la génération de l'ETag pourra tenir compte des
         * différentes versions utilisées.
         */
        $this->includeContents();
    }

    /**
     * Inclus les fichiers externes au template.
     */
    protected function includeContents()
    {
        /**
         * Par défaut ne fait rien.
         */
    }

    /**
     * Initialise le template avant utilisation.
     * Cette méthode est appelée au dernier moment.
     */
    protected function initTemplate()
    {
        if( $this->_initialized === true ) return;
        $this->_initialized = true;

        $this->initContext();

        $bench = kore::$debug->benchInit('template', 'checkVersion');

        /**
         * S'il s'agit d'un template fichier, prépare le cache.
         */
        if( $this->_templateFile !== null ){
            /*
             * Here, we don't want that errors hidden by the at sign (@) are
             * tracked by the error handler, since they are expected ones.
             */
            $errorTracking = kore::$conf->error_trackNonReportedErrors;
            kore::$conf->error_trackNonReportedErrors = false;

            /**
             * Calcule le chemin de stockage du cache.
             */
            if( ( $tplDir = kore::$conf->templatePath ) === NULL )
                $tplDir = kore::getWebDir();

            $root = trim($tplDir, '/');
            $rootPrefix = '/'.str_replace('/', '_', $root).'/';

            $this->_compiledPath = 'compiledTemplates/'.str_replace(
                    '/'.$root.'/', $rootPrefix, $this->getSourcePath()).'.'.
                    $this->config->getPartialVersion().'.html';

            $cacheTime = @filemtime(kore::getStorePath($this->_compiledPath));
            $templateVersion = $cacheTime;

            /**
             * Vérifie la validité du cache.
             */
            if( $cacheTime ){
                $referenceTime = kore::requestTime();
                if( kore::$debug->getEnvironment() !== 'dev' )
                    $referenceTime -= rand(0, 10);

                if( $cacheTime + kore::$conf->template_lifeTime < $referenceTime  )
                    $cacheTime = false;
                else {
                    $sourceTime = @filemtime($this->getSourcePath());

                    /**
                     * Si le template n'existe pas, utilise un template vide.
                     */
                    if( $sourceTime === false ) {
                        /*
                         * Restore error tracking.
                         */
                        kore::$conf->error_trackNonReportedErrors = $errorTracking;

                        kore::$error->track("template file not found [{$this->_templateFile}]",
                                null, $this->getSourcePath());

                        $this->setTemplateCode('');
                        $this->_templateCodeIsCompiled = true;
                        return;
                    }

                    /**
                     * Si le template source est plus récent que le cache, alors
                     * force la recompilation.
                     */
                    if( $sourceTime > $cacheTime )
                        $cacheTime = false;
                }
            }

            /*
             * Restore error tracking.
             */
            kore::$conf->error_trackNonReportedErrors = $errorTracking;


            /**
             * Recompile le template.
             */
            if( $cacheTime === false ) {
                $compiler = new kore_template_compiler($this->config, $this->context);

                try {
                    $compiler->loadContentsFromFile($this->getSourcePath());
                    $templateCode = $compiler->getResult();

                    kore::store($this->_compiledPath, $templateCode, 'tpl');

                    /**
                     * On vient de passer du temps à compiler le template, on va
                     * maintenant l'exécuter directement sans avoir à
                     * déclencher une compilation du cache d'opcode (qui
                     * pourrait d'ailleurs ignorer la mise en cache à cause de
                     * la date de modification récente).
                     */
                    $this->setTemplateCode($templateCode);
                    $this->_templateCodeIsCompiled = true;

                    $templateVersion = kore::requestTime();

                } catch( kore_template_compiler_exception $e ) {

                    kore::$error->trackException($e, E_USER_WARNING);

                    /**
                     * En cas d'erreur, hors environnement de dev, utilise une
                     * "vieille" version du cache si disponible, sinon retourne
                     * un template vide.
                     */
                    if( kore::$debug->getEnvironment() === 'dev' ){
                        $templateVersion = kore::requestTime();
                        $this->setTemplateCode(htmlspecialchars($e->getMessage()));
                        $this->_templateCodeIsCompiled = true;
                    } elseif( !$templateVersion ){
                        $this->setTemplateCode('');
                        $this->_templateCodeIsCompiled = true;
                    }
                }
            }
        }

        /**
         * Si on a le code du template en mémoire et que celui-ci n'est pas
         * compilé, alors le compile maintenant.
         */
        if( $this->_templateCode !== null and !$this->_templateCodeIsCompiled ){
            $this->_templateCodeIsCompiled = true;
            $templateVersion = kore::requestTime();

            try {
                $compiler = new kore_template_compiler($this->config, $this->context);
                $compiler->setContents($this->_templateCode);

                $this->_templateCode = $compiler->getResult();

            } catch( kore_template_compiler_exception $e ) {
                kore::$error->trackException($e, E_USER_WARNING);
                $this->setTemplateCode('');
            }
        }

        $this->context->_conf->templateVersion = $this->config->getFullVersion()
                .'/'. $templateVersion;
    }

    /**
     * Plutôt que d'utiliser un fichier comme source de template, indique
     * directement le code source à utiliser.
     */
    public function setTemplateCode($code)
    {
        $this->_templateCode = $code;
        $this->_templateCodeIsCompiled = false;

        $this->_templateFile = null;
        $this->_compiledPath = null;
    }

    /**
     * Initialise le contexte, en fonction de la configuration.
     */
    protected function initContext()
    {
        $bench = kore::$debug->benchInit('template', 'initContext');

        /**
         * Transmet une partie de la conf dans le contexte.
         */
        $c = $this->context->_conf;

        $c->lang = $this->config->lang;
        $c->charset = $this->config->charset;
        $c->contentType = $this->config->contentType;
        $c->isXML = ( $this->config->contentType === 'application/xhtml+xml' );
        $c->templateVersion = null;
        $c->debug = kore::$debug->isEnabled();
        $c->encodeEntities = $this->config->encodeEntities();

        /**
         * Initialise des variables de dernière minute.
         */
        $this->context->beforeHashComputation();
    }

    /**
     * Raccourci pour envoyer directement la réponse via le template.
     */
    public function send()
    {
        $response = new kore_response_template();
        $response->send($this);
    }

    /**
     * Affiche directement le résultat du template.
     *
     * @param  $returns boolean Indique que le résultat doit être retourné
     *          plutôt qu'affiché.
     * @return boolean
     */
    public function display( $returns = false )
    {
        $this->initTemplate();

        /**
         * Initialise des variables de dernière minute.
         */
        $this->context->afterHashComputation();

        if( $returns === false )
            kore::$debug->benchCheckPoint('main', 'display');


        $filterOutput = $this->config->haveOutputFilter();

        $buffering = ( $returns or $filterOutput );
        $result = false;

        if( $buffering === true )
            ob_start();

        /**
         * Execute le template
         */
        if( $this->_compiledPath !== NULL )
            $result = $this->_includeCompiledCode();
        else
            $result = $this->_executeCompiledCode();

        /**
         * Traite le résultat, si besoin
         */
        if( $buffering === true ) {
            if( $result === true )
                $result = ob_get_contents();
            ob_end_clean();

            if( $filterOutput === true )
                $this->_applyOutputFilters($result);

            if( $returns === false )
                echo $result;
        }

        return $result;
    }

    /**
     * L'inclusion est isolée dans une méthode dédiée afin d'éviter que le
     * template puisse compromettre des variables internes au processus
     * de chargement.
     *
     * @return boolean
     */
    private function _includeCompiledCode()
    {
        $path = kore::getStorePath($this->_compiledPath);
        $c = $this->context;
        return (bool) ( include $path );
    }

    /**
     * L'exécution est isolée dans une méthode dédiée afin d'éviter que le
     * template puisse compromettre des variables internes au processus
     * de chargement.
     *
     * @return boolean
     */
    private function _executeCompiledCode()
    {
        $code = $this->_templateCode;
        $c = $this->context;

        $return = eval("?>\n" . $code);

        if( $return === null ) $return = true;

        return $return;
    }

    /**
     * Applique les filtres de sortie indiqués dans la configuration.
     *
     * @param  $contents string
     */
    protected function _applyOutputFilters( & $contents )
    {
        $outputFilters = $this->config->getOutputFilters();
        foreach(  $outputFilters as $filter ){
            $contents = call_user_func(array($filter, 'filter'), $this->config, $this->context,
                $contents);
        }
    }

    /**
     * Retourne la version du template (fichier + configuration).
     * Cela est surtout utilisé pour calculer un ETag avant affichage de la
     * page.
     *
     * @return string
     */
    public function getVersion()
    {
        $this->initTemplate();

        return $this->context->_conf->templateVersion;
    }

    /**
     * Construit le chemin d'accès complet au fichier source du template
     *
     * @return string
     */
    public function getSourcePath()
    {
        if( $this->_sourcePath !== NULL ) return $this->_sourcePath;

        $tplDir = kore::$conf->templatePath;

        if( substr($this->_templateFile, 0, 1) === '/' ) {
            if( $tplDir === NULL )
                $tplDir = kore::getWebDir();
            $this->_sourcePath = $tplDir . ltrim($this->_templateFile, '/');
        } else {
            if( $tplDir === NULL )
                $tplDir = kore::getScriptDir();
            else {
                $relDir = kore::getRelativeScriptDir();
                if( substr($relDir, 0, 1) !== '/' )
                    $tplDir .= $relDir;
                else $tplDir = $relDir;
            }

            $this->_sourcePath = $tplDir . $this->_templateFile;
        }

        return $this->_sourcePath;
    }

    /**
     * Génère un hash en fonction des propriétés "publiques" du contexte.
     *
     * Cela permet d'établir à un moindre cout un ETag pour la page ; fiable
     * tant qu'aucune donnée "dynamique" ne soit obtenue après cette étape.
     *
     * @return string
     */
    public function buildContextHash()
    {
        $this->initTemplate();

        /**
         * On génère le hash progressivement pour éviter d'avoir à faire un
         * serialize() sur tout $this->context qui pourrait provoquer une
         * consommation mémoire excessive.
         */
        $hash = hash_init('md5');
        $this->_hashContext($hash, $this->context);
        return hash_final($hash);
    }

    /**
     * Methode permettant de générer récursivement le hash du contexte.
     *
     * @param  ressource   $hash
     * @param  traversable $values
     */
    private function _hashContext(&$hash, $values)
    {
        foreach ($values as $property => $value) {
            hash_update($hash, $property);

            if (is_scalar($value))
                hash_update($hash, $value);
            elseif (is_array($value) or $value instanceOf traversable)
                $this->_hashContext($hash, $value);
            else
                hash_update($hash, serialize($value));
        }
    }


}

<?php
/*
 * Before PHP 5.5 DateTimeImmutable is not available, so we use our PHP
 * implementation.
 */
if (!class_exists('DateTimeImmutable', false))
    class_alias('kore_php_DateTimeImmutable', 'DateTimeImmutable');

class kore_date
{
    const year = 32,
          month = 16,
          day = 8,
          hour = 4,
          minute = 2,
          second = 1;

    const monday = 1,
          tuesday = 2,
          wednesday = 3,
          thursday = 4,
          friday = 5,
          saturday = 6,
          sunday = 7;


    /**
     * Crée un objet date à partir d'une date YYYY-MM-DD [HH:MM:SS].
     * Si la date fournie est NULL, alors retourne NULL.
     *
     * @param  $date string
     * @return kore_date
     */
    public static function fromSQL($date)
    {
        if ($date === null) return null;

        $obj = new static();

        $len = strlen($date);
        if ($len === 10)
            $obj->sql = $date;
        elseif ($len === 19)
            $obj->sqltime = $date;

        return $obj;
    }

    /**
     * Créé un objet kore_date à partir d'un objet DateTimeImmutable
     *
     * TODO: starting with PHP 5.5, the InvalidArgumentException exception will
     * be replaced by a true DateTimeInterface parameter check.
     *
     * @param  DateTimeImmutable $date
     * @return kore_date
     */
    static public function fromDateTime(/* DateTimeInterface */ $date)
    {
        $obj = new static();

        if ($date instanceof DateTimeImmutable) {
            $obj->datetime = $date;
        } elseif ($date instanceof DateTime) {
            $obj->datetime = new DateTimeImmutable(
                    $date->format('Y-m-d H:i:s'),
                    $date->getTimezone());
        } else {
            kore::$error->track(
                    __METHOD__ . " only accept parameters of type DateTimeInterface",
                    kore_error::SEVERITY_ERROR
                    );
        }

        return $obj;
    }

    /**
     * Crée un objet date à partir d'un timestamp Unix.
     *
     * @param  $date integer
     * @return kore_date
     */
    public static function fromTimestamp($date)
    {
        $obj = new static();
        $obj->timestamp = (int) $date;

        return $obj;
    }

    /**
     * Crée un objet date à partir d'une chaine au format «TIME» MySQL
     * HH:MM:SS
     *
     * @param string $time
     * @return kore_date
     */
    public static function fromTime($time)
    {
        $d = explode(':', $time);
        $hour = $d[0];
        $minute = $d[1];
        $second = $d[2];

        $obj = static::make($hour, $minute, $second, 1,1,2000);

        return $obj;
    }

    /**
     * Crée un objet date de toute pièce.
     * Note : l'ordre des paramètres est le même que celui de mktime.
     *
     * @param  $hour
     * @param  $minute
     * @param  $second
     * @param  $month
     * @param  $day
     * @param  $year
     * @return kore_date
     */
    public static function make($hour, $minute, $second, $month, $day, $year)
    {
        /*
         * Handle dual digit years, like the «mktime()» function do.
         */
        if ($year <= 69)
            $year += 2000;
        elseif ($year <= 99)
            $year += 1900;

        $dt = new DateTimeImmutable();
        $dt = $dt->setTime($hour, $minute, $second)
                 ->setDate($year, $month, $day);
        return static::fromDateTime($dt);
    }


    /**
     * Crée un objet date à partir d'une expression régulière
     * exemples d'expression avec sous-masques nommés :
     * #^(?<day>\d{1,2})/(?<month>\d{1,2})/(?<year>\d{2,4})$#
     * #^(?<day>\d{1,2})/(?<month>\d{1,2})/(?<year>\d{2,4})(?:\\s+(?<hour>\d{1,2}):(?<minute>\d{1,2})(?::(?<second>\d{1,2}))?)?$#
     *
     * Si l'expression régulière ne correspond pas, une exception sera
     * levée.
     * Si la date obtenue ne correspond pas à la date fournie, une
     * exception sera également levée, sauf si $ignoreErrors est à true.
     *
     * @param   string  $date
     * @param   string  $preg
     * @param   boolean $ignoreModification
     * @return  kore_date
     * @throws  Exception
     */
    static public function fromReg($date, $preg, $ignoreModification = false)
    {
        if (!preg_match($preg, $date, $t))
            throw new Exception("this string doesn't match the regular expression");

        $hour = $minute = $second = 0;
        $month = $day = 1;
        $year = 0;

        if (isset($t['hour']))     $hour = $t['hour'];
        if (isset($t['minute']))   $minute = $t['minute'];
        if (isset($t['second']))   $second = $t['second'];
        if (isset($t['month']))    $month = $t['month'];
        if (isset($t['day']))      $day = $t['day'];
        if (isset($t['year']))     $year = $t['year'];

        $obj = static::make($hour, $minute, $second, $month, $day, $year);

        if ($ignoreModification)
            return $obj;

        if ( $month != $obj->month or
             $day != $obj->day or
             $year != $obj->year or
             $hour != $obj->hour or
             $minute != $obj->minute or
             $second != $obj->second )
            throw new Exception("the date before conversion is not the same than the result [{$obj}]");

        return $obj;
    }


    /**
     * Crée une instance à partir d'un masque simple, de type MM/DD/YY.
     *
     * @param  string $date
     * @param  string $mask
     * @param  boolean $ignoreModification
     * @return kore_date
     * @throws Exception
     */
    static public function fromMask($date, $mask, $ignoreModification = false)
    {
        static $_cache = array();

        if (isset($_cache[$mask]))
            $p = $_cache[$mask];
        else {
            $p = '#^'.strtr(preg_quote($mask, '#'), array(
                    'DD'    => '(?<day>\d{2})',
                    'D'     => '(?<day>\d{1,2})',
                    'MM'    => '(?<month>\d{2})',
                    'M'     => '(?<month>\d{1,2})',
                    'YYYY'  => '(?<year>\d{4})',
                    'YY'    => '(?<year>\d{2})',
                    'Y'     => '(?<year>\d{2,4})',

                    'HH'    => '(?<hour>\d{2})',
                    'H'     => '(?<hour>\d{1,2})',
                    'II'    => '(?<minute>\d{2})',
                    'I'     => '(?<minute>\d{1,2})',
                    'SS'    => '(?<second>\d{2})',
                    'S'     => '(?<second>\d{1,2})',

                    '\\*'   => '.*',
                    '\\?'   => '[^0-9]',
            )).'$#S';

            $_cache[$mask] = $p;
        }

        return static::fromReg($date, $p, $ignoreModification);
    }


    /**
     * Crée un objet date à partir du nombre de jour depuis la date de référence
     * Unix (01/01/1970).
     *
     * @param  $date integer
     * @return kore_date
     */
    public static function fromDays($date)
    {
        $obj = new static();
        $obj->days = (int) $date;

        return $obj;
    }

    /**
     * Crée un objet date à partir de la date courante.
     *
     * @return kore_date
     */
    public static function fromRequest()
    {
        static $obj = NULL;

        if ($obj === NULL)
            $obj = static::fromTimestamp((int) kore::requestTime());

        return $obj;
    }

    /**
     * Retourne la date courante.
     *
     * @return kore_date
     */
    public static function now()
    {
        return static::fromDateTime(new DateTimeImmutable('now'));
    }

    /**
     * Traite les conversions à la volée.
     *
     * @param  $format
     * @return mixed
     */
    public function __get($format)
    {
        static $timeZoneOffset = array();

        $date = NULL;

        switch($format){
            case 'sql':
                if (isset($this->sqltime))
                    $date = substr($this->sqltime, 0, 10);
                elseif ($this->datetime !== NULL)
                    $date = $this->datetime->format('Y-m-d');
                break;

            case 'sqltime':
                if ($this->datetime !== NULL)
                    $date = $this->datetime->format('Y-m-d H:i:s');
                break;

            case 'atom':
                if ($this->datetime !== NULL)
                    $date = $this->datetime->format('c');
                break;

            case 'time':
                if ($this->datetime !== NULL)
                    $date = $this->datetime->format('H:i:s');
                break;

            case 'datetime':
                if (isset($this->sqltime)) {
                    $date = new DateTimeImmutable($this->sqltime);

                } elseif (isset($this->timestamp)) {
                    $dt = new DateTimeImmutable();
                    $date = $dt->setTimestamp($this->timestamp);

                /*
                 * Try to obtain a dateTimeImmutable instance from a
                 * different source.
                 */
                } elseif ($date = $this->_sourceFallback()) {

                /*
                 * If we still don't have a «datetime», then try something less
                 * precise.
                 */
                } elseif (isset($this->sql)) {
                    $dt   = new DateTimeImmutable();
                    $date = $dt->setTime(0, 0, 0)
                            ->setDate($this->year, $this->month, $this->day);

                } elseif (isset($this->days)) {
                    $dt = new DateTimeImmutable();
                    $date = $dt->setTimestamp($this->days * 86400);
                }

                break;

            case 'timestamp':
                if ($this->datetime !== null)
                    $date = $this->datetime->getTimestamp();
                break;

            case 'days':
                /*
                 * We want the number of days since Epoch, independently of the
                 * current timezone. The best way found to do that, is to switch
                 * on an other date, in UTC.
                 */
                $dt = new DateTime($this->sql, new DateTimeZone('UTC'));
                $date = (int) ($dt->getTimestamp() / 86400);
                break;

            /*
             * Calcule le jour de la semaine, de 1 à 7, 1 étant le lundi
             * (cf constantes monday, tuesday, ... sunday ).
             */
            case 'weekDay':
                if (isset($this->days))
                    $date = 1+(($this->days + 3) % 7);
                else
                    $date = (int) $this->datetime->format('N');
                break;

            case 'yearWeek':
                $date = (int) $this->datetime->format('W');
                break;

            case 'monthWeek':
                $date = 1 + $this->yearWeek - $this->getFirstMonthDay()->yearWeek;
                break;

            case 'year':
            case 'month':
            case 'day':
                if (isset($this->sqltime))
                    $d = explode('-', substr($this->sqltime, 0, 10));
                else $d = explode('-', $this->sql);

                $this->year = (int)$d[0];
                $this->month = $d[1];
                $this->day = $d[2];

                $date = $this->{$format};
                break;

            case 'hour':
            case 'minute':
            case 'second':
                if (isset($this->sqltime))
                    $d = explode(':', substr($this->sqltime, -8));
                else $d = explode(':', $this->datetime->format('H:i:s'));

                $this->hour = $d[0];
                $this->minute = $d[1];
                $this->second = $d[2];

                $date = $this->{$format};
                break;

            default:
                $date = $this->_getUnknownFormat($format);
        }

        $this->{$format} = $date;
        return $date;
    }


    /**
     * Called when we search to obtain a DateTimeImmutable instance and we
     * didn't find a precise source.
     * Can be overrided to add a new «source».
     *
     * @return DateTimeImmutable | null
     */
    protected function _sourceFallback()
    {
        return null;
    }

    /**
     * Called when a conversion to an unknown format is triggered. Can be
     * overrided to add now target format.
     *
     * @param  $format
     * @return mixed
     */
    protected function _getUnknownFormat($format)
    {
        kore::$error->track("unknown date format [$format]");
        return null;
    }

    /**
     * Retourne le premier jour du mois correspondant à l'objet.
     *
     * @return kore_date
     */
    public function getFirstMonthDay()
    {
        return static::make(0, 0, 0, $this->month, 1, $this->year);
    }


    /**
     * Retourne le dernier jour du mois correspondant à l'objet.
     *
     * @return kore_date
     */
    public function getLastMonthDay()
    {
        $nbDays = (int) $this->datetime->format('t');

        if( ((int)$this->day) === $nbDays )
            return $this;

        return static::make(0, 0, 0, $this->month, $nbDays, $this->year);
    }

    /**
     * Retourne le premier jour de la semaine correspondant à l'objet.
     *
     * @return kore_date
     */
    public function getFirstWeekDay()
    {
        if( $this->weekDay === 1 )
            return $this;

        return $this->add(self::day, -($this->weekDay - 1));
    }

    /**
     * Retourne le dernier jour de la semaine correspondant à l'objet.
     *
     * @return kore_date
     */
    public function getLastWeekDay()
    {
        if ($this->weekDay === 7)
            return $this;

        return $this->add(self::day, 7 - $this->weekDay);
    }

    /**
     * Formate la date selon un masque "strftime()".
     *
     * @return string
     */
    public function format( $format )
    {
        return strftime($format, mktime(
                            $this->hour,
                            $this->minute,
                            $this->second,
                            $this->month,
                            $this->day,
                            $this->year));
    }

    /**
     * Formate la date selon un masque "DateTime::format()".
     *
     * @return string
     */
    public function formatSimple( $format )
    {
        return $this->datetime->format($format);
    }

    public function toDate()
    {
        return $this->setHour(0, 0, 0);
    }

    public function setHour($hour = 0, $minute = 0, $second = 0)
    {
        if ($this->hour == $hour and $this->minute == $minute and
                $this->second == $second)
            return $this;

        return static::make($hour, $minute, $second, $this->month,
                $this->day, $this->year);
    }

    public function add($type, $value)
    {
        $v = [
            self::year  => 'year',
            self::month => 'month',
            self::day   => 'day',
            self::hour  => 'hour',
            self::minute=> 'minute',
            self::second=> 'second'
        ];

        /*
         * We need to be sure that the value is an negative or positive integer
         */
        $value = (int) $value;

        /*
         * Define the sign of value based on if the value is negative or positive
         */
        if($value >= 0) $sign = '+';
        else $sign = '';

        /*
         * Return a new kore_date object from the previous datetime modified
         */
        return static::fromDateTime(
                $this->modify($sign.$value.' '.$v[$type]));
    }

    public function modify($modify)
    {
        return $this->datetime->modify($modify);
    }

    public function diffDays( kore_date $date )
    {
        return (int) round(($this->timestamp - $date->timestamp)/86400);
    }

    /**
     * Permet d'appeler la fonction «diff()» sur l'objet datetime stocké
     * dans l'objet courrant et ainsi connaitre la différence d'années, de mois,
     * de jours, d'heures, de minutes et de seconde entre la date courante
     * et la date passée en paramètre.
     *
     * @link http://php.net/manual/fr/datetime.diff.php
     *
     * @param kore_date $date
     * @param string $absolute
     *
     * @return DateInterval
     */
    public function diff( kore_date $date, $absolute = false )
    {
        return $this->datetime->diff($date->datetime, $absolute);
    }

    /**
     * Retourne un objet kore_date ajouté au fuseau horair passé en paramètre
     * en passant par DateTime afin de calculer ça de manière automatique.
     *
     * @param  mixed $timezone
     * @return kore_date
     */
    public function setTimezone($timezone)
    {
        if(!($timezone instanceOf DateTimeZone))
            $timezone = new DateTimeZone($timezone);

        $date = $this->datetime->setTimezone($timezone);

        return static::fromDateTime($date);
    }

    /**
     * Retourne une représentation de l'objet sous forme de chaine.
     * Ici on a choisi le format "sqltime", pour simplifier l'utilisation dans
     * le cas de requêtage SQL.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->sqltime;
    }

}

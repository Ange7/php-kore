<?php
/**
 * Class to manage HTTP status code as PHP exceptions.
 */
class kore_response_httpStatus extends Exception
{
    /*
     * 4XX : client errors
     * @see http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#4xx_Client_Error
     */

    static public function BadRequest400()
    {
        return new static('Bad Request', substr(__FUNCTION__, -3));
    }

    static public function Unauthorized401()
    {
        return new static('Unauthorized', substr(__FUNCTION__, -3));
    }

    static public function PaymentRequired402()
    {
        return new static('Payment Required', substr(__FUNCTION__, -3));
    }

    static public function Forbidden403()
    {
        return new static('Forbidden', substr(__FUNCTION__, -3));
    }

    static public function NotFound404()
    {
        return new static('Not Found', substr(__FUNCTION__, -3));
    }

    static public function MethodNotAllowed405()
    {
        return new static('Method Not Allowed', substr(__FUNCTION__, -3));
    }

    static public function NotAcceptable406()
    {
        return new static('Not Acceptable', substr(__FUNCTION__, -3));
    }

    static public function ProxyAuthenticationRequired407()
    {
        return new static('Proxy Authentication Required', substr(__FUNCTION__, -3));
    }

    static public function RequestTimeout408()
    {
        return new static('Request Timeout', substr(__FUNCTION__, -3));
    }

    static public function Conflict409()
    {
        return new static('Conflict', substr(__FUNCTION__, -3));
    }

    static public function Gone410()
    {
        return new static('Gone', substr(__FUNCTION__, -3));
    }


    /*
     * 5XX : server errors
     * @see http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#5xx_Server_Error
     */

    static public function InternalServerError500()
    {
        return new static('Internal Server Error', substr(__FUNCTION__, -3));
    }

    static public function NotImplemented501()
    {
        return new static('Not Implemented', substr(__FUNCTION__, -3));
    }

    static public function BadGateway502()
    {
        return new static('Bad Gateway', substr(__FUNCTION__, -3));
    }

    static public function ServiceUnavailable503()
    {
        return new static('Service Unavailable', substr(__FUNCTION__, -3));
    }

    static public function GatewayTimeout504()
    {
        return new static('Gateway Timeout', substr(__FUNCTION__, -3));
    }


    /**
     * Return a string representation of the error
     *
     * @see Exception::__toString()
     * @return string
     */
    public function __toString()
    {
        return 'error ' . $this->code.' : '.$this->message;
    }

    /**
     * Return an HTML representation of the error
     *
     * @see kore_response_httpStatus::__toString
     * @return string
     */
    public function _toHTML()
    {
        $pMessage = htmlspecialchars((string) $this);

        return <<<HTML
<html><head><title>$pMessage</title></head>
<body><h1>$pMessage</h1></body></html>
HTML;
    }

}
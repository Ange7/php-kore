<?php
/**
 * Préparation de la réponse au format "text"
 *
 * @package response
 */

/**
 * Prépare la réponse au format "text".
 *
 * Lors de l'appel de send() la session est fermée, et les
 * entêtes HTTP sont envoyés.
 *
 * @package response
 */
class kore_response_text
{
    protected $text = NULL;

	/**
	 * Instanciation de la classe.
	 */
    public function __construct()
    {
        kore::$conf->response_contentType = 'text/plain';
        kore::$conf->response_charset = 'UTF-8';
    }

    /**
     * Définie le contenu des données à envoyer.
     */
    public function setText( $text )
    {
        $this->text = $text;
    }

    /**
     * Initialise l'envoi des données : la session est fermée
     * et les entêtes HTTP envoyés.
     */
    public function send()
    {
        kore::$debug->benchCheckPoint( 'main', 'response' );

        if( kore::$conf->get('response_autoCloseSession', true) )
            kore::$session->close();

        if( kore::$conf->get('response_autoCloseDB', true) )
            kore::$db->closeAll();

        if( kore_response_http::getEtag() === NULL
            and kore::$conf->get( 'response_autoBuildEtag', true ) ) {

            kore_response_http::setEtag( md5( $this->text ) );
        }

        kore_response_http::start();

        kore::$debug->benchCheckPoint( 'main', 'text' );

        $this->sendData( $this->text );
    }

    /**
     * Envoi les données
     */
    protected function sendData( & $data )
    {
        echo $data;
    }

}

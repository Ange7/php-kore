<?php
/**
 * Préparation de la réponse dans un format quelconque.
 *
 * @package response
 *
 */

/**
 * Prépare la réponse dans un format quelconque.
 *
 * Lors de l'appel de send() la session est fermée, et les
 * entêtes HTTP sont envoyés.
 *
 * @package response
 */
class kore_response_data
{
    /**
     * Données à envoyer
     *
     * @var string
     */
    private $_data;

    /**
     * Définie le contenu des données à envoyer.
     *
     * @param string $data
     */
    public function setData($data)
    {
        $this->_data = $data;
    }

    /**
     * Initialise l'envoi des données : la session est fermée et les entêtes
     * HTTP envoyés. Les diverses connexions aux bases de données ne sont pas
     * fermées, celles ci étant probablement nécessaire à l'affichage des
     * données.
     */
    public function send()
    {
        kore::$debug->benchCheckPoint('main', 'response');

        if( kore::$conf->get('response_autoCloseSession', true) )
            kore::$session->close();

        kore_response_http::start();

        kore::$debug->benchCheckPoint('main', 'data');

        if($this->_data !== NULL) {
            echo $this->_data;
            flush();
        }
    }

}

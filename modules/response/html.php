<?php
/**
 * Préparation de la réponse au format "HTML" (via le moteur de template)
 *
 * TODO: pour le moment cette classe sert uniquement de transition vers
 * kore_response_template. A terme il faudra la remplacer par une surcharge de
 * kore_response_data, ou la supprimer.
 *
 * @package response
 *
 * @global string  kore::$conf->response_defaultTemplateContext
 * @global boolean kore::$conf->response_autoBuildEtag
 *
 */

/**
 * Préparation de la réponse au format "HTML" (via le moteur de template)
 *
 * @package response
 */
class kore_response_html
{
    /**
     * Template utilisé pour la réponse.
     *
     * @var kore_template
     */
    protected $_template;
    protected $_response;

    public function __construct( $templateFile )
    {
        $this->_template = new kore_template($templateFile);
        $this->_response = new kore_response_template();

        $this->_init();
    }

    protected function _init()
    {
    }

    public function addStylesheet( $url, $title = NULL, $media = NULL, $enableVersionning = true, $ieOnly = false )
    {
        return $this->_template->addStylesheet($url, $title, $media, $enableVersionning, $ieOnly);
    }

    public function addScript( $url, $type = NULL, $enableVersionning = true, $attribs = NULL )
    {
        return $this->_template->addScript($url, $type, $enableVersionning, $attribs);
    }

    public function send()
    {
        return $this->_response->send($this->_template);
    }

    public function buildContextHash()
    {
        return $this->_response->buildContextHash($this->_template);
    }

    public function __get( $name )
    {
        return $this->_template->context->$name;
    }

    public function __set( $name, $value )
    {
        $this->_template->context->$name = $value;
    }

    public function __isset( $name )
    {
        return isset($this->_template->context->$name);
    }
}
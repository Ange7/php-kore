<?php
/**
 * Gère l'envoi des entêtes HTTP appropriés à la page.
 *
 * @package response
 *
 * @global string  kore::$conf->response_contentType
 * @global string  kore::$conf->response_charset
 * @global boolean kore::$conf->response_httpUseModificationTime
 * @global integer kore::$conf->response_lastModificationTime
 * @global integer kore::$conf->response_httpExpire
 * @global string  kore::$conf->response_httpLimiter
 * @global string  kore::$conf->response_etag
 * @global string  kore::$conf->response_ignoreIfHeaderAlreadySent
 * @global boolean kore::$conf->response_allowStaleCache
 *
 */

/**
 * Gère l'envoi des entêtes HTTP appropriés à la page.
 *
 * @package response
 */
class kore_response_http
{
    private static $_headersSent = false;
    private static $_exitOnRedirect = true;

    private static $_vary = array();

    private static $_miscHeaders = array();

    /**
     * Empèche l'instanciation.
     */
    private function __construct()
    {
    }

    public static function redirect( $url, $code = 302 )
    {
        /**
         * Si le système de router de kore est utilisé et que le paramètre passé
         * est une instance de kore_router_route alors on fait le reverse
         * automatiquement.
         */
        if($url instanceOf kore_route_route)
            $url = $url->reverse();

        /**
         * Si les entêtes ont déjà été envoyés, on ne peut pas faire la
         * redirection, déclenche donc une erreur fatale.
         */
        $file = NULL; $line = NULL;
        if (!self::canSendHeader($file, $line))
            throw new RuntimeException("headers already sent, can't redirect to $url");

        $url = kore_request_http::completeURL($url, true);

        ini_set('zlib.output_compression', false);

        self::_sendCommonHeaders();

        if (kore::$debug->isEnabled())
            echo "Redirection ($code) to $url\n";
        else
            header('Location: '.$url, true, $code);

        if (self::$_exitOnRedirect)
            exit;
    }

    public static function redirectPermanent( $url )
    {
        /**
         * S'il s'agit d'une redirection permanente, on peut y mettre un long
         * temps d'expiration.
         */
        if (kore::$conf->response_httpExpire < 3600)
            kore::$conf->response_httpExpire = 3600;

        self::redirect($url, 301);
    }

    public static function redirectTemporary( $url )
    {
        self::redirect($url, 307);
    }

    /**
     * Indique si les entêtes peuvent être envoyés ou non.
     *
     * @param  string  $file
     * @param  integer $line
     * @return boolean
     */
    public static function canSendHeader( &$file, &$line )
    {
        $file = __CLASS__; $line = 0;
        return ! ( self::$_headersSent or headers_sent($file, $line) );
    }

    public static function disableExitOnRedirect()
    {
        self::$_exitOnRedirect = false;
    }

    public static function getLastModificationTime()
    {
        return kore::$conf->response_lastModificationTime;
    }
    public static function setLastModificationTime( $time )
    {
        kore::$conf->response_lastModificationTime = $time;
    }
    public static function updateLastModificationTime( $time )
    {
        if( $time > kore::$conf->response_lastModificationTime )
            kore::$conf->response_lastModificationTime = $time ;
    }

    public static function getEtag()
    {
        return kore::$conf->response_etag;
    }

    public static function setEtag( $etag )
    {
        $file = NULL; $line = NULL;
        if (self::canSendHeader($file, $line))
            kore::$conf->response_etag = trim($etag);
        elseif (!kore::$conf->response_ignoreIfHeaderAlreadySent)
            kore::$error->track("ETag must be set _before_ send headers",
                    null, null, $file, $line);
    }

    public static function addVary( $id )
    {
        self::$_vary[$id] = true;
    }
    public static function removeVary( $id )
    {
        if( isset(self::$_vary[$id]) )
            unset(self::$_vary[$id]);
    }
    public static function resetVary()
    {
        self::$_vary = array();
    }

    public static function addMiscHeader( $name, $value )
    {
        self::$_miscHeaders[$name] = $value;
    }

    public static function removeMiscHeader( $name )
    {
        if( isset(self::$_miscHeaders[$name]) )
            unset(self::$_miscHeaders[$name]);
    }

    public static function start()
    {
        /**
         * Si les entêtes ont déjà été envoyés, on ne peut plus rien faire ici.
         */
        $file = NULL; $line = NULL;
        if (self::canSendHeader($file, $line))
            self::$_headersSent = true;
        else {
            if (!kore::$conf->response_ignoreIfHeaderAlreadySent)
                kore::$error->track("headers sent before ".__METHOD__."()",
                        null, null, $file, $line);
            return;
        }

        self::_sendCommonHeaders();

        if( kore::$conf->response_httpUseModificationTime
            and kore::$conf->response_lastModificationTime ) {

            header('Last-Modified: '.
                self::formatDate(kore::$conf->response_lastModificationTime));

            if( isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ) {
                $lastLoad = @ strtotime(substr(
                    $_SERVER['HTTP_IF_MODIFIED_SINCE'], 0, 29));

                if($lastLoad >= kore::$conf->response_lastModificationTime ) {
                    self::_response304();
                }
            }
        }

        if( kore::$conf->response_etag !== NULL ) {
            $prefix = 'k';
            if (!empty($_SERVER['HTTP_ACCEPT_ENCODING']))
                $prefix .= 'c';
            else
                $prefix .= 'p';

            header('ETag: "'.$prefix.kore::$conf->response_etag.'"');

            if( isset($_SERVER['HTTP_IF_NONE_MATCH']) and
                strpos($_SERVER['HTTP_IF_NONE_MATCH'],
                    kore::$conf->response_etag) !== false ) {

                self::_response304();
            }
        }

        if( @ $_SERVER['REQUEST_METHOD'] === 'HEAD' ) {
            exit;
        }

        self::_statusCookie(NULL);
    }

    private static function _sendCommonHeaders()
    {
        /**
         * Pour éviter un appel système supplémentaire, n'utilise l'heure exacte
         * que si kore::$conf->response_httpUseModificationTime est actif.
         */
        if( ! kore::$conf->response_httpUseModificationTime )
            $currentTime = kore::requestTime();
        else $currentTime = time();

        header('Date: ' . self::formatDate($currentTime));

        if( kore::$conf->response_charset )
            header('Content-Type: ' . kore::$conf->response_contentType .
                    '; charset=' . kore::$conf->response_charset);
        else
            header('Content-Type: ' . kore::$conf->response_contentType);

        /**
         * Envoi l'entête Expires si celui fourni est différent de 0
         * (positif comme négatif) ; ou bien si les sessions sont activées
         * afin d'écraser l'entête envoyé par session_start().
         */
        $maxAge = 0;
        if ( ( $expires = (int) kore::$conf->get('response_httpExpire', 0) )
             or isset($_SESSION) ) {

            if( $expires < 86400*366 ) {
                $maxAge = $expires;
                $expires += $currentTime;
            } else {
                $maxAge = $expires - $currentTime;
            }

            header('Expires: ' . self::formatDate($expires));
        }

        $cacheControl = '';
        if( $maxAge < 0 ) $maxAge = 0;
        if( $maxAge != 0 ) $cacheControl .= 'max-age='.$maxAge;

        $limiter = kore::$conf->response_httpLimiter;
        if( isset($_SESSION) and ($limiter === NULL) )
            $limiter = 'private';
        if( @ $_SERVER['REQUEST_METHOD'] === 'POST' )
            str_replace($limiter, 'public', 'private');

        if( $limiter !== '' ) {
            if( $cacheControl !== '' ) $cacheControl .= ', ';
            $cacheControl .= $limiter;
        }

        $allowStaleCache = ( (kore::$conf->response_allowStaleCache !== false)
                and (@ $_SERVER['REQUEST_METHOD'] !== 'POST'));
        if (!$allowStaleCache){
            if( $cacheControl !== '' ) $cacheControl .= ', ';
            $cacheControl .= 'must-revalidate';
        }

        if ($cacheControl !== '')
            header('Cache-Control: ' . $cacheControl);

        if( $limiter === 'no-cache' )
            header('Pragma: no-cache');

        if( count(self::$_vary) > 0 )
            header('Vary: ' . implode(', ', array_keys(self::$_vary)));

        foreach( self::$_miscHeaders as $name => $value )
            header($name .': '. $value);
    }

    /**
     * Send status cookie
     */
    private static function _statusCookie( $status )
    {
        if( kore::$conf->response_addStatusCookie ) {

            if( ! kore::$conf->response_httpUseModificationTime )
                $currentTime = kore::requestTime();
            else $currentTime = time();

            if( $status === NULL and !isset($_COOKIE[kore::$conf->response_addStatusCookie]) )
                return;

            setcookie(kore::$conf->response_addStatusCookie, $status,
                $currentTime + 5);
        }
    }


    /**
     * Envoi une réponse 304 et arrête l'exécution du script.
     */
    private static function _response304()
    {
        self::_statusCookie(304);

        ini_set('zlib.output_compression', false);

        if (kore::$conf->sapiName === 'apa')
            header('Not Modified', true, 304);
        else
            header('Status: 304 Not Modified');
        exit;
    }

    /**
     * Format a date to "RFC 822, updated by RFC 1123"
     * http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3.1
     *
     * @param  timestamp $date
     * @return string
     */
    public static function formatDate( $date )
    {
        return gmdate('D, d M Y H:i:s', $date) . ' GMT';
    }

}

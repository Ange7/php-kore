<?php
/**
 * ...
 *
 * @package response
 *
 * @global boolean kore::$conf->response_httpUseModificationTime
 * @global boolean kore::$conf->response_autoBuildEtag
 *
 */

class kore_response_file
{
    protected $filename = NULL;

    public function __construct( $filename )
    {
        $this->filename = $filename;
    }

    public function send()
    {
        kore::$debug->benchCheckPoint( 'main', 'response' );

        if( kore::$conf->get('response_autoCloseSession', true) )
            kore::$session->close();

        kore::$db->closeAll();

        $time = @ filemtime( $this->filename );
        if( $time ) {
            kore::$conf->response_httpUseModificationTime = true;
            kore_response_http::setLastModificationTime( $time );

            if( ( kore_response_http::getEtag() === NULL )
                and kore::$conf->get( 'response_autoBuildEtag', true ) ) {

                kore_response_http::setEtag( dechex($time) .'-'.
                    dechex(filesize($this->filename)) );
            }
        }

        kore_response_http::start();

        kore::$debug->benchCheckPoint( 'main', 'readfile' );
        readfile( $this->filename );
    }

}

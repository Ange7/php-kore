<?php
/**
 * Préparation de la réponse au format "json"
 *
 * @package response
 */

/**
 * Prépare la réponse au format "json".
 *
 * Lors de l'appel de send() la session est fermée, et les
 * entêtes HTTP sont envoyés.
 *
 * @package response
 */
class kore_response_json
{
    protected $jsonData = NULL;

    /**
     * Instancie la classe et déclenche directement la réponse
     *
     * @param  mixed $data
     */
    static public function direct($data)
    {
        $response = new static;
        $response->setData($data);
        $response->send();
    }

    /**
     * Instanciation de la classe.
     */
    public function __construct()
    {
        kore::$conf->response_contentType = 'text/plain';
        kore::$conf->response_charset = 'UTF-8';
    }

    /**
     * Définie le contenu des données à envoyer.
     */
    public function setData( $data )
    {
        $this->jsonData = $data;
    }

    /**
     * Initialise l'envoi des données : la session est fermée
     * et les entêtes HTTP envoyés.
     */
    public function send()
    {
        kore::$debug->benchCheckPoint( 'main', 'response' );

        if( kore::$conf->get('response_autoCloseSession', true) )
            kore::$session->close();

        $encodedData = json_encode( $this->jsonData );

        if( kore_response_http::getEtag() === NULL
            and kore::$conf->get( 'response_autoBuildEtag', true ) ) {

            kore_response_http::setEtag( md5( $encodedData ) );

        }
        kore_response_http::start();

        kore::$debug->benchCheckPoint( 'main', 'data' );

        $this->sendData( $encodedData );
    }

    /**
     * Envoi les données
     */
    protected function sendData( $data )
    {
        if ($prefix = kore::$conf->response_prefixJSON)
            echo $prefix;

        echo $data;

        flush();
    }

}

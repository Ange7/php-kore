<?php
/**
 * Préparation de la réponse pour débugage.
 *
 * @package response
 */

/**
 * Prépare une réponse de type débug : affichage des messages de débugage, de la
 * configuration courante, et des données actuelles.
 *
 * @package response
 */
class kore_response_debug
{
    protected $_calls = array();
    public $DOM;

    /**
     * Instanciation de la classe.
     */
    public function __construct()
    {
        kore::$conf->response_contentType = 'text/plain';
        $this->DOM = new kore_response_debug_DOM();
    }

    /**
     * Initialise l'envoi des données : la session est fermée
     * et les entêtes HTTP envoyés.
     */
    public function send()
    {
        kore::$debug->benchCheckPoint( 'main', 'response' );

        if( kore::$conf->get('response_autoCloseSession', true) )
            kore::$session->close();

        if( kore::$conf->get('response_autoCloseDB', true) )
            kore::$db->closeAll();

        kore_response_http::start();
        kore::$debug->benchCheckPoint( 'main', 'data' );

        $this->showDebug();
    }

    /**
     * Capture tous les appels de méthode pour les tracer.
     *
     * @param  string  $fctName
     * @param  array   $fctArgs
     */
    public function __call( $fctName, $fctArgs )
    {
        $call = $fctName.'(';
        foreach( $fctArgs as $idx => $value ){
            if( $idx > 0 ) $call .= ', ';
            $call .= var_export( $value, true );
        }
        $call .= ')';
        $this->_calls[] = $call;
    }

    /**
     * Envoi les données
     */
    protected function showDebug()
    {
        $line = str_repeat( ':', 80 ).PHP_EOL;

        echo PHP_EOL.'::: RESPONSE / CALLS :::'.$line;
        var_dump( $this->_calls );

        echo PHP_EOL.'::: RESPONSE / DATA :::'.$line;
        unset( $this->_calls );
        var_dump( $this );

        echo PHP_EOL.'::: DEBUG / MSG :::'.$line;
        print_r( kore::$debug->getMessages() );

        echo PHP_EOL.'::: DEBUG / BENCH :::'.$line;
        print_r( kore::$debug->benchGet() );

        echo PHP_EOL.'::: KORE / CONF :::'.$line;
        print_r( kore::$conf->dump() );
    }

}

/**
 * Gestion d'un "arbre" d'objet : chaque accès à un "noeud"
 * le crée, ce qui permet de reproduire facilement un arbre
 * du genre : DOM->head->meta->author ou DOM->forms->login->username
 *
 * @package response
 */
class kore_response_debug_DOM
{
    /**
     * Identifiant interne du noeud.
     *
     * @var string
     */
    protected $_internalId = NULL;

    /**
     * Construction de l'objet
     *
     * @param string  $internalId
     */
    function __construct( $internalId = 'DOM' )
    {
        $this->_internalId = $internalId;
    }

    /**
     * Retourne la valeur d'un "noeud" DOM.
     *
     * @param string $name
     * @return mixed
     */
    function __get( $name )
    {
        $this->$name = new self(
                $this->_internalId .'.'. $name );

        return $this->$name;
    }

}

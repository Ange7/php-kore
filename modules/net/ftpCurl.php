<?php

/**
 * Classe ftp utilisant cURL
 */
class kore_net_ftpCurl extends kore_net_ftpAbstract
{

    /**
     * Handler cURL
     * @var resource
     */
    private $_curlHandler;

    /**
     * Options cURL
     * @var array
     */
    private $_options = array();

    /**
     * Chemin de la racine lors de la connexion
     * @var string
     */
    private $_root = '';

    /**
     * Répertoire courant sur le serveur distant
     * @var string
     */
    private $_currentDir = '/';

    /**
     * Renvoit une instance de kore_net_ftpCurl d'après une URL
     * @param  string $url
     * @return kore_net_ftpCurl
     */
    public static function fromUrl($url)
    {
        $ftp = parent::fromUrl($url);

        $url = parse_url($url);
        if (isset($url['path']) and $url['path'] !== '')
            $ftp->_root .= trim($url['path'], '/').'/';

        return $ftp;
    }

    /**
     * Constructeur
     * @param string $host
     * @param string $port
     * @param string $login
     * @param string $password
     */
    public function __construct($host = 'localhost', $port = 21,
                                $login = 'anonymous', $password = NULL)
    {

        parent::__construct($host, $port, $login, $password);

        // Spécificité cURL
        $this->_root = rtrim('ftp://'.$host, '/').'/';
    }

    /**
     * Établit la connexion
     * Autant kore_net_ftp se connecte immédiatement, autant il peut être
     * nécessaire pour cURL de passer des options spécifiques avant
     * la connexion. Cette méthode doit donc être publique puisqu'appelée
     * depuis un contrôleur.
     */
    public function connect()
    {
        return $this->_connect();
    }

    /**
     * Établit la connexion
     */
    protected function _connect()
    {
        if (!$this->_connected) {
//            $this->_curlHandler = curl_init($this->_root);
            $this->_curlHandler = curl_init();
            $this->_options = array(
                CURLOPT_USERPWD         => $this->_login.':'.$this->_password,
                CURLOPT_PORT            => $this->_port,
                CURLOPT_RETURNTRANSFER  => TRUE,
                CURLOPT_CONNECTTIMEOUT  => $this->_timeout,
                CURLOPT_URL             => $this->_root,
            );

            curl_setopt_array($this->_curlHandler, $this->_options);

            if (!$ret = curl_exec($this->_curlHandler))
                $this->_trackError('connect', curl_error($this->_curlHandler),
                        __FILE__, __LINE__);
            else
                $this->_connected = TRUE;
        }
        return $ret;
    }

    /**
     * Ferme la connexion
     */
    protected function _close()
    {
        $this->_handler = NULL;
        $this->_root = NULL;
        $this->_currentDir = '/';
        curl_close($this->_curlHandler);
    }

    /**
     * Définit les timeout
     * @param $timeout
     */
    public function setTimeout($timeout)
    {
        $this->_options[CURLOPT_CONNECTTIMEOUT] = (int) $timeout;
    }

    /**
     * Liste le contenu du répertoire
     * @param $dirName
     */
    public function rawlist($dirName = NULL)
    {
        $options = $this->_options;
        if (NULL !== $dirName)
            if (substr($dirName, 0) === '/')
                $url = $this->_root.trim($dirname, '/').'/';
            else
                $url = $this->_getUrl().$dirName;
        else
            $url = $this->_getUrl();

        $options[CURLOPT_URL] = $url;

        $options[CURLOPT_CUSTOMREQUEST] = 'LIST';
        $ret = $this->_exec($options, 'ls');

        // Le cas où $ret vaut FALSE est géré dans _exec qui déclenche l'erreur+
        if (FALSE !== $ret)
            $ret = $this->_parseRawList($ret);

        return $ret;
    }

    /**
     * Change de répertoire courant
     * @param $dirName
     */
    public function chdir($dirName)
    {
        $this->_currentDir = trim($dirName, '/').'/';
    }

    /**
     * Retourne le nom du répertoire courant
     */
    public function pwd()
    {
        return $this->_currentDir;
    }

    /**
     * Renvoit l'url du répertoire courant
     */
    protected function _getUrl()
    {
        return rtrim($this->_root.$this->_currentDir, '/').'/';
    }

    /**
     * Active ou non le mode passif
     * cURL gère lui-même le mode actif/passif, cette méthode n'est présente
     * que pour compatibilité avec kore_net_ftp
     * @param boolean $enable
     */
    public function pasv($enable)
    {
        return TRUE;
    }

    /**
     * Télécharge un fichier
     * @param string $remoteName
     * @param string $localName
     * @param string $mode
     * @param int $resumePos
     */
    public function get($remoteName, $localName, $mode = FTP_BINARY,
            $resumePos = NULL)
    {
        $options = $this->_options;
        unset($options[CURLOPT_CUSTOMREQUEST],
                $options[CURLOPT_RETURNTRANSFER]);

        // On tient compte du fait que l'url peut être absolue ou relative
        if (substr($remoteName, 0) === '/')
            $remoteName = ltrim($remoteName, '/');
        else
            $remoteName = $this->_currentDir.$remoteName;

        $options[CURLOPT_URL] = $this->_root.$remoteName;

        $fMode = 'w';
        if (FTP_BINARY === $mode) {
            $options[CURLOPT_BINARYTRANSFER] = TRUE;
            $fMode .= 'b';
        } else {
            $options[CURLOPT_FTPTEXT] = TRUE;
        }

        $localFile = fopen($localName, $fMode);
        $options[CURLOPT_FILE] = $localFile;

        if (NULL !== $resumePos)
            $options['CURLOPT_RESUME_FROM'] = (int) $resumePos;

        // On ne gère pas l'erreur ici : elle est gérée dans _exec
        $ret = $this->_exec($options, 'get');

        fclose($localFile);

        return $ret;
    }

    /**
     * Exécute une requête
     * @param array $options
     * @param string $customRequest
     */
    private function _exec(array $options, $cmd = 'exec')
    {
        $opt = $this->_options;
        foreach ($options as $k => $v) {
            if (NULL === $v)
                unset($opt[$k]);
            else
                $opt[$k] = $v;
        }

        curl_setopt_array($this->_curlHandler, $options);

        $ret = curl_exec($this->_curlHandler);
        if (FALSE === $ret)
            $this->_trackError($cmd, curl_error($this->_curlHandler),
                    __FILE__, __LINE__);
        return $ret;
    }

    public function cdup()
    {
        $this->_currentDir = dirname(trim($this->currentDir, '/')).'/';
        if ('.' === $this->_currentDir or '/' === $this->_currentDir)
            $this->_currentDir = '';
    }

}

<?php

abstract class kore_net_ftpAbstract implements kore_net_interface
{

    /**
     * Précision de la date : heure
     * @var int
     */
    const TIME_PRECISION_HOUR = 1;

    /**
     * Précision de la date : journée
     * @var int
     */
    const TIME_PRECISION_DAY  = 2;

    /**
     * Serveur distant
     * @var string
     */
    protected $_host;

    /**
     * Port
     * @var int
     */
    protected $_port;

    /**
     * Nom d'utilisateur
     * @var string
     */
    protected $_login;

    /**
     * Mot de passe
     * @var string
     */
    protected $_password;

    /**
     * Timeout
     * @var int
     */
    protected $_timeout = 15;

    /**
     * Mode passif / actif
     * @var boolean|NULL
     */
    protected $_pasv = NULL;

    /**
     * Identifiant interne de la connexion
     * @var int
     */
    protected $_id;

    /**
     * Etat de la connexion
     * @var boolean
     */
    protected $_connected = false;

    /**
     * Format du listing des fichiers
     * @var int
     */
    protected $_rawlistTemplate = NULL;

    /**
     * Contient la dernière erreur rencontrée
     * @var string
     */
    private $_lastError = false;

    /**
     * Retourne une instance de la classe en fonction de l'URL donnée.
     * @param  string $url
     * @return kore_net_ftpAbstract
     */
    static public function fromUrl($url)
    {
        $className = get_called_class();

        $host = 'localhost';
        $port = 21;
        $login = 'anonymous';
        $password = NULL;

        $url = parse_url($url);

        if (isset($url['host'])) $host = $url['host'];
        if (isset($url['port'])) $port = $url['port'];
        if (isset($url['user'])) $login = $url['user'];
        if (isset($url['pass'])) $password = $url['pass'];

        return new $className($host, $port, $login, $password);
    }


    /**
     * Télécharge un fichier
     * @param string $remoteName
     * @param string $localName
     * @param string $mode
     * @param string $resumePos
     */
    abstract public function get($remoteName, $localName, $mode = FTP_BINARY,
            $resumePos = NULL);

    /**
     * Liste les fichiers dans le répertoire
     * @param string|NULL $dirName Répertoire à lister
     */
    abstract public function rawlist($dirName = NULL);

    /**
     * Timeout (en secondes)
     * @param int $timeout
     */
    abstract public function setTimeout($timeout);

    /**
     * Établit la connexion
     */
    abstract protected function _connect();

    /**
     * Ferme la connexion
     */
    abstract protected function _close();

    /**
     * Retourne le chemin courant sur le serveur distant
     */
    abstract public function pwd();

    /**
     * Remonte d'un niveau dans l'arborescence
     */
    abstract public function cdup();

    /**
     * Change le répertoire courant sur le serveur distant
     * @param string $dirName
     */
    abstract public function chdir($dirname);

    /**
     * Constructeur
     * @param $host
     * @param $port
     * @param $login
     * @param $password
     */
    public function __construct($host = 'localhost', $port = 21,
                                $login = 'anonymous', $password = NULL)
    {
        $this->_timeout = kore::$conf->get('net_ftpTimeout', 15);

        $this->_host = $host;
        $this->_port = $port;
        $this->_login = $login;
        $this->_password = $password;

        if( $password === NULL )
            $password = ini_get( 'from' );

        static $id = 0;
        $this->_id = ++$id;

    }

    /**
     * Destructeur
     */
    public function __destruct()
    {
        if( $this->_connected === true )
            $this->_close();
    }

    /**
     * Retourne l'identifiant interne de la connexion
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Retourne l'état de la connexion
     */
    public function isConnected()
    {
        return $this->_connected;
    }

    /**
     * Réinitialise la connexion
     */
    public function rein()
    {
        $this->_close();
        return $this->_connect();
    }

    /**
     * Retourne la liste des fichiers/répertoires du répertoire (par défaut
     * le répertoire courant)
     * @param string|NULL $dirName
     */
    public function readdir( $dirName = NULL )
    {
        return $this->rawlist( $dirName );
    }

    /**
     * Trace une erreur
     * @param string $command
     * @param string $message
     * @param string $file
     * @param int $line
     */
    protected function _trackError( $command, $message, $file=NULL, $line=NULL )
    {
        kore::$error->track("FTP $command : $message", null, null, $file, $line);
    }


    /**
     * Récupère la dernière erreur rencontrée (et efface le buffer)
     * @return string
     */
    public function getError()
    {
        $lastError = $this->_lastError;
        $this->_lastError = false;
        return $lastError;
    }


    /**
     * Analyse le listing brut des fichiers/répertoire
     * thanks to : D.J. Bernstein, djb@cr.yp.to (http://cr.yp.to/ftpparse.html)
     * and Tim Kosse <tim.kosse@gmx.de> (http://sourceforge.net/projects/filezilla)
     * for their code to made this function.
     * @param array $list
     */
    protected function _parseRawList( & $list )
    {

        if (is_string($list))
            $list = preg_split('`[\n\r]`', $list);

        if( ! is_array($list) ) return false;

        /**
         * Initialise l'objet résultat.
         */
        $result = new stdClass;
        $result->totalSize = 0;
        $result->entries = array();
        $result->directories = array();
        $result->files = array();
        $result->usedTemplate = NULL;

        $entryTemplate = new stdClass;
        $entryTemplate->isDir = false;
        $entryTemplate->permission = NULL;
        $entryTemplate->subDir = NULL;
        $entryTemplate->owner = NULL;
        $entryTemplate->group = NULL;
        $entryTemplate->domain = NULL;
        $entryTemplate->size = NULL;
        $entryTemplate->date = NULL;
        $entryTemplate->time = NULL;
        $entryTemplate->timePrecision = NULL;

        foreach( $list as $idx => $line ){
            $line = rtrim($line, "\r\n");
            if( $line === '' ) continue;

            // like UNIX standard, but without the group column
            if( ( $this->_rawlistTemplate === 2 or
                  $this->_rawlistTemplate === NULL ) and
                preg_match('`^([bcdlps\\-][a-zA-Z\\-]{9}) +([0-9]+) +(\\S{1,8}) +([0-9]+) +(\\S{1,4}[,.]?\\s +\\S{1,4}[,.]? +[0-9:]{4,5}) (.*)`', $line, $match)
                ){

                $this->_rawlistTemplate = 2;

                $name = $match[6];
                $entry = clone $entryTemplate;
                $entry->isDir = ($line{0}==='d');
                $entry->permission = $match[1];
                $entry->subDir = $match[2];
                $entry->owner = $match[3];
                $entry->size = (int) $match[4];
                $entry->date = $match[5];

            // the standard UNIX format
            } elseif( ( $this->_rawlistTemplate === 1 or
                        $this->_rawlistTemplate === NULL ) and
                preg_match('`^([bcdlps\\-][a-zA-Z\\-]{9}) +([0-9]+) +(?:(\\S{1,8}) *(\\S{1,8}) *|(\\S+) +(\\S+) +)([0-9]+) +(\\S{1,4}[,.]? +\\S{1,4}[,.]? +[0-9:]{4,5}) (.*)`', $line, $match)
                ){
                $this->_rawlistTemplate = 1;

                $name = $match[9];
                $entry = clone $entryTemplate;
                $entry->isDir = ($line{0}==='d');
                $entry->permission = $match[1];
                $entry->subDir = $match[2];
                /*
                 * Certains serveurs ftp affichent le nom d'utilisateur
                 * complet même s'il fait plus de 8 caractères. D'où le | dans
                 * la pcre qui implique de tester quel masque est satisfait
                 */
                $entry->owner = empty($match[3]) ? $match[5] : $match[3];
                $entry->group = empty($match[4]) ? $match[6] : $match[4];
                $entry->size = (int) $match[7];
                $entry->date = $match[8];

            // a Windows format, with user, group and domain given
            } elseif( ( $this->_rawlistTemplate === 4 or
                        $this->_rawlistTemplate === NULL ) and
                preg_match('`^([bcdlps\\-][a-zA-Z\\-]{9}) +([0-9]+) +(\\S{1,8}) *(\\S{1,8}) *(\\S{1,8}) *([0-9]+) +(\\S{1,4}[,.]? +\\S{1,4}[,.]? +[0-9:]{4,5}) (.*)`', $line, $match)
                ){

                $this->_rawlistTemplate = 4;

                $name = $match[8];
                $entry = clone $entryTemplate;
                $entry->isDir = ($line{0}==='d');
                $entry->permission = $match[1];
                $entry->subDir = $match[2];
                $entry->group = $match[3];
                $entry->domain = $match[4];
                $entry->owner = $match[5];
                $entry->size = (int) $match[6];
                $entry->date = $match[7];

            // a DOS format, used by IIS
            } elseif( ( $this->_rawlistTemplate === 3 or
                        $this->_rawlistTemplate === NULL ) and
                preg_match('`^([0-9]{2,4}-[0-9]{2}-[0-9]{2} +[0-9]{2}:[0-9]{2}[A-Z]+) +(<DIR>|[0-9,]+) +(.*)`', $line, $match)
                ){

                $this->_rawlistTemplate = 3;

                $name = $match[3];
                $entry = clone $entryTemplate;
                $entry->isDir = ($match[2]==='<DIR>');
                $entry->size = (int) str_replace(',','',$match[2]);
                $entry->date = $match[1];

            // like standard UNIX format, but with full date
            } elseif( ( $this->_rawlistTemplate === 5 or
                        $this->_rawlistTemplate === NULL ) and
                preg_match('`^([bcdlps\\-][a-zA-Z\\-]{9}) +([0-9]+) +(\\S{1,8}) *(\\S{1,8}) *([0-9]+) +([0-9]{2,4}[\\-/][0-9]{2}[\\-/][0-9]{2,4} [0-9]{2}:[0-9]{2}) (.*)`', $line, $match)
                ){

                $this->_rawlistTemplate = 5;

                $name = $match[7];
                $entry = clone $entryTemplate;
                $entry->isDir = ($line{0}==='d');
                $entry->permission = $match[1];
                $entry->subDir = $match[2];
                $entry->owner = $match[3];
                $entry->group = $match[4];
                $entry->size = (int) $match[5];
                $entry->date = $match[6];

            // like standard UNIX format, but with short date
            } elseif( ( $this->_rawlistTemplate === 6 or
                        $this->_rawlistTemplate === NULL ) and
                preg_match('`^([bcdlps\\-][a-zA-Z\\-]{9}) +([0-9]+) +(\\S{1,8}) *(\\S{1,8}) *([0-9]+) +([0-9]{2}-[0-9]{2} [0-9:]{4,5}) (.*)`', $line, $match)
                ){

                $this->_rawlistTemplate = 6;

                $name = $match[7];
                $entry = clone $entryTemplate;
                $entry->isDir = ($line{0}==='d');
                $entry->permission = $match[1];
                $entry->subDir = $match[2];
                $entry->owner = $match[3];
                $entry->group = $match[4];
                $entry->size = (int) $match[5];
                $entry->date = $match[6];

            // unknown format, try to extract some info
            } elseif( strtolower(substr($line, 0, 5)) !== 'total' ){

                if( preg_match('`\\S+$`', rtrim($line), $match) )
                    $name = $match[0];
                else $name = $line;

                $entry = clone $entryTemplate;
                $entry->isDir = ($line{0}==='d');

            } else {
                continue;
            }

            if( $entry->isDir === true ) {
                $result->directories[ $name ] = $entry;
                $result->totalSize += $entry->size;
            } else {
                $result->files[ $name ] = $entry;
            }

            if( $entry->date !== NULL ) {
                $this->_parseDate( $entry );
            }
        }

        uksort($result->directories, 'strnatcasecmp');
        uksort($result->files, 'strnatcasecmp');
        $result->entries = $result->directories + $result->files;

        $result->usedTemplate = $this->_rawlistTemplate;

        return $result;
    }

    /**
     * Convertit une date du listing brut en timestamp
     * thanks to : D.J. Bernstein, djb@cr.yp.to (http://cr.yp.to/ftpparse.html)
     * and Tim Kosse <tim.kosse@gmx.de> (http://sourceforge.net/projects/filezilla)
     * for their code to made this function.
     * @param stdClass $entry
     */
    protected function _parseDate( $entry )
    {
        /**
         * Tableau de conversion pour les mois.
         */
        static $months = array(
            // Noms anglais
            'jan' => 1,'feb' => 2,'mar' => 3,'apr' => 4,'may' => 5,'jun' => 6,
            'jul' => 7,'aug' => 8,'sep' => 9,'oct' =>10,'nov' =>11,'dec' =>12,

            // Ajouts français
            'janv'=> 1,'fév' => 2,'fev' => 2,'févr'=> 2,'fevr'=> 2,'mars'=> 3,
            'avr' => 4,'mai' => 5,'juin'=> 6,'juil'=> 7,'aoû' => 8,'aou' => 8,
            'août'=> 8,'aout'=> 8,'sept'=> 9,'déc' =>12,

            // Ajouts numeriques
            '01'  => 1,'1'   => 1,'02'  => 2,'2'   => 2,'03'  => 3,'3'   => 3,
            '04'  => 4,'4'   => 4,'05'  => 5,'5'   => 5,'06'  => 6,'6'   => 6,
            '07'  => 7,'7'   => 7,'08'  => 8,'8'   => 8,'09'  => 9,'9'   => 9,
            '10'  =>10,'11'  =>11,'12'  =>12 ,

            // Ajouts allemands
            'mrz' => 3,'mär' => 3,'okt' => 5,'okt' =>10,'dez' =>12,

            // Ajouts italiens
            'gen' => 1,'mag' => 5,'giu' => 6,'lug' => 7,'ago' => 8,'set' => 9,
            'ott' =>10,'dic' =>12,

            // Ajouts espagnols
            'ene' => 1,'abr' => 4,

            // Ajouts polonais
            'sty' => 1,'lut' => 2,'kwi' => 4,'maj' => 5,'cze' => 6,'lip' => 7,
            'sie' => 8,'wrz' => 9,'pa' =>10,'lis' =>11,'gru' =>12,

            // Ajouts russes
            'ÿíâ' => 1,'ÿíâ' => 2,'ìàð' => 3,'àïð' => 4,'ìàé' => 5,'èþí' => 6,
            'èþë' => 7,'àâã' => 8,'ñåí' => 9,'îêò' =>10,'íîÿ' =>11,'äåê' =>12,

            // Ajouts néerlandais
            'mrt' => 3,'mei' => 5,
            );

        $date = preg_split('`[ .,\\-/]+`', $entry->date);
        $nbCols = count($date);
        if( $nbCols < 3 or $nbCols > 4 ) return;

        $hour = false;
        $minute = false;
        $day = false;
        $month = false;
        $year = false;

        $yearIdx = false;
        foreach( $date as $idx => $col) {
            // Numeric = year, month or day
            if( is_numeric($col) ){
                if( $col > 31 ){
                    $yearIdx = $idx;
                    $year = $col;
                    unset($date[$idx]);

                } elseif( $col > 12 ){
                    $day = $col;
                    unset($date[$idx]);
                }

            // Not numeric = hour or month
            } elseif( preg_match('`^([0-9]{1,2}):([0-9]{2})(:[0-9]{2})?(AM|PM)?$`',
                    $col, $match) ){

                $hour = $match[1];
                $minute = $match[2];
                if( isset($match[4]) and $match[4] === 'PM' )
                    $hour += 12;

                unset($date[$idx]);

            } else {
                $month = strtolower($col);
                unset($date[$idx]);
            }
        }

        // day or month not found ?
        if( $day === false or $month === false ){
            /**
             * both are missing ? problem : can't know in which order they're
             * placed. So we suppose month placed before day, except if format
             * is DD-MM-YYYY
             */
            if( $day === false and $month === false and count($date) === 2
                and $yearIdx === 3 and isset($date[0]) and isset($date[1]) ){

                $day = $date[0];
                $month = $date[1];

            } else {
                foreach( $date as $idx => $col ){
                    if( $month === false ){
                        $month = $col;
                    } elseif( $day === false ){
                        $day = $col;
                    }
                }
            }

            if( $day === false or $month === false )
                return;
        }

        // month is not numeric ? try to convert it
        if( ! is_numeric($month) ){
            // can't have conversion ? exit
            if( ! isset($months[$month]) )
                return;
            $month = $months[$month];
        }

        // if year is not given, try to found it
        if( $year === false ){
            $actualMonth = date('m');
            $actualYear = date('Y');
            if($month > $actualMonth)
                $year = $actualYear - 1;
            else $year = $actualYear;
        }

        // here, we have all infos
        // if invalid time, then exit
        if(! ($time = @mktime($hour, $minute, 0, $month, $day, $year)) )
            return;

        $entry->time = $time;
        if( $hour !== false)
            $entry->timePrecision = self::TIME_PRECISION_HOUR;
        else $entry->timePrecision = self::TIME_PRECISION_DAY;
    }

}

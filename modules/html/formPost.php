<?php
class kore_html_formPost extends kore_html_form implements Countable
{
    const   TOKEN_NAME = '_koreToken';

    public function __construct( $action = NULL, $requireSession = null )
    {
        parent::__construct($action, 'post', $requireSession);
    }

    public function isSubmitted()
    {
        if ($this->_isSubmitted !== NULL)
            return $this->_isSubmitted;

        /*
         * Détecte si le formulaire a été soumis, ou non.
         */
        if (isset($_SERVER['REQUEST_METHOD']))
            $submitted = ($_SERVER['REQUEST_METHOD'] === 'POST');
        else
            $submitted = !empty($_POST);

        /*
         * Pas de soumission de formulaire, mais pas d'erreur à tracer.
         */
        if (!$submitted)
            return $this->_isSubmitted = false;

        $errorMsg = NULL;

        /*
         * If a token is required, we check it now.
         */
        if ($this->token) {

            if (!isset($_POST[self::TOKEN_NAME])) {
                /*
                 * The token was not given, track an error.
                 */
                $errorMsg = 'missing token';

            } elseif(! $this->token->validate($_POST[self::TOKEN_NAME])) {

                if (!$this->_legacyToken or !$this->_legacyToken->validate($_POST[self::TOKEN_NAME])){
                    /*
                     * Token fourni incorrect : erreur à tracer.
                     */
                    $errorMsg = 'bad token';
                }
            }
        }

        if ($errorMsg !== NULL){
            $submitted = false;
            kore::$error->track("refused form {$this->_action} ($errorMsg)",
                    null, array(
                        'action' => $this->_action,
                        'wanted token' => (string) $this->token,
                        'given token' => @ $_POST[self::TOKEN_NAME],
                        ));
        }

        if ($this->_isSubmitted = $submitted)
            $this->_filterInput();
        return $this->_isSubmitted;
    }

    protected function _filterInput() {
        foreach( $_POST as $name => $value ){
            $this->$name = $value;

            if( substr($name, -2) === '_x' ){
                $baseName = substr($name, 0, -2);
                if( !isset($_POST[$baseName]) and
                    isset($_POST[$baseName.'_y']) )
                    $this->$baseName = true;
            }
        }
    }

    public function __get($name) {
        if( ! $this->isSubmitted() ) return null;
        if( isset($this->$name) ) return $this->$name;
        return null;
    }

    public function __isset($name) {
        if( ! $this->isSubmitted() ) return false;
        return isset($this->$name);
    }

    public function exists( $name ) {
        return $this->__isset($name);
    }

    public function get($name) {
        return $this->__get($name);
    }

    public function getAll() {
        if( ! $this->isSubmitted() ) return array();
        return kore_html_formPost_dump($this);
    }



    /**
     * Méthodes utilisées par l'attribut kore:bind du moteur de template.
     */



    /**
     * Retourne le code HTML à placer après l'ouverture du tag <form> .
     *
     * @return string
     */
    public function postOpenTag()
    {
        /*
         * Si les tokens sont désactivés, il n'y a rien à injecter.
         */
        if (! $this->token)
            return '';

        /*
         * On place le champ <input /> dans un <div /> pour éviter que le code
         * XHTML ne soit plus valide.
         */
        return '<div><input type="hidden" name="'.self::TOKEN_NAME.'" value="'.
                htmlspecialchars($this->token, ENT_COMPAT,
                kore::$conf->response_charset). '" /></div>' . "\n";
    }

    /**
     * Implements Countable
     * @see Countable::count()
     * @return number
     */
    public function count()
    {
        if (!$this->isSubmitted()) return 0;
        return count($_POST);
    }

    /**
     * If form was submitted, return all values. Else an empty array will be
     * returned.
     *
     * @return array
     */
    public function getValues()
    {
        if (!$this->isSubmitted()) return array();
        return $_POST;
    }

}

function kore_html_formPost_dump( kore_html_formPost $obj )
{
    return get_object_vars($obj);
}


/*
    public function addHidden()
    {
        $item = new kore_html_formElement( 'hidden' );
        $this->elements[] = $item;
        return $item;
    }
    public function addText( $name )
    {
        $this->elements[ $name ] = new kore_html_formElement( 'text' );
        $this->elements[] = $item;
        return $item;
    }
    public function addPassword( $name )
    {
        $this->elements[ $name ] = new kore_html_formElement( 'password' );
        $this->elements[] = $item;
        return $item;
    }
    public function addSelect( $name )
    {
        $this->elements[ $name ] = new kore_html_formElement( 'select' );
        $this->elements[] = $item;
        return $item;
    }
    public function addRadio( $name )
    {
        $this->elements[ $name ] = new kore_html_formElement( 'radio' );
        $this->elements[] = $item;
        return $item;
    }
    public function addCheckbox( $name )
    {
        $this->elements[ $name ] = new kore_html_formElement( 'checkbox' );
        $this->elements[] = $item;
        return $item;
    }
    public function addSubmit( $name )
    {
        $this->elements[ $name ] = new kore_html_formElement( 'submit' );
        $this->elements[] = $item;
        return $item;
    }


    public function register_select( $name, $mandatory )
    {
    }
    public function register_checkbox( $name, $mandatory )
    {
    }
    public function register_radio( $name, $mandatory )
    {
    }
    public function register_textfield( $name, $mandatory )
    {
    }
    public function register_button( $name )
    {
    }

    protected function check_form()
    {
        if( $this->form_was_checked === true )
            return ;
        $this->form_was_checked = true;

        if( $this->method === 'post' )
            $source = & $_POST ;
        elseif( $this->method === 'get' )
            $source = & $_GET ;
        else
        {
            kore::$error->trigger( kore_error::WARN, 'HTML', "no valid method set", array( 'method' => $this->method ) );
            return ;
        }

        foreach( $source as $name => $value )
        {
            if( isset( $this->elements[ $name ] ) )
            {
                $this->elements[ $name ][ 'raw_value' ] = $value ;
            }
        }

        if( $this->accept_alternate_method !== NULL )
        {
            if( $this->accept_alternate_method === 'post' )
                $source = & $_POST ;
            else $source = & $_GET ;

            foreach( $source as $name => $value )
            {
                if( isset( $this->elements[ $name ] ) and ! isset( $this->elements[ $name ][ 'raw_value' ] ) )
                {
                    $this->elements[ $name ][ 'raw_value' ] = $value ;
                }
            }
        }
    }

    public function get_value( $name )
    {
        if( ! $this->form_was_checked )
            $this->check_form();

        if( isset( $this->elements[ $name ][ 'safe_value' ] ) )
            return $this->elements[ $name ][ 'safe_value' ];
        else return NULL ;
    }

    public function get_all_values()
    {
        if( ! $this->form_was_checked )
            $this->check_form();

        $out = array();
        foreach( $this->elements as $name => $infos )
        {
            if( isset( $infos[ 'safe_value' ] ) )
                $out[ $name ] = $infos[ 'safe_value' ];
            else $out[ $name ] = NULL ;
        }

        return $out ;
    }
*/

/*
class kore_html_formElement
{
    protected $type;
    protected $name = NULL;
    protected $value = NULL;

    public function __construct( $type )
    {
        $this->type = $type;
        return $this;
    }
    public function setName( $name )
    {
        $this->name = $name;
        return $this;
    }
    public function setValue( $value )
    {
        $this->value = $value;
        return $this;
    }
}

class kore_html_formElementText extends kore_html_formElement
{
    public function __construct()
    {
        return parent::__construct( 'text' );
    }
}
*/


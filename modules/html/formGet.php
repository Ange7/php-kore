<?php

class kore_html_formGet extends kore_html_form {

    const TOKEN_NAME = 'tk';

    private $_url;

    public function __construct( $action = NULL ) {
        parent::__construct($action, 'get');
    }

    public function postOpenTag() {
        return '';
    }

    public function count() {
        return count($_GET);
    }

    public function __get($name) {
        if (isset($_GET[$name]))
            return $_GET[$name];
    }
}
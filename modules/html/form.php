<?php

abstract class kore_html_form implements Countable
{
    public $token;
    protected $_legacyToken;

    protected $_action;
    protected $_method;

    protected $_isSubmitted;

    public function __construct($action, $method, $requireSession = null)
    {
        $this->_method = $method;

        if ($action !== NULL)
            $this->_action = $action;
        else $this->_action = kore_request_http::getUrl();

        $this->token = new kore_request_token($this->_action);
        if (kore::$conf->allow_legacy_token)
            $this->_legacyToken = new kore_request_tokenLegacy($this->_action,
                    $requireSession);
    }

    /**
     * Return the current token, if enabled.
     *
     * @return string
     */
    public function getToken()
    {
        if (!$this->token)
            return $this->token;

        return (string) $this->token;
    }

    /**
     * Disable token managment for this form.
     *
     */
    public function disableToken()
    {
        $this->token = false;
    }

    /**
     * Retourne les attributs à placer dans le tag <form> .
     *
     * @return string
     */
    public function inTag()
    {
        return 'method="'.$this->_method.'" action="' .
            htmlspecialchars($this->_action) . '"';
    }

    abstract public function postOpenTag();

    public function __get($name)
    {
        if (isset($this->$name)) return $this->$name;
        return null;
    }

    public function __isset($name)
    {
        return isset($this->$name);
    }

}
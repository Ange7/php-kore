<?php

class kore_user_privilegeList extends kore_db_mapping_list
{
    /**
     * Classe commune à tous les objets de la collection.
     */
    const CLASS_ITEM = 'kore_user_privilege';


    static function fromUser(kore_user $user)
    {
        $list = new static;

        $stmt = $list->_stmtGetFromUser();
        $list->_fetchStatement($stmt, array(
                'idUser1' => $user->getKey(),
                'idUser2' => $user->getKey()));

        return $list;
    }

    static function fromGroup(kore_user_group $group)
    {
        $list = new static;

        $stmt = $list->_stmtGetFromGroup();
        $list->_fetchStatement($stmt, array('idGroup' => $group->getKey()));

        return $list;
    }

    /**
     * Retourne un PDOStatement permettant de récupérer tous les utilisateurs
     * d'un groupe.
     * Un seul paramètre sera passé : idGroup
     *
     * @return PDOStatement
     */
    protected function _stmtGetFromGroup()
    {
        $db = $this->_getReadDb();
        $class = static::CLASS_ITEM;

        $myTable = $class::getFullTableName($db);
        list($joinGroup, $keyGroup) = $class::helperJoinRelGroup($db);

        return $db->prepare(
           "select ".$class::MAIN_COLS."
            from $myTable
            $joinGroup
            where $keyGroup = :idGroup");
    }

    /**
     * Retourne un PDOStatement permettant de récupérer tous les utilisateurs
     * d'un groupe.
     * Deux paramètres seront passés : idUser1 & idUser2
     *
     * @return PDOStatement
     */
    protected function _stmtGetFromUser()
    {
        $db = $this->_getReadDb();
        $class = static::CLASS_ITEM;

        $myTable = $class::getFullTableName($db);

        list($joinUser, $keyUser) = $class::helperJoinRelUser($db);
        list($joinGroup, $keyGroup) = $class::helperJoinRelGroup($db);
        $classGroup = $class::CLASS_GROUP;
        list($joinUserGroup, $keyUserGroup) = $classGroup::helperJoinRelUser($db,
            $class::TABLE_REL_GROUP,$classGroup::KEY_REL_PRIVILEGE);

        $query =
           "select ".$class::MAIN_COLS."
            from $myTable
            $joinUser
            where $keyUser = :idUser1
            union
            select ".$class::MAIN_COLS."
            from $myTable
            $joinGroup
            $joinUserGroup
            where $keyUserGroup = :idUser2";

        return $db->prepare($query);
    }

}

<?php
/*
create table kore_privilege (
    id varbinary(75) not null,
    primary key (id)
) engine=innodb;

create table kore_user_has_privilege (
    idUser varchar(50) not null,
    idPrivilege varbinary(75) not null,
    primary key (idUser, idPrivilege),
    key (idPrivilege),
    constraint fk_userPriv_user foreign key (idUser) references kore_user (login) on update cascade on delete cascade,
    constraint fk_userPriv_priv foreign key (idPrivilege) references kore_privilege (id) on update cascade on delete cascade
) engine=innodb;

create table kore_group_has_privilege (
    idGroup smallint unsigned not null,
    idPrivilege varbinary(75) not null,
    primary key (idGroup, idPrivilege),
    key (idPrivilege),
    constraint fk_groupPriv_group foreign key (idGroup) references kore_group (id) on update cascade on delete cascade,
    constraint fk_groupPriv_priv foreign key (idPrivilege) references kore_privilege (id) on update cascade on delete cascade
) engine=innodb;
*/

class kore_user_privilege extends kore_db_mapping_element
{
    /**
     * Liste des champs principaux.
     */
    const MAIN_COLS = 'id';

    /**
     * Classe permettant de gérer une collection d'utilisateurs.
     */
    const CLASS_COLLECTION = 'kore_user_privilegeList';

    /**
     * Nom de la table dans laquelle sont stockés les utilisateurs.
     */
    const TABLE_NAME = 'kore_privilege';

    /**
     * Nom de la classe permettant de gérer les utilisateurs (hérite de
     * kore_user).
     */
    const CLASS_USER = 'kore_user';

    /**
     * Nom de la classe permettant de gérer les groupes d'utilisateur (hérite de
     * kore_group).
     */
    const CLASS_GROUP = 'kore_user_group';


    const TABLE_REL_USER = 'kore_user_has_privilege';
    const KEY_REL_USER = 'idPrivilege';

    const TABLE_REL_GROUP = 'kore_group_has_privilege';
    const KEY_REL_GROUP = 'idPrivilege';



    static public function getTableKey($db)
    {
        return static::getTableAlias($db).'.'.
                $db->quoteKeyword(static::PRIMARY_KEY);
    }

    static public function helperJoinRelGroup($db)
    {
        $rel = $db->quoteKeyword(static::TABLE_REL_GROUP);
        $targetClass = static::CLASS_GROUP;
        return array(
                "join $rel on ".static::getTableKey($db).
                " = $rel." . $db->quoteKeyword(static::KEY_REL_GROUP),
                "$rel." . $db->quoteKeyword($targetClass::KEY_REL_PRIVILEGE));
    }

    static public function helperJoinRelUser($db)
    {
        $rel = $db->quoteKeyword(static::TABLE_REL_USER);
        $targetClass = static::CLASS_USER;
        return array(
                "join $rel on ".static::getTableKey($db)." = $rel.".$db->quoteKeyword(static::KEY_REL_USER),
                "$rel.".$db->quoteKeyword($targetClass::KEY_REL_PRIVILEGE));
    }

    /**
     * Retourne la liste des propriétés de l'objet, ainsi que le nom de la
     * fonction permettant son chargement à la volée, un booléen indiquant si la
     * propriété est accessible en écriture, et enfin le type dans lequel
     * formater les données.
     *
     * @return array
     */
    static protected function _getProperties()
    {
        $user = static::CLASS_GROUP;
        $userCollection = $user::CLASS_COLLECTION;

        $group = static::CLASS_GROUP;
        $groupCollection = $group::CLASS_COLLECTION;

        return array(
            /*
             * Identifiant du groupe (probablement la clé de la table).
             */
            static::PRIMARY_KEY    => static::_newProperty('_load'),

            /*
             * Relations avec d'autres classes.
             */
            'availableToUsers' => static::_newRelation1N($userCollection,      'fromPrivilege'),
            'availableToGroups'=> static::_newRelation1N($groupCollection,     'fromPrivilege'),
            );
    }

    /**
     * Ajoute un privilege à un utilisateur ou un groupe.
     *
     * @param kore_user/kore_user_group $target
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function grantTo($target, kore_date $startDate = null, kore_date $endDate = null)
    {
        if ($target instanceOf kore_user){
            // ?
        } elseif ($target instanceOf kore_user_group){
            // ?
        } else {
            throw new InvalidArgumentException("\$target must be an instance of kore_user or kore_user_group");
        }

        throw new Exception('TODO');
    }

    /**
     * Retire un privilege d'un utilisateur ou d'un groupe.
     *
     * @param kore_user/kore_user_group $target
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function revokeFrom($target, kore_date $startDate = null, kore_date $endDate = null)
    {
        if ($target instanceOf kore_user){
            // ?
        } elseif ($target instanceOf kore_user_group){
            // ?
        } else {
            throw new InvalidArgumentException("\$target must be an instance of kore_user or kore_user_group");
        }

        throw new Exception('TODO');
    }

}

<?php
/**
 * Prise en charge des erreurs lors de l'identification d'un utilisateur.
 *
 * @package user
 */

/**
 * Classe représentant une erreur lors de l'identification d'un utilisateur.
 *
 * @package user
 */
class kore_user_exception extends ErrorException
{
    const INSUFFISANT_PRIVILEGES = 1,
          MUST_BE_IDENTIFIED = 2,

          BAD_QUERY_RESULT = 10,

          BAD_ID_SYNTAX = 20,
          BAD_PASSWORD_SYNTAX = 21,
          PASSWORD_TOO_WEAK = 22,

          WRONG_CREDENTIAL = 30,

          SQL_ERROR = 50;
}

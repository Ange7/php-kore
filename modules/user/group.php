<?php

/*
create table kore_group (
    id smallint unsigned not null auto_increment,
    primary key (id)
) engine=innodb;

create table kore_user_in_group (
    idUser varchar(50) not null,
    idGroup smallint unsigned not null,
    startDate date null,
    endDate date null,
    primary key (idUser, idGroup),
    key (idGroup),
    constraint fk_userGroup_user foreign key (idUser) references kore_user (login) on update cascade on delete cascade,
    constraint fk_userGroup_group foreign key (idGroup) references kore_group (id) on update cascade on delete cascade
) engine=innodb;
*/



class kore_user_group extends kore_db_mapping_element
{
    /**
     * Liste des champs principaux.
     */
    const MAIN_COLS = 'id';

    /**
     * Classe permettant de gérer une collection d'utilisateurs.
     */
    const CLASS_COLLECTION = 'kore_user_groupList';

    /**
     * Nom de la table dans laquelle sont stockés les utilisateurs.
     */
    const TABLE_NAME = 'kore_group';

    /**
     * Nom de la classe permettant de gérer les utilisateurs (hérite de
     * kore_user).
     */
    const CLASS_USER = 'kore_user';

    /**
     * Nom de la classe permettant de gérer les privileges (hérite de
     * kore_user_privilege).
     */
    const CLASS_PRIVILEGE = 'kore_user_privilege';


    const TABLE_REL_USER = 'kore_user_in_group';
    const KEY_REL_USER = 'idGroup';

    const TABLE_REL_PRIVILEGE = 'kore_group_has_privilege';
    const KEY_REL_PRIVILEGE = 'idGroup';

    static public function getTableKey($db)
    {
        return static::getTableAlias($db).'.'.
                $db->quoteKeyword(static::PRIMARY_KEY);
    }

    static public function helperJoinRelPrivilege($db, $fromTable = null, $fromKey = null)
    {
        if ($fromTable === null)
            $fromTable = static::getTableAlias($db);
        else
            $fromTable = $db->quoteKeyword($fromTable);

        if ($fromKey === null) $fromKey = static::PRIMARY_KEY;

        $rel = $db->quoteKeyword(static::TABLE_REL_PRIVILEGE);
        $targetClass = static::CLASS_PRIVILEGE;
        return array(
                "join $rel on $fromTable.".$db->quoteKeyword($fromKey)." = $rel.
                ".$db->quoteKeyword(static::KEY_REL_PRIVILEGE),
                "$rel." . $db->quoteKeyword($targetClass::KEY_REL_GROUP));
    }

    static public function helperJoinRelUser($db, $fromTable = null, $fromKey = null)
    {
        if ($fromTable === null)
            $fromTable = static::getTableAlias($db);
        else
            $fromTable = $db->quoteKeyword($fromTable);

        if ($fromKey === null) $fromKey = static::PRIMARY_KEY;

        $rel = $db->quoteKeyword(static::TABLE_REL_USER);
        $targetClass = static::CLASS_USER;
        return array(
                "join $rel on $fromTable.".$db->quoteKeyword($fromKey)." = $rel.
                ".$db->quoteKeyword(static::KEY_REL_USER),
                "$rel." . $db->quoteKeyword($targetClass::KEY_REL_GROUP));
    }


    /**
     * Retourne la liste des propriétés de l'objet, ainsi que le nom de la
     * fonction permettant son chargement à la volée, un booléen indiquant si la
     * propriété est accessible en écriture, et enfin le type dans lequel
     * formater les données.
     *
     * @return array
     */
    static protected function _getProperties()
    {
        $user = static::CLASS_GROUP;
        $userCollection = $user::CLASS_COLLECTION;

        $privilege = static::CLASS_PRIVILEGE;
        $privilegeCollection = $privilege::CLASS_COLLECTION;

        return array(
            /*
             * Identifiant du groupe (probablement la clé de la table).
             */
            static::PRIMARY_KEY    => static::_newProperty('_load'),

            /*
             * Relations avec d'autres classes.
             */
            'hasUsers'         => static::_newRelation1N($userCollection,      'fromGroup'),
            'hasPrivileges'    => static::_newRelation1N($privilegeCollection, 'fromGroup'),
            );
    }

    /**
     * Indique si le groupe possède le privilège en question.
     *
     * @param  mixed $privilege
     * @return boolean
     */
    public function hasPrivilege( $privilege )
    {
        if (!$privilege instanceOf kore_user_privilege){
            $class = static::$_classPriv;
            $privilege = $class::fromId($privilege);
        }

        return $this->hasPrivileges->contains($privilege);
    }

    /**
     * Ajoute un utilisateur au groupe, durant la période indiquée.
     *
     * @param kore_user $user
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function addUser(kore_user $user, kore_date $startDate = null, kore_date $endDate = null)
    {
        $db = $this->_getWriteDb();

        $stmt = $this->_stmtInsertUser();

        $stmt->execute(array(
                'idGroup'   => $this->getKey(),
                'idUser'    => $user->getKey(),
                'startDate' => $startDate,
                'endDate'   => $endDate));


        $user->refreshGroups();
    }

    /**
     * Supprime un utilisateur du groupe
     *
     * @param kore_user $user
     */
    public function removeUser(kore_user $user)
    {
        $db = $this->_getWriteDb();

        $stmt = $this->_stmtDeleteUser();

        $stmt->execute(array(
                'idGroup'   => $this->getKey(),
                'idUser'    => $user->getKey()));

        $user->refreshGroups();
    }

    /**
     * Ajoute un privilege au groupe.
     *
     * @param kore_user_privilege $privilege
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function grant(kore_user_privilege $privilege, kore_date $startDate = null, kore_date $endDate = null)
    {
        $privilege->grantTo($this, $startDate, $endDate);
    }

    /**
     * Retire un privilege au groupe.
     *
     * @param kore_user_privilege $privilege
     * @param kore_date $startDate
     * @param kore_date $endDate
     */
    public function revoke(kore_user_privilege $privilege, kore_date $startDate = null, kore_date $endDate = null)
    {
        $privilege->revokeFrom($this, $startDate, $endDate);
    }


    /**
     * Retourne un PDOStatement permettant d'ajouter un utilisateur au groupe.
     * Quatre paramètres seront passés : idGroup, idUser, startDate, endDate
     *
     * @return PDOStatement
     */
    protected function _stmtInsertUser()
    {
        $db = $this->_getWriteDb();

        $classUser = static::CLASS_USER;
        $keyRelUser = $db->quoteKeyword(static::KEY_REL_USER);
        $keyRelGroup = $db->quoteKeyword($classUser::KEY_REL_GROUP);

        return $db->prepare(
           "insert into ".$db->quoteKeyword(static::TABLE_REL_USER)."
                (".$keyRelUser.", ".$keyRelGroup.", startDate, endDate)
            values (:idGroup, :idUser, :startDate, :endDate)");
    }

    /**
     * Retourne un PDOStatement permettant de retirer un utilisateur au groupe.
     * Deux paramètres seront passés : idGroup, idUser
     *
     * @return PDOStatement
     */
    protected function _stmtDeleteUser()
    {
        $db = $this->_getWriteDb();

        $classUser = static::CLASS_USER;

        $keyRelUser = $db->quoteKeyword(static::KEY_REL_USER);
        $keyRelGroup = $db->quoteKeyword($classUser::KEY_REL_GROUP);

        return $db->prepare(
           "delete from ".$db->quoteKeyword(static::TABLE_REL_USER)."
            where $keyRelUser = :idGroup and $keyRelGroup = :idUser");
    }

}

<?php
/**
 * The kore_request_tokenLegacy reproduce the same token behavior of Kore,
 * before that "kore_request_token" was available.
 *
 * This class is not tunable, less secured, and since it don't use time slice it
 * can also refuse some legitimate tokens.
 * Use it only to ensure smooth transition to kore_request_token.
 */
class kore_request_tokenLegacy extends kore_request_token
{
    /**
     * Some random data to integrate in the salt.
     *
     * @var string
     */
    protected $_additionnalSalt = '';

    /**
     * List of data sources to populate the salt (bit mask).
     *
     * @var integer
     */
    protected $_saltSources = 0;

    /**
     * Time (in seconds) after what the salt change
     *
     * @var integer
     */
    protected $_timeSlice = 432000;

    /**
     * Number of time slice which are accepted.
     *
     * @var integer
     */
    protected $_nbSlice = 1;

    /**
     * Use session ID instead of a timeout.
     *
     * @var boolean
     */
    protected $_useSession;

    /**
     * Initiate an instance of kore_request_token
     *
     * @param string  $baseValue
     */
    public function __construct($baseValue, $withSession = null)
    {
        parent::__construct($baseValue);

        $this->setSalt(kore::getWebDir());

        if ($withSession === null)
            $this->_useSession = kore::$session->isEnabled();
        else
            $this->_useSession = (bool) $withSession;
    }

    /**
     * Hash and encode data to build the final token.
     *
     * @param  string  $rawData
     * @return string
     */
    protected function _encodeToken($rawData)
    {
        if ($this->_useSession)
            $salt = kore::$session->getId();
        else
            $salt = kore::$conf->http_token_salt;

        if (!empty($salt)){
            /*
             * We need to remove the timeSlice composent of current rawData
             */
            if ($pos = strpos($rawData, '/'))
                $rawData = $salt . substr($rawData, $pos);
        }

        return md5($rawData);
    }

}
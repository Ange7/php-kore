<?php
/**
 * Gère le contexte d'exécution CLI.
 *
 * @package request
 */

/**
 * Gère le contexte d'exécution HTTP.
 *
 * @package request
 */
class kore_request_cli
{
    protected static $_params;
    protected static $_options = array();

    private static function _compileParams()
    {
        if( self::$_params !== NULL ) return;

        self::$_params = array();
        self::$_options = array();

        $parse = true;
        foreach( $_SERVER['argv'] as $idx => $param ){
            if( $idx === 0 ) continue;

            if( $parse === false ) {
                self::$_params[] = $param;
                continue;
            }
            if( $param === '--' ) {
                $parse = false;
                continue;
            }
            if( substr($param, 0, 2) === '--' ){
                $name = substr($param, 2);
                $value = true;
                if( ( $pos = strpos($param, '=') ) !== false ){
                    $name = substr($param, 2, $pos - 2);
                    $value = substr($param, $pos + 1);
                }
                /*
                 * Si le paramètre n'existe pas on le crée
                 * sinon on génère un tableau qui contiendra toutes les valeurs.
                 */
                if (!isset(self::$_options[$name])) {
                    self::$_options[$name] = $value;
                } else {
                    if (!is_array(self::$_options[$name]))
                        self::$_options[$name] = array( self::$_options[$name] );

                    self::$_options[$name][]=$value;
                }
                continue;
            }

            self::$_params[] = $param;
        }
    }

    public static function getScriptName()
    {
        return $_SERVER['argv'][0];
    }

    public static function countOptions()
    {
        self::_compileParams();

        return count(self::$_options);
    }

    public static function getOptions()
    {
        self::_compileParams();

        return self::$_options;
    }

    public static function getOption( $name, $defaultValue = NULL )
    {
        self::_compileParams();

        if( isset(self::$_options[$name]) )
            return self::$_options[$name];
        return $defaultValue;
    }

    public static function countParams()
    {
        self::_compileParams();

        return count(self::$_params);
    }

    public static function getParams( )
    {
        self::_compileParams();

        return self::$_params;
    }

    public static function getParam( $idx )
    {
        self::_compileParams();

        if( isset(self::$_params[$idx]) )
            return self::$_params[$idx];
        return NULL;
    }
}

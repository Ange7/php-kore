<?php
/**
 * Class to add DateTimeImmutable functionalities in PHP < 5.5
 *
 * @package php
 */
class kore_php_DateTimeImmutable extends DateTime
{
    public function add($interval)
    {
        $obj = clone $this;
        return $obj->_parentAdd($interval);
    }

    public function modify($modify)
    {
        $obj = clone $this;
        return $obj->_parentModify($modify);
    }

    public function setDate($year, $month, $day)
    {
        $obj = clone $this;
        return $obj->_parentSetDate($year, $month, $day);
    }

    public function setISODate($year, $week, $day = 1)
    {
        $obj = clone $this;
        return $obj->_parentSetISODate($year, $week, $day);
    }

    public function setTime($hour, $minute, $second = 0)
    {
        $obj = clone $this;
        return $obj->_parentSetTime($hour, $minute, $second);
    }

    public function setTimestamp($unixtimestamp)
    {
        $obj = clone $this;
        return $obj->_parentSetTimestamp($unixtimestamp);
    }

    public function setTimezone($timezone )
    {
        $obj = clone $this;
        return $obj->_parentSetTimezone($timezone);
    }

    public function sub($interval)
    {
        $obj = clone $this;
        return $obj->_parentSub($interval);
    }

    protected function _parentAdd($interval)
    {
        return parent::add($interval);
    }

    protected function _parentModify($modify)
    {
        return parent::modify($modify);
    }

    protected function _parentSetDate($year, $month, $day)
    {
        return parent::setDate($year, $month, $day);
    }

    protected function _parentSetISODate($year, $week, $day)
    {
        return parent::setISODate($year, $week, $day);
    }

    protected function _parentSetTime($hour, $minute, $second)
    {
        return parent::setTime($hour, $minute, $second);
    }

    protected function _parentSetTimestamp($unixtimestamp)
    {
        return parent::setTimestamp($unixtimestamp);
    }

    protected function _parentSetTimezone($timezone)
    {
        return parent::setTimezone($timezone);
    }

    protected function _parentSub($interval)
    {
        return parent::sub($interval);
    }
}
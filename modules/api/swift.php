<?php
/**
 * Handle Swift API to manipulate OpenStack Object Storage.
 *
 */
class kore_api_swift extends kore_api_rest
{
    /**
     * Are we authenticated on the swift server ?
     *
     * @var boolean
     */
    protected $_authenticated = false;

    /**
     * Default ContentType to use with objectCreate()
     *
     * @var string
     */
    protected $_defaultContentType;


    /**
     * Authenticate the user on the swift server.
     * Note that the method returns a new kore_api_swift instance.
     *
     * @param  string  $user
     * @param  string  $key
     * @return kore_api_swift
     */
    public function auth($user, $key)
    {
        $res = $this->get('/auth', null, array(
                'X-Auth-User' => $user,
                'X-Auth-Key'  => $key));

        /*
         * Create a new kore_api_swift instance, based on the new X-Storage-Url.
         */
        $authenticatedSwift = new static($res->headers['x-storage-url']);
        $authenticatedSwift->setToken($res->headers['x-auth-token']);

        /*
         * Copy current settings.
         */
        $authenticatedSwift->_defaultContentType = $this->_defaultContentType;

        return $authenticatedSwift;
    }

    /**
     * Register token to use to be authenticated.
     *
     * @param string $token
     */
    public function setToken($token)
    {
        $this->setHeader('X-Auth-Token', $token);
        $this->_authenticated = true;
    }

    /**
     * Verify the syntax of the container name.
     *
     * @param  string  $container
     * @throws kore_api_swiftException
     */
    protected function _checkContainerName($container)
    {
        if (!is_string($container))
            throw new kore_api_swiftException("container name can't be empty");
        if (preg_match('#[^a-zA-Z0-9\\-]#', $container))
            throw new kore_api_swiftException("container name contains invalid chars");
    }

    /**
     * Verify the syntax of the meta name.
     *
     * @param  string  $meta
     * @throws kore_api_swiftException
     */
    protected function _checkMetaName($meta)
    {
        if (!is_string($meta))
            throw new kore_api_swiftException("meta name can't be empty");
        if (preg_match('#[^a-zA-Z0-9_\\-]#', $meta))
            throw new kore_api_swiftException("meta name contains invalid chars");
    }

    /**
     * List all containers (returns an array of stdClass).
     *
     * @param  string  $marker
     * @param  string  $endMarker
     * @param  integer $limit
     * @return array
     */
    public function containersList($marker = null, $endMarker = null, $limit = null)
    {
        $params = array('format' => 'json');
        if ($marker !== null)      $params['marker'] = $marker;
        if ($endMarker !== null)   $params['endMarker'] = $endMarker;
        if ($limit !== null)       $params['limit'] = $limit;

        $res = $this->get('/', $params);
        return json_decode($res->contents);
    }

    /**
     * Create a new container, with ACL and metas.
     *
     * @param  string $container
     * @param  array  $readUsers
     * @param  array  $writeUsers
     * @param  array  $additionnalMetas
     * @return kore_api_restResult
     */
    public function containerCreate($container, array $readUsers = null, array $writeUsers = null, array $additionnalMetas = null)
    {
        $this->_checkContainerName($container);

        $headers = array();
        if ($readUsers)
            $headers['X-Container-Read'] = implode(',', $readUsers);
        if ($writeUsers)
            $headers['X-Container-Write'] = implode(',', $writeUsers);

        if ($additionnalMetas)
            foreach ($additionnalMetas as $key => $value){
                $this->_checkMetaName($key);
                $headers['X-Container-Meta-' . $key] = $value;
            }

        return $this->put("/$container", null, $headers);
    }

    /**
     * Get metas of the container.
     *
     * @param  string  $container
     * @return kore_api_restResult
     */
    public function containerGetMetas($container)
    {
        $this->_checkContainerName($container);

        return $this->head("/$container");
    }

    /**
     * Add or update metas of the container.
     *
     * @param  string  $container
     * @param  array   $additionnalMetas
     * @return kore_api_restResult
     */
    public function containerSetMeta($container, array $additionnalMetas)
    {
        return $this->containerUpdate($container, null, null, $additionnalMetas);
    }

    /**
     * Delete metas of the container.
     *
     * @param  string  $container
     * @param  array   $metasToDelete
     * @return kore_api_restResult
     */
    public function containerDeleteMeta($container, array $metasToDelete)
    {
        $metas = array();
        foreach ($metasToDelete as $key)
            $metas[$key] = '';

        return $this->containerUpdate($container, null, null, $metas);
    }

    /**
     * Update ACL of the container.
     *
     * @param  string $container
     * @param  array  $readUsers
     * @param  array  $writeUsers
     * @return kore_api_restResult
     */
    public function containerUpdateACL($container, array $readUsers = null, array $writeUsers = null)
    {
        return $this->containerUpdate($container, $readUsers, $writeUsers);
    }

    /**
     * Update ACL and metas of the container.
     *
     * @param  string  $container
     * @param  array   $readUsers
     * @param  array   $writeUsers
     * @param  array   $additionnalMetas
     * @return kore_api_restResult
     */
    public function containerUpdate($container, array $readUsers = null, array $writeUsers = null, array $additionnalMetas = null)
    {
        $this->_checkContainerName($container);

        $headers = array();
        if ($readUsers)
            $headers['X-Container-Read'] = implode(',', $readUsers);
        if ($writeUsers)
            $headers['X-Container-Write'] = implode(',', $writeUsers);

        if ($additionnalMetas)
            foreach ($additionnalMetas as $key => $value){
                $this->_checkMetaName($key);
                $headers['X-Container-Meta-' . $key] = $value;
            }

        return $this->post("/$container", null, $headers);
    }

    /**
     * Delete a container (if it's empty).
     *
     * @param  string $container
     * @return kore_api_restResult
     */
    public function containerDelete($container)
    {
        $this->_checkContainerName($container);

        return $this->delete("/$container");
    }

    /**
     * Remove a container and all its contents.
     *
     * @param  string $container
     * @return kore_api_restResult
     */
    public function containerDrop($container)
    {
        $this->_checkContainerName($container);

        $limit = 1000;

        do {
            $removed = 0;

            $objects = $this->objectsList($container, null, null, $limit);
            if ($objects){
                foreach ($objects as $object){
                    $this->objectDelete($container, $object->name);
                    $removed++;
                }
            }
        } while ($removed === $limit);

        return $this->containerDelete($container);
    }

    /**
     * List objects from a container (returns an array of stdClass).
     *
     * @param  string  $container
     * @param  string  $prefix
     * @param  string  $marker
     * @param  integer $limit
     * @param  string  $delimiter
     * @param  string  $path
     * @return array
     */
    public function objectsList($container, $prefix = null, $marker = null, $limit = null, $delimiter = null, $path = null)
    {
        $this->_checkContainerName($container);

        $params = array('format' => 'json');
        if ($prefix !== null)   $params['prefix'] = $prefix;
        if ($limit !== null)    $params['limit'] = $limit;
        if ($marker !== null)   $params['marker'] = $marker;
        if ($delimiter !== null) $params['delimiter'] = $delimiter;
        if ($path !== null)     $params['path'] = $path;

        $res = $this->get("/$container", $params);
        return json_decode($res->contents);
    }

    /**
     * Add an object in the container.
     *
     * @param  string $container
     * @param  string $object
     * @param  string $data
     * @param  string $contentType
     * @param  string $contentEncoding
     * @param  string $contentDisposition
     * @param  string $transferEncoding
     * @param  string $deleteAt
     * @param  string $deleteAfter
     * @param  string $cacheControl
     * @return kore_api_restResult
     */
    public function objectCreate($container, $object, $data = null,
            $contentType = null, $contentEncoding = null,
            $contentDisposition = null, $transferEncoding = null,
            $deleteAt = null, $deleteAfter = null, $cacheControl = null)
    {
        $this->_checkContainerName($container);

        $headers = array();
        if ($data !== null)
            $headers['ETag'] = md5($data);

        if ($contentType === null)
            $contentType = $this->_defaultContentType;
        if ($contentType !== null)
            $headers['Content-Type'] = $contentType;

        if ($contentEncoding !== null)
            $headers['Content-Encoding'] = $contentEncoding;
        if ($contentDisposition !== null)
            $headers['Content-Disposition'] = $contentDisposition;

        if ($transferEncoding !== null)
            $headers['Transfer-Encoding'] = $transferEncoding;

        if($cacheControl !== null)
            $headers['Cache-Control'] = $cacheControl;

        if ($deleteAt !== null)
            $headers['X-Delete-At'] = $deleteAt;
        if ($deleteAfter !== null)
            $headers['X-Delete-After'] = $deleteAfter;

        return $this->put("/$container/$object", $data, $headers);
    }

    /**
     * Copy an object.
     *
     * @param  string  $sourceContainer
     * @param  string  $sourceObject
     * @param  string  $destContainer
     * @param  string  $destObject
     * @param  string  $ifModifiedSince
     * @param  string  $ifUnmodifiedSince
     * @param  string  $copyIfMatch
     * @param  string  $copyIfNoneMatch
     * @return kore_api_restResult
     */
    public function objectCopy($sourceContainer, $sourceObject, $destContainer, $destObject, $ifModifiedSince = null, $ifUnmodifiedSince = null, $copyIfMatch = null, $copyIfNoneMatch = null)
    {
        $this->_checkContainerName($container);

        $headers = array('X-Copy-From:' => "$sourceContainer/$sourceObject");

        if ($ifModifiedSince !== null)
            $headers['If-Modified-Since'] = $ifModifiedSince;
        if ($ifUnmodifiedSince !== null)
            $headers['If-Unmodified-Since'] = $ifUnmodifiedSince;
        if ($copyIfMatch !== null)
            $headers['Copy-If-Match'] = $copyIfMatch;
        if ($copyIfNoneMatch !== null)
            $headers['Copy-If-None-Match'] = $copyIfNoneMatch;

        return $this->put("/$destContainer/$destObject");
    }

    /**
     * Rename an object (it will copy to destination then delete source).
     *
     * @param  string  $sourceContainer
     * @param  string  $sourceObject
     * @param  string  $destContainer
     * @param  string  $destObject
     * @param  string  $ifModifiedSince
     * @param  string  $ifUnmodifiedSince
     * @param  string  $copyIfMatch
     * @param  string  $copyIfNoneMatch
     * @return kore_api_restResult
     */
    public function objectMove($sourceContainer, $sourceObject, $destContainer, $destObject, $ifModifiedSince = null, $ifUnmodifiedSince = null, $copyIfMatch = null, $copyIfNoneMatch = null)
    {
        $this->objectCopy($sourceContainer, $sourceObject, $destContainer,
                $destObject, $ifModifiedSince, $ifUnmodifiedSince, $copyIfMatch,
                $copyIfNoneMatch);
        return $this->objectDelete($sourceContainer, $sourceObject);
    }

    /**
     * Delete an object from the container.
     *
     * @param  string  $container
     * @param  string  $object
     * @return kore_api_restResult
     */
    public function objectDelete($container, $object)
    {
        $this->_checkContainerName($container);

        return $this->delete("/$container/$object");
    }

    /**
     * Get an object from the container.
     *
     * @param  string  $container
     * @param  string  $object
     * @param  string  $range
     * @param  string  $ifModifiedSince
     * @param  string  $ifUnmodifiedSince
     * @return kore_api_restResult
     */
    public function objectGet($container, $object, $range = null, $ifModifiedSince = null, $ifUnmodifiedSince = null)
    {
        $this->_checkContainerName($container);

        $headers = array();

        if ($range !== null)
            $headers['Range'] = $range;
        if ($ifModifiedSince !== null)
            $headers['If-Modified-Since'] = $ifModifiedSince;
        if ($ifUnmodifiedSince !== null)
            $headers['If-Unmodified-Since'] = $ifUnmodifiedSince;

        return $this->get("/$container/$object", null, $headers);
    }

    /**
     * Get object metadatas.
     *
     * @param  string  $container
     * @param  string  $object
     * @return kore_api_restResult
     */
    public function objectGetMetas($container, $object)
    {
        $this->_checkContainerName($container);

        return $this->head("/$container/$object");
    }

    /**
     * Set object metadatas.
     *
     * @param  string  $container
     * @param  string  $object
     * @param  array   $metas
     * @return kore_api_restResult
     */
    public function objectSetMeta($container, $object, array $metas)
    {
        $this->_checkContainerName($container);

        $headers = array();
        foreach ($metas as $key => $value){
            $this->_checkMetaName($key);
            $headers['X-Container-Meta-' . $key] = $value;
        }

        return $this->post("/$container/$object", null, $headers);
    }



    /**
     * Manage the PUT method.
     *
     * It seems that for some SWIFT servers (like RadosGW) the Content-Length
     * header is mandatory with the PUT method, but PHP add it only if there is
     * contents. So we override the default method.
     *
     * @see kore_api_rest::put()
     */
    public function put($url, $data = null, array $miscHeaders = null)
    {
        if (((string)$data) === '')
            $miscHeaders['Content-Length'] = 0;

        return parent::put($url, (string)$data, $miscHeaders);
    }

}

/**
 * Handle SWIFT specific exceptions.
 *
 */
class kore_api_swiftException extends kore_api_restException
{
}

<?php
/**
 * Class providing simple access to the Twitter API.
 * See https://dev.twitter.com/
 */
class kore_api_twitter extends kore_api_rest
{
    const BASE_URL = 'https://api.twitter.com/1.1';

    private $_oauthAccessToken;
    private $_oauthAccessTokenSecret;

    private $_consumerKey;
    private $_consumerSecret;

    private $_additionalData;

    /**
     * Handle direct auth for single user.
     *
     * @param  string $oauthToken
     * @param  string $oauthSecret
     * @param  string $consumerKey
     * @param  string $consumerSecret
     * @return api_twitter
     */
    static public function oAuth($oauthToken, $oauthSecret, $consumerKey, $consumerSecret)
    {
        $api = new static(static::BASE_URL);
        $api->setOAuthCredential($oauthToken, $oauthSecret);
        $api->setConsumer($consumerKey, $consumerSecret);

        return $api;
    }

    /**
     * Register access tokens
     *
     * @param string $token
     * @param string $secret
     */
    public function setOAuthCredential($token, $secret)
    {
        $this->_oauthAccessToken = $token;
        $this->_oauthAccessTokenSecret = $secret;
    }

    /**
     * Set or change the consumerKey credentials.
     *
     * @param string $consumerKey
     * @param string $consumerSecret
     */
    public function setConsumer($consumerKey, $consumerSecret)
    {
        $this->_consumerKey = $consumerKey;
        $this->_consumerSecret = $consumerSecret;
    }

    /**
     * Build the base string used to make the OAuth signature
     *
     * @param  string $uri
     * @param  string $method
     * @param  null/array $postParams
     * @param  array  $oauth
     * @return string
     */
    protected function _buildBaseString($uri, $method, $postParams, array $oauth)
    {
        if ( ($pos = strpos($uri, '?')) === false ){
            $baseUrl = $uri;
        } else {
            $baseUrl = substr($uri, 0, $pos);
            $urlParams = substr($uri, $pos + 1);
            foreach (explode('&', $urlParams) as $param){
                list($key, $value) = @explode('=', $param, 2);
                $oauth[$key] = $value;
            }
        }

        if (is_array($postParams))
            foreach ($postParams as $key => $value)
                $oauth[$key] = $value;

        ksort($oauth);

        return $method.'&'.rawurlencode($baseUrl).'&'.rawurlencode(
                http_build_query($oauth, '', '&', PHP_QUERY_RFC3986));
    }

    /**
     * Trace sent data, to be able to build the signature.
     *
     * @param  mixed $data
     * @return string
     */
    protected function _encodeData($data)
    {
        $this->_additionalData = $data;

        return parent::_encodeData($data);
    }

    /**
     * Before requesting the server, we have to add the signature required by
     * OVH.
     *
     * @see    kore_api_restJson::_query()
     * @param  string $url
     * @param  array  $contextOpts
     * @return kore_api_restResult
     * @throws kore_api_restException
     */
    protected function _query($url, array $contextOpts)
    {
        if ($this->_consumerKey){
            /*
             * «nonce» stand for «number used once», it's a unique and random
             * string, identifying each signed request.
             *
             * For now, we use timestamp for this, but it's probably not a great
             * idea.
             */
            $TS = time();
            $nonce = $TS;

            $oauth = [
                'oauth_consumer_key'     => $this->_consumerKey,
                'oauth_nonce'            => rtrim(base64_encode($nonce), '='),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_timestamp'        => $TS,
                'oauth_token'            => $this->_oauthAccessToken,
                'oauth_version'          => '1.0',
                ];

            $baseInfo = $this->_buildBaseString($url,
                    $contextOpts['http']['method'], $this->_additionalData,
                    $oauth);

            $oauth['oauth_signature'] = base64_encode(hash_hmac('sha1',
                    $baseInfo, rawurlencode($this->_consumerSecret).'&'.
                    rawurlencode($this->_oauthAccessTokenSecret), true));

            ksort($oauth);

            $oauthHeader = '';
            foreach ($oauth as $key => $value){
                if ($oauthHeader !== '')
                    $oauthHeader .= ', ';
                $oauthHeader .= $key .'="'.rawurlencode($value).'"';
            }
            $contextOpts['http']['header'] .=
                    "Authorization: OAuth $oauthHeader\r\nExpect:\r\n";
        }

        /*
         * Clear this cache.
         */
        $this->_additionalData = null;

        return parent::_query($url, $contextOpts);
    }

    /**
     * Fetch the result : try to detect and decode output formats.
     *
     * @param  kore_api_restResult $result
     * @return kore_api_restResult
     */
    protected function _fetchContents(kore_api_restResult $result)
    {
        /*
         * The parent may have to handle data decompression.
         */
        $result = parent::_fetchContents($result);

        /*
         * If the result is in JSON format, decode it.
         */
        if (isset($result->headers['content-type']) and
            strpos($result->headers['content-type'], '/json')){
            $result->contents = json_decode($result->contents);
        }

        return $result;
    }

}

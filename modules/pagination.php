<?php

/*
  {hidden:list(pagesBefore, pagesAfter) = pagination.getPageRange()}
  <a kore:foreach="pagesBefore, pageNum, pageUrl"
    href="{pageUrl}">{pageNum}</a>
  <a class="active" href="{pagination.getUrl()}"
    >{pagination.getCurrentPage()}</a>
  <a kore:foreach="pagesAfter, pageNum, pageUrl"
    href="{pageUrl}">{pageNum}</a>
*/

/**
 * Classe permettant de gérer la pagination d'une liste.
 */
class kore_pagination
{
    const URL_PARAM = 'p';

    /**
     * Url de la page, sans paramètre de pagination.
     *
     * @var string
     */
    protected $_baseUrl;



    /**
     * Nombre de pages, si renseigné.
     *
     * @var number
     */
    private $_nbPages;



    /**
     * Numéro de la page courante.
     *
     * @var number
     */
    protected $_currentPage = 1;

    /**
     * Nombre d'éléments par page.
     *
     * @var number
     */
    protected $_maxItemsPerPage = 25;


    /**
     * Autorise ou non la page 0, affichant tous les élements.
     *
     * @var boolean
     */
    protected $_allowAllItems = false;


    /**
     * Initialise un objet kore_pagination en fonction de l'URL courante.
     *
     * @throws kore_paginationException
     * @return kore_pagination
     */
    public static function fromRequest()
    {
        $baseUrl = kore_request_http::getPage();

        $currentPage = null;

        $currentParams = $_GET;
        if (isset($currentParams[static::URL_PARAM])){
            $currentPage = $currentParams[static::URL_PARAM];
            unset($currentParams[static::URL_PARAM]);
        }
        if ($currentParams)
            $baseUrl .= '?'.http_build_query($currentParams);

        $pagination = new static($baseUrl);

        if ($currentPage !== null)
            $pagination->setCurrentPage($currentPage);

        return $pagination;
    }

    /**
     * Initialise un objet kore_pagination en fonction d'un objet
     * kore_route_route. La route fournie est ainsi modifiée afin que la page
     * courante soit retirée de l'URL.
     *
     * @param  kore_route_route $route
     * @return kore_pagination
     */
    public static function fromRoute(kore_route_route $route)
    {
        $paramName = static::URL_PARAM;
        $currentPage = null;

        if (isset($route->params[$paramName])){
            $currentPage = $route->params[$paramName];
            $route->remove($paramName);
        }

        $pagination = new static($route->reverse());
        if ($currentPage !== null)
            $pagination->setCurrentPage($currentPage);

        return $pagination;
    }

    /**
     * Instanciation de kore_pagination
     *
     * @param  string $baseUrl
     * @return kore_pagination
     */
    public function __construct($baseUrl)
    {
        $this->_baseUrl = (string) $baseUrl;
    }


    /**
     * Défini le nombre d'élément maximum par page
     *
     * @param number $maxItemsPerPage
     */
    public function setMaxPerPage( $maxItemsPerPage )
    {
        $this->_maxItemsPerPage = (int) $maxItemsPerPage;
    }

    /**
     * Retourne le nombre d'élément maximum par page
     *
     * @return number
     */
    public function getMaxPerPage()
    {
        return $this->_maxItemsPerPage;
    }

    /**
     * Indique si la page 0 est autorisée. Cette page affiche tous les élements
     * de la liste.
     *
     * @param boolean $allow
     */
    public function allowAllItems( $allow )
    {
        $this->_allowAllItems = (bool) $allow;
    }

    /**
     * Indique si la page 0 est autorisée.
     *
     * @return boolean
     */
    public function areAllItemsAllowed()
    {
        return $this->_allowAllItems;
    }

    /**
     * Retourne l'URL de la page indiquée, ou de la page courante si non
     * précisée.
     *
     * @param  number $page
     * @return string
     */
    public function getUrl( $page = NULL )
    {
        if ($page === NULL)
            $page = $this->_currentPage;
        else $page = $this->_validatePage($page);

        $url = $this->_baseUrl;
        if ($page !== 1) {
            $param = static::URL_PARAM.'='.$page;

            if (strpos($url, '?') !== false)
                $url .= '&'. $param;
            else $url .= '?'. $param;
        }

        return $url;
    }

    /**
     * Retourne le nombre total de pages
     *
     * @return number
     */
    public function getNbPages()
    {
        return $this->_nbPages;
    }

    /**
     * Retourne le numéro de la page courante
     *
     * @return number
     */
    public function getCurrentPage()
    {
        return $this->_currentPage;
    }

    /**
     * Change le numéro de la page courante
     *
     * @param number $page
     * @throws kore_paginationException
     */
    public function setCurrentPage($page)
    {
        $this->_currentPage = $this->_validatePage($page);
    }

    /**
     * Vérifie que le numéro de page fourni soit syntaxiquement correct.
     *
     * @param  string $page
     * @throws kore_paginationException
     * @return number
     */
    protected function _validatePage($page)
    {
        if (!preg_match('#^(0|[1-9]+[0-9]*)$#', $page))
            throw new kore_paginationException("syntax error in page number",
                    $this->getUrl(1));

        return (int) $page;
    }


    /**
     * Renseigne le nombre d'éléments total, afin d'en déduire le nombre de
     * pages.
     *
     * @param  number $nbItems
     * @return number
     */
    public function registerNumberOfItems($nbItems)
    {
        $this->_nbPages = ceil($nbItems / $this->_maxItemsPerPage);
        if ($this->_nbPages < 1)
            $this->_nbPages = 1;

        return $this->_nbPages;
    }

    /**
     * Renseigne le nombre de pages.
     *
     * @param  number $nbPages
     * @return number
     */
    public function registerNumberOfPages($nbPages)
    {
        $this->_nbPages = (int) $nbPages;
        if ($this->_nbPages < 1)
            $this->_nbPages = 1;
    }

    /**
     * Vérifie que le numéro de page courante soit bien valide.
     *
     * @return boolean
     */
    public function isValid()
    {
        return ( ( ( $this->_currentPage === 0 and $this->_allowAllItems) or
                $this->_currentPage > 0 ) and
                $this->_currentPage <= $this->_nbPages );
    }

    /**
     * Vérifie que le numéro de page courante soit bien valide et déclenche une
     * exception si ce n'est pas le cas.
     *
     * @throws kore_paginationException
     */
    public function verifyIsValid()
    {
        if($this->isValid())
            return;

        if($this->_nbPages === NULL)
            throw new kore_paginationException("Number of pages wasn't registered",
                    $this->getUrl(1));

        if($this->_currentPage === 0 and !$this->_allowAllItems)
            throw new kore_paginationException("Returning all items is not allowed",
                    $this->getUrl(1));

        throw new kore_paginationException("Invalid page {$this->_currentPage} / {$this->_nbPages}",
                $this->getUrl(1));
    }


    /**
     * Retourne une chaîne pouvant être utilisée à l'intérieur d'une requête SQL
     * afin de limiter les résultats provenant d'une base de données.
     *
     * @return string
     */
    public function getSQLLimit()
    {
        $this->verifyIsValid();

        $limit = '';
        if ($this->_currentPage === 0)
            return $limit;

        $min = ($this->_currentPage - 1) * $this->_maxItemsPerPage;
        $nb  = $this->_maxItemsPerPage;

        $limit = "\nlimit $min, $nb";
        return $limit;
    }

    /**
     * Retourne un tableau délimitant les bornes min et max des items de la page,
     * en commençant la numérotation à 0.
     *
     * @throws kore_paginationException
     * @return array
     */
    public function getItemRange()
    {
        $this->verifyIsValid();

        if ($this->_currentPage === 0)
            return array(0, $this->_maxItemsPerPage * $this->_nbPages - 1);

        return array(
                $this->_maxItemsPerPage * ($this->_currentPage - 1),
                $this->_maxItemsPerPage * $this->_currentPage - 1,
                );
    }


    /**
     * Calcule une plage de pages valides autour de la page courante.
     *
     * @param  number $nbReturnedPages
     * @return array
     */
    protected function _calcPageRange($nbReturnedPages)
    {
        /*
         * Détermine le premier numéro de page à afficher dans la liste.
         */
        $minPage = (int)($this->_currentPage - floor(($nbReturnedPages-1)/2));
        if ($minPage < 1) $minPage = 1;

        /*
         * En déduit le dernier numéro de page à afficher.
         */
        $maxPage = $minPage + ($nbReturnedPages - 1);
        if ($maxPage > $this->_nbPages)
            $maxPage = $this->_nbPages;

        /*
         * Dans le cas où on a demandé l'avant-dernière page par exemple, et que
         * la liste contient plus de page que ce qui est demandé, il nous faut
         * réajuster la position min.
         */
        if (1 + $maxPage - $minPage < $nbReturnedPages)
            $minPage = $maxPage - $nbReturnedPages + 1;

        /*
         * On s'assure dans tous les cas qu'on ne commence pas avant la première
         * page.
         */
        if ($minPage < 1)
            $minPage = 1;

        return array($minPage, $maxPage);
    }

    /**
     * Retourne une liste de pages (valides) autour de la page courante.
     *
     * @param  number $nbReturnedPages
     * @return array
     */
    public function getPageRange($nbReturnedPages = 8)
    {
        $this->verifyIsValid();

        $before = $after = array();

        if ($this->_nbPages > 1) {
            /*
             * S'il y a plusieurs pages, calcule les pages à afficher.
             */
            list($minPage, $maxPage) = $this->_calcPageRange($nbReturnedPages);

            for ($numPage = $minPage ; $numPage <= $maxPage ; $numPage++) {
                if ($numPage > $this->_currentPage)
                    $after[$numPage] = $this->getUrl($numPage);
                elseif ($numPage < $this->_currentPage)
                    $before[$numPage] = $this->getUrl($numPage);
            }
        }

        return array($before, $after);
    }

}
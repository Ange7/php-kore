<?php
/**
 * Module permettant la gestion des sessions.
 *
 * @package session
 *
 * @global boolean kore::$conf->session_useDualId
 * @global integer kore::$conf->session_timeout (sync with ini)
 * @global integer kore::$conf->session_concurrencyTimeout
 * @global integer kore::$conf->session_historyCleanUpDelay
 * @global string  kore::$conf->session_cookieDomain (sync with ini)
 * @global string  kore::$conf->session_cookiePath (sync with ini)
 * @global boolean kore::$conf->session_sslOnlyCookie (sync with ini)
 * @global boolean kore::$conf->session_httpOnly (sync with ini)
 *
 */


/**
 * Classe statique de gestion des sessions.
 *
 * Par rapport aux sessions PHP classiques, cette classe ajoute :
 * - gestion des "droits d'accès" : par exemple une fois la session
 *         écrite les variables restent accessibles mais en lecture seule.
 * - gestion de l'expiration de la session : si la session est expirée
 *         alors la considère comme invalide.
 * - gestion d'un double ID de session, l'un changeant très fréquemment,
 *         ce qui diminue fortement les risques de vol de session.
 *
 * @package session
 */
class kore_session
{
    /**
     * Nom du cookie utilisé pour le deuxième ID de session.
     */
    const RANDOM_NAME = 'KORESID';

    const STAT_CTIME = '__koreCTime';
    const STAT_MTIME = '__koreMTime';

    /**
     * Indique si la session a été ouverte.
     *
     * @var boolean
     */
    protected $_isStarted   = false;

    /**
     * Indique si les variables sont accessibles en lecture.
     *
     * @var boolean
     */
    protected $_isReadable  = false;
    /**
     * Indique si les variables sont accessibles en écriture.
     *
     * @var boolean
     */
    protected $_isWritable  = false;

    /**
     * Indique s'il s'agit d'une nouvelle session.
     *
     * @var boolean.
     */
    protected $_isNew       = true;


    /**
     * Variables flash définies au cours de la page actuelle.
     *
     * @var array
     */
    protected $_newFlash = NULL;

    /**
     * Mesure de la durée d'ouverture de la session.
     *
     * @var kore_bench
     */
    protected $_benchOpenTime = NULL;


    /**
     * Retourne l'heure GMT courante.
     * On utilise l'heure GMT pour éviter d'invalider toutes les sessions
     * en cas de changement de fuseau horaire ou de changement heure été/hiver .
     *
     * @return timestamp
     */
    protected static function _getTime()
    {
        $oldTZ = ini_get('date.timezone');
        if ($oldTZ !== 'UTC')
            date_default_timezone_set('UTC');
        $time = time();
        if ($oldTZ !== 'UTC' and $oldTZ)
            date_default_timezone_set($oldTZ);
        return $time;
    }


    public function __construct()
    {
        if ( kore::$conf->sapiName === 'cli' )
            return;

        kore::$conf->get('session_concurrencyTimeout',   60);
        kore::$conf->get('session_historyCleanUpDelay', 300);

        kore::$conf->get('session_sslIsMandatory',      false);

        kore::$conf->bindWithIni( 'session_timeout', 'session.gc_maxlifetime' );
        kore::$conf->bindWithIni(
                'session_cookieDomain',     'session.cookie_domain' );
        kore::$conf->bindWithIni(
                'session_cookiePath',       'session.cookie_path' );
        kore::$conf->bindWithIni(
                'session_sslOnlyCookie',    'session.cookie_secure' );
        kore::$conf->bindWithIni(
                'session_httpOnly',         'session.cookie_httponly' );

        kore::$conf->bindWithIni(
                'session_hashFunction',     'session.hash_function' );

        /**
         * Si l'id de session n'a pas le format requis, alors l'efface.
         */
        if( isset( $_COOKIE[ session_name() ] ) ) {
            if( $this->_checkId( $_COOKIE[ session_name() ] ) === false ){
                unset($_COOKIE[session_name()]);
            }
        }
    }


    public function __destruct()
    {
        $this->_writeFlashVars();
    }


    /**
     * Démarre la session.
     */
    public function start()
    {
        if( $this->_isStarted === true ) return;

        if( kore::$conf->sapiName === 'cli' )
            return;
        if( kore::$conf->session_sslIsMandatory === true and
            kore_request_http::isSecured() === false )
            return;

        $this->_isStarted = true;

        $bench = kore::$debug->benchInit('session', 'open');

        $actualTime = self::_getTime();

        /**
         * On autorise uniquement les sessions basées sur les cookies ; donc si
         * l'ID n'est pas renseigné c'est qu'il s'agit d'une nouvelle session.
         */
        $newSession = ! isset($_COOKIE[session_name()]);

        $sessionError = false;
        $trackError = true;

        kore::$error->clearLastError();
        @ session_start();
        $error = kore::$error->getLastError();
        if( $error )
            $sessionError = 'internal error : ' . $error->getMessage();

        else {

            $this->_benchOpenTime = kore::$debug->benchInit( 'session',
                                                             'openedTime' );

            /**
             * Note : la méthode keep ne fonctionne pas dans notre cas car une
             * instance du benchmark est également conservée dans kore_session ;
             * le destructeur ne serait donc pas appelé avant la destruction de
             * l'instance de kore_session.
             */
//            $this->_benchOpenTime->keep();

            /**
             * Pour une nouvelle session le tableau de session devrait
             * être vide.
             */
            if( $newSession and !empty($_SESSION) ) {
                $sessionError = 'new session which is not empty';

            /**
             * Vérifie que la session ne soit pas expirée.
             */
            } elseif ( isset($_SESSION[self::STAT_MTIME]) and
                ( $_SESSION[self::STAT_MTIME] +
                  kore::$conf->session_timeout < $actualTime ) ) {

                $sessionError = 'session expired';

                /**
                 * Erreur "normale", pas besoin de tracer ce cas.
                 */
                $trackError = false;

            /**
             * Contrôles liés au système de double ID
             */
            } elseif( kore::$conf->get( 'session_useDualId', true ) ) {

                if( ! isset( $_COOKIE[ self::RANDOM_NAME ] ) )
                    $randomID = NULL;
                else $randomID = $_COOKIE[ self::RANDOM_NAME ];

                /**
                 * Initialisation du mécanisme
                 */
                if( ! isset( $_SESSION['__koreRandomId'] ) ) {

                    /**
                     * Le système de RandomId n'est pas initialisé, ce qui est
                     * tout à fait normal s'il s'agit d'une nouvelle session
                     * (cf $newSession).
                     *
                     * Dans le cas contraire il y a plusieurs possibilités :
                     * - la session a été initialisée par un script n'utilisant
                     *   pas le framework
                     * - le cookie de session a été créé "manuellement"
                     * - la session a été détruite sur le serveur entre temps,
                     *   ce qui peut être normal
                     */

                } elseif( $randomID === $_SESSION['__koreRandomId']['current']){

                    /**
                     * Cas normal.
                     */

                } elseif( $randomID === NULL ) {

                    /**
                     * Le RandomId n'a pas été envoyé alors que le mécanisme est
                     * initialisé. Il s'agit probablement d'une tentative de vol
                     * de session.
                     */

                    $sessionError = 'random id is not set';

                //} elseif( $randomID === $_SESSION['__koreRandomId']['pending']){
                  } elseif( isset( $_SESSION['__koreRandomId']['pending'][$randomID] ) ) {

                    /**
                     * Le RandomId envoyé était en attente de confirmation de sa
                     * bonne réception par le client.
                     * Effectue donc le remplacement de l'id.
                     */

                    $oldId = $_SESSION['__koreRandomId']['current'];

                    /**
                     * TODO : on devrait peut être gérer plusieurs ID à l'état
                     * "pending".
                     */
                    //$_SESSION['__koreRandomId']['pending'] = NULL;
                    $_SESSION['__koreRandomId']['pending'] = array();
                    $_SESSION['__koreRandomId']['current'] = $randomID;

                    /**
                     * Conserve l'ID de session pour quelques secondes encore.
                     *
                     * Cela peut notament servir dans le cas d'accès simultanés
                     * avec un temps d'exécution assez long (upload de fichier
                     * par exemple).
                     */
                    if( $oldId !== NULL ) {
                        $_SESSION['__koreRandomId']['old'][$oldId] =
                            self::_getTime() +
                            kore::$conf->session_concurrencyTimeout ;
                    }

                /**
                 * ID non existant : tentative de vol de session ?
                 */
                } elseif(!isset($_SESSION['__koreRandomId']['old'][$randomID])) {

                    if (isset($_SESSION['__koreRandomId']['current']) and
                       isset($_SESSION[self::STAT_CTIME]) &&
                        ( $_SESSION[self::STAT_CTIME] + kore::$conf->session_concurrencyTimeout < $actualTime ) )
                        $sessionError = 'unknown random id';

                } elseif( $expirationTime =
                        $_SESSION['__koreRandomId']['old'][$randomID] ) {

                    /**
                     * Random ID "expiré" : tentative de vol de session ?
                     */
                    if( $expirationTime < $actualTime ) {
                        $sessionError = 'expired random id';
                    }
                }
            }
        }

        /**
         * Session invalide
         */
        if( $sessionError !== false ) {

            if ($trackError) {
                $timeOffset = time() - self::_getTime();

                kore::$error->track('session refusée : '.$sessionError,
                        null, array(
                        'PHP_ID' => @ $_COOKIE[ session_name() ],
                        'RANDOM_ID' => @ $_COOKIE[ self::RANDOM_NAME ],
                        'HISTO_ID' => @ $_SESSION[ '__koreRandomId' ],
                        'CURRENT_TIME' => self::_getTime(),
                        'REQUEST_TIME' => kore::requestTime() - $timeOffset,
                        'CREATION_TIME' => @ $_SESSION[self::STAT_CTIME],
                        'MODIFICATION_TIME' => @ $_SESSION[self::STAT_MTIME],
                        ));
            }

            /**
             * Par sécurité détruit le contenu de la session. Ainsi si un pirate
             * avait réussi a voler la session, il est lui aussi déconnecté.
             */
            $_SESSION = array();
            session_destroy();

            unset($_COOKIE[ session_name() ]);
            $_COOKIE[ self::RANDOM_NAME ] = NULL;

            /**
             * Génère une nouvelle session.
             */
            session_regenerate_id();
            session_start();

        } elseif( isset( $_SESSION['__koreRandomId'] ) ) {

            /**
             * Nettoyage de l'historique des ID
             */
            foreach( $_SESSION['__koreRandomId']['old'] as
                    $idx => $expirationTime ) {

                if( $expirationTime + kore::$conf->session_historyCleanupDelay
                        < $actualTime )

                    unset( $_SESSION[ '__koreRandomId' ][ 'old' ][ $idx ] );
            }
        }

        if( isset( $_SESSION ) and is_array( $_SESSION ) ) {
            $this->_isReadable = true;
            $this->_isWritable = true;
        }

        $this->_isNew = ( $this->_isReadable and ( count($_SESSION) === 0 ) );
        if( !isset($_SESSION[self::STAT_CTIME]) )
            $_SESSION[self::STAT_CTIME] = $actualTime;
        $_SESSION[self::STAT_MTIME] = $actualTime;

        /**
         * Initialise de suite le système de "dualId" en cas de nouvelle session
         */
        if( $this->_isNew and kore::$conf->get( 'session_useDualId', true ) )
            $this->_changeId();
    }


    /**
     * Ferme la session : les variables resteront accessibles en lecture seule.
     */
    public function close()
    {
        if( $this->_isWritable ) {

            $bench = kore::$debug->benchInit( 'session', 'close' );

            if( kore::$conf->session_useDualId and ! $this->_isNew )
                $this->_changeId();

            $this->_writeFlashVars();

            session_write_close();

            $this->_isWritable = false;
        }

        // Détruit l'instance de kore_bench
        $this->_benchOpenTime = NULL;
    }


    /**
     * Détruit la session
     */
    public function destroy()
    {
        if( ! $this->_isWritable ) {
            kore::$error->track("can't destroy session : session not writable");
            return;
        }

        $this->_isReadable = false;
        $this->_isWritable = false;

        session_destroy();
        $this->_setcookie(self::RANDOM_NAME, NULL);
        $this->_setcookie(session_name(), NULL);

        $this->_newFlash = NULL;
        $_SESSION = array();
    }

    /**
     * Indique que le module de session est bien chargé.
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * Retourne l'identifiant de session.
     */
    public function getId()
    {
        if( !$this->_isReadable )
            return false;
        return session_id();
    }

    /**
     * Indique si la session est accessible en lecture.
     *
     * @return boolean
     */
    public function isReadable()
    {
        return $this->_isReadable ;
    }

    /**
     * Indique si la session est accessible en écriture.
     *
     * @return boolean
     */
    public function isWritable()
    {
        return $this->_isWritable ;
    }

    /**
     * Indique s'il s'agit d'une nouvelle session.
     *
     * @return boolean
     */
    public function isNew()
    {
        return $this->_isNew;
    }

    /**
     * Vérifie la validité syntaxique d'un ID de session.
     *
     * @param  string  $sid
     * @return boolean
     */
    protected function _checkId( $sid )
    {
        $hashFunction = kore::$conf->session_hashFunction;
        $len = strlen($sid);
        if (strlen($hashFunction) > 1){
            /**
             * Difficile de connaître la taille dans ce cas, donc on génère un
             * identifiant factice pour en tester la taille.
             */
            if ($len !== strlen(self::_createId('test')))
                return false;
        } else {
            if ($hashFunction == 1)
                $bits = 160;
            else
                $bits = 128;
            if ($len != ceil($bits/ini_get('session.hash_bits_per_character')))
                return false;
        }
        /**
         * Vérifie que le SID ne comporte que des caractères autorisés.
         */
        $mask = '';
        $end = 1 << ini_get('session.hash_bits_per_character');
        for($i = 0; $i < $end; $i++){
            if($i === 63)
                $mask .= ',';
            elseif($i === 62)
                $mask .= '-';
            elseif($i>=36)
                $mask .= chr(65+$i-36);
            elseif($i>=10)
                $mask .= chr(97+$i-10);
            else
                $mask .= chr(48+$i);
        }
        return ( strspn($sid, $mask) === $len );
    }


    /**
     * Génère un identifiant pouvant être utilisé comme ID de session.
     *
     * @return string
     */
    protected function _createId( $salt = NULL)
    {
        if( $salt === NULL )
            $salt = uniqid( mt_rand(), true );

        $hashFunction = kore::$conf->session_hashFunction;

        if( strlen( $hashFunction ) > 1 ) {
            $sid = hash( $hashFunction, $salt );
        } else {
            if( $hashFunction == 1 ) {
                $raw = sha1( $salt );
            } else {
                /**
                 * Par défaut : md5
                 */
                $raw = md5( $salt );
            }

            return self::bin_to_readable($raw,
                    ini_get('session.hash_bits_per_character'));
        }

        return $sid;
    }


    /**
     * Set a cookie, using session params
     *
     * @param string  $name
     * @param string  $value
     * @param integer $time
     */
    protected function _setcookie( $name, $value )
    {
        setcookie( $name, $value, NULL,
            kore::$conf->session_cookiePath,
            kore::$conf->session_cookieDomain,
            kore::$conf->session_sslOnlyCookie,
            kore::$conf->session_httpOnly );
    }


    /**
     * Change le deuxième ID de session.
     */
    protected function _changeId()
    {
        /**
         * Ne change surtout pas l'ID si l'utilisateur n'est plus connecté,
         * car il ne recevra pas cet entête.
         */
        if( connection_aborted() === true )
            return false;

        if( ! $this->_isWritable ) {
            kore::$error->track("can't change session id : session not writable");
            return;
        }

        if( headers_sent( $file, $line ) ) {
            kore::$error->track("can't set session random id : headers already sent",
                    null, null, $file, $line);

        } else {

            if( ! isset( $_SESSION[ '__koreRandomId' ] ) ) {
                $_SESSION[ '__koreRandomId' ] = array(
                    'current' => NULL,
//                    'pending' => NULL,
                    'pending' => array(),
                    'old' => array(),
                    );
            }

            /**
             * Le nouvel ID est d'abord ajouté à la liste "new". Il ne sera validé
             * que si on remarque son utilisation.
             */
            $newId = $this->_createId();
            //$_SESSION[ '__koreRandomId' ][ 'pending' ] = $newId;
            $_SESSION[ '__koreRandomId' ][ 'pending' ][$newId] = true ;

            $this->_setcookie( self::RANDOM_NAME, $newId );

            /**
             * Au cas où des requêtes se croiseraient, on ajoute également cet ID
             * à la liste de ceux à conserver durant quelques secondes.
             */
            $_SESSION[ '__koreRandomId' ][ 'old' ][ $newId ] =
                self::_getTime() + kore::$conf->session_concurrencyTimeout ;

        }
    }


    /**
     * Retourne la valeur d'une variable de session.
     *
     * @param  string  $name
     * @return mixed
     */
    public function __get( $name )
    {
        if (! $this->_isReadable) {
            kore::$error->track("can't get variable [$name] : session not readable");
            return NULL;

        } elseif( ! array_key_exists( $name, $_SESSION ) ) {

            return NULL;
        }

        return $_SESSION[$name];
    }


    /**
     * Modifie une variable de session.
     *
     * @param string  $name
     * @param mixed   $value
     */
    public function __set( $name, $value )
    {
        if (! $this->_isWritable) {
            kore::$error->track("can't set variable [$name] : session not writable");
            return false;
        }

        $_SESSION[$name] = $value;
    }


    /**
     * Teste l'existence d'une variable de session.
     *
     * @param  string  $name
     * @return boolean
     */
    public function __isset( $name )
    {
        if (! $this->_isReadable) {
            kore::$error->track("can't get variable [$name] : session not readable");
            return false;
        }

        return isset( $_SESSION[ $name ] );
    }


    /**
     * Efface une variable de session.
     *
     * @param  string  $name
     */
    public function __unset( $name )
    {
        if (! $this->_isWritable) {
            kore::$error->track("can't set variable [$name] : session not writable");
            return false;
        }

        if( isset( $_SESSION[ $name ] ) )
            unset( $_SESSION[ $name ] );
    }


    /**
     * Vérifie l'existence d'une variable "flash".
     *
     * @param  string $name
     * @return boolean
     */
    public function issetFlash( $name )
    {
        return ( $this->_isReadable and isset( $_SESSION['__koreFlash'] ) and
            array_key_exists( $_SESSION['__koreFlash'], $name ) );
    }

    /**
     * Retourne la valeur d'une variable "flash" (variable à durée ponctuelle).
     *
     * Note : ne produit pas d'erreur dans le cas où la variable flash n'existe
     * pas.
     *
     * @param  string $name
     * @return mixed
     */
    public function getFlash( $name )
    {
        if (!$this->_isReadable) {
            kore::$error->track("can't get flash variable [$name] : session not readable");
            return NULL;
        }

        if(! isset( $_SESSION['__koreFlash'][ $name ] ) )
            return NULL;

        return $_SESSION['__koreFlash'][ $name ] ;
    }

    /**
     * Enregistre une variable "flash". Elle sera automatiquement détruite à la
     * prochaine page.
     *
     * @param  string  $name
     * @param  mixed   $value
     */
    public function setFlash( $name, $value )
    {
        if (!$this->_isWritable) {
            kore::$error->track("can't set flash variable [$name] : session not writable");
            return NULL;
        }

        if( $this->_newFlash === NULL )
            $this->_newFlash = array( $name => $value );
        else $this->_newFlash[ $name ] = $value;
    }

    /**
     * Retourne une variable "flash" nouvellement créée,
     * qui n'a pas encore été enregistrée.
     *
     * @param string $name
     */
    public function getFlashPending( $name ) {
        if (isset($this->_newFlash[$name])) {
            return $this->_newFlash[$name];
        }
    }


    /**
     * Efface les anciennes variables "flash" et enregistre les nouvelles.
     */
    protected function _writeFlashVars()
    {
        if( $this->_isWritable ) {

            if( $this->_newFlash !== NULL )

                $_SESSION['__koreFlash'] = $this->_newFlash;

            elseif( isset( $_SESSION['__koreFlash'] ) )
                unset( $_SESSION['__koreFlash'] );

            $this->_newFlash = NULL;
        }
    }

    /**
     * Retourne une représentation lisible d'une chaine binaire.
     * L'algo est issu du générateur d'id de session de PHP
     * (fonction bin_to_readable() dans /ext/session/session.c de PHP 5.3.3)
     *
     * Attention : la fonction ne prend pas la peine d'avoir une représentation
     * identique à ce que produisent les fonctions telles que md5() ou sha1().
     * Toutefois l'algo est très rapide et relativement simple.
     *
     * @param  string  $string
     * @param  integer $nbits
     * @return string
     */
    static public function bin_to_readable($string, $nbits = 6)
    {
        $table = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-,';
        $out = '';
        $len = strlen($string);
        $mask = (1 << $nbits) - 1;

        $i = 0;
        $w = 0;
        $have = 0;

        while (true) {
            if ($have < $nbits) {

                if ($i < $len) {
                    $w |= (ord($string[$i++]) << $have);
                    $have += 8;
                } elseif ($have === 0) {
                    break;
                } else {
                    $have = $nbits;
                }
            }

            $out .= $table[$w & $mask];
            $w >>= $nbits;
            $have -= $nbits;
        }
        return $out;
    }

}

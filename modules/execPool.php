<?php

class kore_execPool
{
    protected $maxProcess;
    protected $loopDelay;

    protected $listTodo     = array();
    protected $listRunning  = array();
    public    $listDone     = array();

    /**
     * Initialise un objet kore_execPool
     *
     * @param integer $maxProcess
     * @param integer $loopDelay (in µs)
     */
    public function __construct($maxProcess = 2, $loopDelay = 5000)
    {
        $this->setMaxProcess($maxProcess);
        $this->setLoopDelay($loopDelay);
    }

    /**
     * Change the number of allowed parallel executions
     *
     * @param integer $maxProcess
     */
    public function setMaxProcess( $maxProcess )
    {
        $this->maxProcess = (int)$maxProcess;
    }

    /**
     * Change the idle time between each verification pass (in µs)
     *
     * @param integer $loopDelay
     */
    public function setLoopDelay( $loopDelay )
    {
        $this->loopDelay = (int) $loopDelay;
    }

    /**
     * Ajoute une commande à la pile
     *
     * @param  kore_exec $cmdLine instance kore_exec, ou chaine $cmdLine
     * @param  string  $executionPath (uniquement si $cmdLine est une chaine)
     * @return kore_exec instance kore_exec utilisée
     */
    public function addCmd( $cmdLine, $executionPath = NULL )
    {
        if( $cmdLine instanceof kore_exec ) {
            $exec = $cmdLine;
        } else {
            $exec = new kore_exec( $cmdLine, $executionPath );
        }
        $this->listTodo[] = $exec;

        return $exec;
    }

    public function launch()
    {
        while( count($this->listTodo) > 0
            or count($this->listRunning) > 0 ) {

            foreach( $this->listRunning as $idx => $exec )
                if (!$exec->isRunning()) {
                    unset($this->listRunning[$idx]);
                    array_push( $this->listDone, $exec );

                    $this->event_ProcessEnd( $exec );
                }

            if( count($this->listTodo) > 0 and
                count($this->listRunning) < $this->maxProcess ) {

                $exec = array_shift( $this->listTodo );
                array_push( $this->listRunning, $exec );

                $this->event_ProcessStart( $exec );

            } else {
                $this->waitForEnd();
            }
        }
    }

    public function waitForEnd()
    {
        usleep( $this->loopDelay );
    }

    public function event_ProcessStart( kore_exec $exec )
    {
        $exec->start();
    }

    public function event_ProcessEnd( kore_exec $exec )
    {

    }

}

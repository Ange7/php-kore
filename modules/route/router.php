<?php
/**
 * Class to allow handling of complex URL rewriting directly from PHP.
 *
 * @package route
 */
class kore_route_router
{
    private $_sendHTTPHeaders = true;

    private $_directRules = array();
    private $_rules = array();

    /**
     * Use a cache to store compiled routes
     *
     * @var kore_cache_common
     */
    private $_cacheCompilation = null;

    /**
     * We use a per chunk cache, to skip _compileChunk() process when possible.
     *
     * @var array
     */
    private $_cacheChunkCompilation = array();

    /**
     * Define regexps shorcuts
     *
     * d  : decimal digits
     * f  : float (including negative values)
     * h  : hexadecimal digits
     * a  : letters and digits
     * S  : exclude -.~,
     * '' : exclude /
     */
    private $_pregTypes = array(
            'd' => '\\d+',
            'f' => '-?[0-9]+(?:\.?[0-9]+)?',
            'h' => '[[:xdigit:]]+',
            'a' => '[[:alnum:]]+',
            'w' => '\\w+',
            'S'  => '[^\\-.~,]+',
            ''  => '[^/]+',
    );

    /**
     * Define current dispatched route
     *
     * @var kore_route_route
     */
    private $_currentRoute;

    /**
     * Add new preg type to predefined list
     *
     * @param string $id
     * @param string $pregType
     */
    public function addPregType($id, $pregType)
    {
        $this->_pregTypes[$id] = $pregType;
    }

    /**
     * Register all rules for direct routing.
     * $ruleList is a key-pair array, of URI (or URL) → callback.
     *
     * @param array $ruleList
     */
    public function setDirectRules(array $ruleList)
    {
        $this->_directRules = $ruleList;
    }

    /**
     * Register all rules from routing.
     * $ruleList is an array of tuples, each tuples contains preg rule followed
     * by the callback.
     * Array keys can be used, to allow referencing specific rule later.
     *
     * @param array $ruleList
     */
    public function setRules(array $ruleList)
    {
        $this->_rules = $ruleList;
    }

    /**
     * Allow use of a cache to store compiled routes;
     *
     * @param kore_cache_common $cache
     */
    public function useCompilationCache(kore_cache_common $cache)
    {
        $this->_cacheCompilation = $cache;
    }

    /**
     * Dispatch an URI to a specific method, following predefined rules. If no
     * URI or no host was given, use the current one.
     *
     * @param  string $uri
     * @param  string $host
     * @param  array  $getParams
     * @return mixed
     */
    public function dispatch($uri = null, $host = null, array $getParams = null)
    {
        kore::$debug->benchCheckPoint('main', 'dispatch');

        if ($uri === null)
            $uri = kore_request_http::getUri();
        if (($pos = strpos($uri, '?')) === false)
            $url = $uri;
        else
            $url = substr($uri, 0, $pos);
        if ($host === null)
            $host = kore_request_http::getHTTPHost();

        $routeClass = kore::$conf->get('routeClass','kore_route_route');

        /*
         * Erase previous current dispatched route property
         */
        $this->_currentRoute = NULL;

        /*
         * Try direct rules, with the full URI
         */
        $route = null;
        if (isset($this->_directRules[$uri])) {
            $route = $routeClass::fromDirect($host, $url, $uri,
                    $uri, $this->_directRules[$uri]);

            /*
             * Check once again direct rules, with just the URL
             */
        } elseif (isset($this->_directRules[$url])) {
            $route = $routeClass::fromDirect($host, $url, $uri,
                    $url, $this->_directRules[$url]);

        } else {

            /*
             * If the compilation cache is enabled, we try to load it.
             */
            $compiledRoutes = null;
            if (isset($this->_cacheCompilation)){
                /*
                 * Use current $host and checksum of current rules as idCache.
                 */
                $idCache = __CLASS__.".$host.".md5(json_encode($this->_rules));
                $compiledRoutes = $this->_cacheCompilation->get($idCache);

                if (!is_array($compiledRoutes)){
                    $compiledRoutes = array();
                    foreach ($this->_rules as $idx => $rule)
                        $compiledRoutes[$idx] = $this->_compileRoute($rule[0]);
                    $this->_cacheCompilation->set($idCache, $compiledRoutes);
                }
            }

            foreach ($this->_rules as $idx => $rule){
                /*
                 * Get regexp from given rule.
                 */
                if (isset($this->_cacheCompilation))
                    $compiledRoute = $compiledRoutes[$idx];
                else
                    $compiledRoute = $this->_compileRoute($rule[0]);

                if (preg_match($compiledRoute[0], $url, $matches) !== 0){
                    $params = $matches;

                    if (!empty($rule[2]))
                        $routeClassToUse = $rule[2];
                    else $routeClassToUse = $routeClass;

                    unset($params[0]);
                    $route = $routeClassToUse::fromPreg($host, $url, $uri,
                            $idx, $rule[0], $rule[1], $params,
                            $compiledRoute[1]);
                    break;
                }
            }
        }

        if ($route === null){
            /*
             * If no route match the current URL, throw a 404 error
             */
            $this->_onHTTPStatus(kore_response_httpStatus::NotFound404());

        } else {
            /*
             * Import parameters from $_GET, only if they haven't been given.
             */
            if ($getParams === null)
                $getParams = $_GET;

            /*
             * route params could not be override by other parameters
             */
            foreach ($getParams as $paramName => $paramValue) {
                if (!isset($route->params[$paramName])) {
                    $route->params[$paramName] = $paramValue;
                } else {
                    /*
                     * Deny overriding, and refuse this URL.
                     */
                    $this->_onHTTPStatus(kore_response_httpStatus::Forbidden403());
                }
            }

            try {
                $this->_currentRoute = $route;

                $response = $route->route();

                if ($response instanceOf kore_template) {
                    $response->context->currentRoute = $route;
                    $response->send();
                } elseif ($response instanceOf kore_response_file) {
                    $response->send();
                } elseif ($response instanceOf kore_response_json) {
                    $response->send();
                } elseif($response instanceOf kore_response_data) {
                    $response->send();
                } elseif($response instanceOf kore_response_httpRedirect) {
                    $response->send();
                } else {
                    /*
                     * For now, kore_route_router only handle a very reduced
                     * list of responses.
                     */
                    throw new UnexpectedValueException("kore_route_router doesn't handle this kind of response for now");
                }

            } catch (kore_response_httpStatus $e){
                $this->_onHTTPStatus($e);
            }

            /*
             * Erase current route property
             */
            $this->_currentRoute = NULL;
        }
    }

    /**
     * Get route from its name.
     *
     * @param string $name
     * @throws Exception
     * @return kore_route_route
     */
    public function getRoute($name)
    {
        /*
         * If the route doesn't exist, we throw an exception
         */
        if (!isset($this->_rules[$name]))
            throw new OutOfBoundsException("Undefined route $name");

        $rule = $this->_rules[$name];

        $routeClass = kore::$conf->get('routeClass', 'kore_route_route');
        if (!empty($rule[2]))
            $routeClass = $rule[2];

        $compiledRoute = $this->_compileRoute($rule[0]);

        $route = $routeClass::fromPreg(null, null, null,
                $name, $rule[0], $rule[1], array(), $compiledRoute[1]);
        $route->init();
        return $route;
    }

    /**
     * Skip sending HTTP headers while handling kore_response_httpStatus
     * exceptions.
     */
    public function disableHTTPHeaders()
    {
        $this->_sendHTTPHeaders = false;
    }

    /**
     * Enable sending of HTTP headers while handling kore_response_httpStatus
     * exceptions (on by default).
     */
    public function enableHTTPHeaders()
    {
        $this->_sendHTTPHeaders = true;
    }

    /**
     * Return current dispatched route
     *
     * @return kore_route_route
     */
    public function getCurrentRoute()
    {
        return $this->_currentRoute;
    }

    /**
     * Method which handle HTTP response exceptions
     *
     * By default we only change the HTTP header, we don't display any error
     * page.
     *
     * @param  kore_response_httpStatus $e
     * @throws kore_response_httpStatus
     */
    protected function _onHTTPStatus(kore_response_httpStatus $e)
    {
        /*
         * If asked, then send HTTP header
         */
        if ($this->_sendHTTPHeaders === true){
            $code = $e->getCode();
            if (kore::$conf->sapiName === 'apa')
                header($e->getMessage(), true, $code);
            else header("Status: $code ".$e->getMessage());
        }

        /*
         * We throw again the same exception, to allow user to display
         * appropriate contents.
         */
        throw $e;
    }

    /**
     * Convert the route into a PCRE pattern, and build an helper for reverse
     * routing.
     *
     * @param  string $route
     * @return array
     */
    private function _compileRoute($route)
    {
        $bench = kore::$debug->benchInit(__METHOD__, $route);

        $offset = 0;
        $offsetMax = strlen($route) - 1;

        $preg    = '#^';
        $reverse = '';

        while (($start = strpos($route, '[', $offset)) !== false){

            /*
             * Search the end of the param chunk
             */
            $end = strpos($route, ']', $start + 1);
            if (($end < $offsetMax) and $route[$end+1] === '?'){
                if (--$start < 0) $start=0;
                $end++;
            }

            /*
             * If we skipped part of the string, add it to the preg pattern as
             * a regular string.
             */
            if ($start > $offset){
                $chunk = substr($route, $offset, $start-$offset);
                $reverse .= $chunk;
                $preg .= preg_quote($chunk, '#');
            }

            /*
             * Add the param chunk to the preg pattern (and reverse one).
             */
            $chunk = substr($route, $start, 1 + $end - $start);
            list($reverseAnchor, $pattern) = $this->_compileChunk($chunk);
            $preg .= $pattern;
            $reverse .= $reverseAnchor;

            $offset = $end+1;
        }

        /*
         * If end of the route was not reached, add the rest as regular string.
         */
        if ($offset <= $offsetMax){
            $chunk = substr($route, $offset);
            $reverse .= $chunk;
            $preg .= preg_quote($chunk, '#');
        }

        $preg .= '$#u';

        return array($preg, $reverse);
    }

    /**
     * Convert a param mask from route into a PCRE pattern.
     *
     * @param  string $chunk
     * @throws kore_route_exception
     * @return array
     */
    private function _compileChunk($chunk)
    {
        if (isset($this->_cacheChunkCompilation[$chunk]))
            return $this->_cacheChunkCompilation[$chunk];

        /*
         * Search the colon, to identify type and name of the parameter.
         */
        $pos = strpos($chunk, ':', 1);
        if ($pos === false)
            throw new kore_route_exception("can't compile route chunk '$chunk'");
        $reversedPos = -1 * (strlen($chunk)-$pos) - 1;

        $startBracket = strrpos($chunk, '[', $reversedPos);

        $isOptionnal = (substr($chunk, -1) === '?');
        $endBracket = strlen($chunk) -1 - $isOptionnal;

        $type = substr($chunk, $startBracket+1, $pos - $startBracket - 1);

        if (strlen($type) > 1){
            /*
             * We don't modify the pattern, but not sure it's a good idea.
             * It allow enum types like "foo|bar"
             */
        } else {
            if (!isset($this->_pregTypes[$type]))
                throw new kore_route_exception("can't find type '$type' in route chunk '$chunk'");
            $type = $this->_pregTypes[$type];
        }

        $name = substr($chunk, $pos+1, $endBracket - $pos - 1);
        $pattern = "(?<$name>$type)";
        $reverseAnchor = '{'.$name.'}';

        if ($isOptionnal === true){
            $pattern = '(?:'.preg_quote($chunk[0], '#').$pattern.')?';
            $reverseAnchor = $chunk[0].$reverseAnchor.'?';
        }

        return $this->_cacheChunkCompilation[$chunk] =
                array($reverseAnchor, $pattern);
    }

}

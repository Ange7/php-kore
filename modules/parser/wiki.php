<?php

class kore_parser_wiki extends kore_parser {

    /**
     * Configuration du parser
     * @var unknown_type
     */
    protected $_config = array(
        // Header principal
        'header'          => 'h1',
        // Header intermédiaire
        'mediumHeader'    => 'h2',
        // Petit header
        'smallHeader'     => 'h3',
        // Langue par défaut
        'defaultLang'     => 'fr',
        // Prefix des classes CSS à appliquer aux images (quand précisées)
        'imgClassPrefix'  => 'img_',
        // Classe CSS pour le texte souligné
        'underlineClass'  => 'underline',
        // Remplacement des guillemets par des guillemets typographiques
        'replaceQuotes'   => true,
        // Appliquer ou non des liens sur les notes de bas de page
        'notesLinks'      => true,
        // Préfixe des liens des notes
        'notesPrefix'     => 'note-',
        // Préfixe des classes pour les notes (dans le texte)
        'notesClass'      => 'note',
        // Préfixe des noms des liens des notes (dans le texte)
        'notesNamePrefix' => 'text-note-',
        // Afficher les notes dans des title
        'notesInTitle'    => false,
        // Character encoding.
        'charset'         => 'UTF-8',
        // Automatically add <p> and </p> before and after the text
        'autoP'           => TRUE,
        // Désactive les blocs
        'noBlock'         => FALSE
    );

    /**
     * Masques PCRE
     * @var array
     */
    private $_miscPatterns = array();

    /**
     * Chaînes de remplacements pour les masques
     * @var array
     */
    private $_miscReplaces = array();

    /**
     * Masque des headers
     * @var string
     */
     protected $_headersPattern = '`^(={2,4})\s(.+)(?:\Z|\n)$`Um';

    /**
     * Masques de formattage (gras, italique, ...)
     * @var array
     */
    protected $_defaultFormatPatterns = array(
        // Doivent toujours être précédés d'un caractère blanc
        // Gras : **texte**
        '`(\*{2})(?!\*)(.*)\1`Ums',
        // Italique : //texte//
        '`((?<!:)/{2})(.*)(?<!:)\1`Ums',
        // Souligné : __texte__
        '`(_{2})(.*)\1`Ums',
        // Supprimé : --texte--
        '`(-{2}(?!-))(.*)\1`Ums',
    );

    /**
     * Masques utilisés lors du parsing
     * Recalculé à chaque parsing, pour tenir compte de la configuration
     * @var array
     */
    protected $_formatPatterns = array();

    /**
     * Masque des URL
     * @var string
     */
    //protected $_urlPattern = '`&lt;(?:([^|]*)\|(?:([^|]*)\|(?:([^|]*)\|)?)?)?([a-z0-9]+://.+)&gt;`U';
    protected $_urlPattern = '`&lt;(?:([^|]*)\|(?:([^|]*)\|(?:([^|]*)\|(?:([^|]*)\|)?)?)?)?([a-z0-9]+://.+)&gt;`U';

    /**
     * Masque des images
     * @var string
     */
    protected $_imagesPattern= '`\{(.*(?=\|))\|?([^\|\}]+)\|?((?<=\|).*)?\}`U';

    /**
     * Masque des listes
     * Récupère les blocs qui composent les listes
     * @var string
     */
    protected $_listsBlockPattern = '`^(#|\*)\s+(.*)(\Z|\n)$`msU';

    /**
     * Masque des listes
     * Pour chaque ligne d'un bloc d'une liste
     * @var string
     */
    protected $_listsPattern = '`^(?:#|\*)\s+(.*)$`mU';
    //protected $listsPattern = '`^((#|\*)\s+.*)<br />$`mU';

    /**
     * Masque des blocs de citation
     * @var string
     */
    protected $_blockquotesPattern = '`^(&gt;\s+.*)(?!\n&gt;)(\n\n|\n\Z)`msU';

    /**
     * Masques des liens Wiki [[wikipage]]
     * @var string
     */
    protected $_wikiLinksPattern = '`\[\[([^\]]+)]]`U';

    /**
     * Masques des notes de bas de page
     * @var string
     */
    protected $_notesPattern = '`@@(.*)@@`U';

    /**
     * Notes de base de page, pour affichage séparé
     * @var array
     */
    protected $_notes = array();

    /**
     * La chaîne d'origine à analyser
     * @var string
     */
    protected $_wikiString = '';

    /**
     * Transformation de la chaîne, avec gestion d'une configuration ponctuelle
     * @param 	string $string
     * @param 	array  $localConfig
     * @return 	string
     */
    public function parse($string, array $localConfig = array())
    {
        $globalConfig = $this->_config;
        $this->setConfig($localConfig);
        $this->_wikiString = $this->_parse($string);
        $this->_config = $globalConfig;
        return $this->_wikiString;
    }

    /**
     * Transformation de la chaîne en html
     * @param string $wikiString
     * @return string
     */
    protected function _parse($wikiString) {
        // Bench
        $bench = kore::$debug->benchInit('WikiParser');

        /*
         * Intégration du block <nowiki>
         */
        if(preg_match('#^(.*)<nowiki>(.*)</nowiki>(.*)$#sm', $wikiString, $matches)) {
            $first  = $this->parse($matches[1]);
            $second = $this->_simpleParse($matches[2]);
            $third  = $this->parse($matches[3]);
            return $first.$second.$third;
        }

        /******************************
         * Préparation, configuration
         ******************************/

        // Prepare des masques supplémentaires, plus simples
        if (!$this->_config['noBlock'])
            $this->_additionnalPatterns();

        // Définit les masques de formattage selon la configuration
        $this->_formatPatterns = $this->_defaultFormatPatterns;
        if ($this->replaceQuotes)
            $this ->_formatPatterns[] = '`\s(")([^"]*)\1`Ums';

        // Sauts de ligne Unix uniquement
        $str = str_replace( array( "\r\n", "\r" ), "\n", $wikiString);

        // HTMLSpecialchars
        $str = $this->_escape($str);

        /************************************
         * Remplacement des éléments inline
         ************************************/

        // Images
        $str = preg_replace_callback($this->_imagesPattern,
                array($this, '_parseImages'), $str);

        // URLs
        $str = preg_replace_callback($this->_urlPattern,
                array($this, '_parseUrl'), $str);

        // Format (gras, italique, ...)
        $str = $this->_parseFormat($str);

        // Wikilinks [[wikipage]]
        $str = preg_replace_callback($this->_wikiLinksPattern,
                array($this, '_parseWikiLinks'), $str);

        // Notes de bas de page
        $str = preg_replace_callback($this->_notesPattern,
                array($this, '_parseNotes'), $str);

        /*******************************************
         * Remplacements des éléments de type bloc
         *******************************************/
        if (!$this->_config['noBlock']) {
            // Headers
            $str = preg_replace_callback($this->_headersPattern,
                    array($this, '_parseHeaders'), $str);

            // Listes ol / ul
            $str = preg_replace_callback($this->_listsBlockPattern,
                array($this, '_parseLists'), $str);

            // Blockquotes
            $str = preg_replace_callback($this->_blockquotesPattern,
                    array($this, '_parseBlockquotes'), $str);
        }
        /************************
         * Remplacements divers
         ************************/
        $str = preg_replace($this->_miscPatterns, $this->_miscReplaces, $str);

        if (!$this->_config['noBlock'])
            $str = $this->_makeParagraphs($str);

        return $str;
    }

    /**
     * Préparation des masques simples supplémentaires
     */
    protected function _additionnalPatterns() {
        // Ligne horizontale
        $this->_miscPatterns[] = '`(\n)(----)\n`';
        $this->_miscReplaces[] = PHP_EOL.'<hr />';
    }

    /**
     * Passe en revue les headers
     * @param array $input
     * @return string
     */
    protected function _parseHeaders($input) {
        if ($input[1] === '==')
            $tag = $this->header;
        elseif ($input[1] === '===')
            $tag = $this->mediumHeader;
        elseif ($input[1] === '====')
            $tag = $this->smallHeader;
        return sprintf('<%1$s>%2$s</%1$s>', $tag, $input[2]);
    }

    /**
     * Passe en revue le formattage de base (gras, souligné, ...)
     * @param array $input
     * @return string
     */
    protected function _parseFormat($input) {
        if (is_array($input)) {
            $class = '';
            if ($input[1] === '**')
                $tag = 'strong';
            elseif ($input[1] === '//')
                $tag = 'em';
            elseif ($input[1] === '__') {
                $tag = 'em';
                $class = ' class="'.$this->underlineClass.'"';
            }
            elseif ($input[1] === '--')
                $tag = 'del';
            elseif ($input[1] === '"' && $this->replaceQuotes) {
                $tag = 'q';
            }
            $input = sprintf(' <%1$s%2$s>%3$s</%1$s>',
                    $tag, $class, $input[2]);
        }

        return preg_replace_callback($this->_formatPatterns,
                array($this, '_parseFormat'), $input);
    }

    /**
     * Passe en revue les url
     * @param  array $input
     * @return string
     */
    protected function _parseUrl($input) {
        // Ordre des masques, en théorie
        // 1 : Nom
        // 2 : Title
        // 3 : Langue
        // 4 : URL

        $nom    = $input[1];
        $title  = $input[2];
        $target = $input[3];
        $lang   = $input[4];
        $url    = $input[5];
        if ('' === $url) {
            return $input[0];
        }
        if ('' === $nom) {
            $nom = $url;
        }

        $target = $target ? 'target="'.$target.'"' : '';
        $title = $title ? 'title="'.$title.'"' : '';
        $lang = $lang ? 'lang="'.$lang.'"' : '';

        $link = sprintf('<a href="%1$s" %2$s %3$s %5$s>%4$s</a>',
                    $url, $title, $lang, $nom, $target);

        return $link;
    }

    /**
     * Passe en revue les images
     * @param array $matches
     */
    protected function _parseImages($input) {
        $alt = $input[1];
        $src = $input[2];
        $pfx = $this->imgClassPrefix;
        $classes = '';
        if (isset($input[3]) && '' !== $input[3]) {
            $classes = 'class="'.$pfx.str_replace(' ', ' '.$pfx, $input[3]).'"';
        }

        return sprintf('<img alt="%s" src="%s" %s/>',
                $alt, $src, $classes);
    }

    /**
     * Passe en revue les listes (ul et ol)
     * @param array $input
     */
    protected function _parseLists($input) {
        // On détermine le type de liste
        $tag = ('*' === $input[1]) ? 'ul' : 'ol';
        $return = "<$tag>\n";
        $return .= preg_replace($this->_listsPattern, '<li>$1</li>', $input[0]);
        $return .= "</$tag>";

        return $return;
    }

    /**
     * Passe en revue les blocs de citations
     * @param array $input
     */
    protected function _parseBlockquotes($input) {
        $return = '<blockquote>'.nl2br(str_replace('&gt;', '', $input[1])).'</blockquote>';

        return $return;
    }

    /**
     * Passe en revue les liens wiki
     * @param array $input
     */
    protected function _parseWikiLinks($input) {
        $return = sprintf('<a href="%s">%s</a>',
                $this->_makeWikiLink($input[1]), $input[1]);

        return $return;
    }

    /**
     * Passe en revue les notes de bas de page
     * @param array $input
     */
    protected function _parseNotes($input) {
        $this->_notes[] = $input[1];
        $index = count($this->_notes);
        $linkName = $this->notesNamePrefix.$index;
        $noteLink = $this->notesPrefix.count($this->_notes);
        $title = $this->notesInTitle ? 'title="'.$input[1].'"' : '';
        if ($this->notesLinks) {
            $return = sprintf('<a name="%s" href="#%s" class="%s" %s>[%d]</a>',
                $linkName, $noteLink, $this->notesClass, $title, $index);
        }
        else {
            $return = sprintf('<span class="%s" %s>[%d]</span>',
                $this->notesClass, $title, $index);
        }

        return $return;
    }

    /**
     * Retourne la chaîne protégée pour l'affichage
     * @param string $text
     * @return string
     */
    protected function _escape($text) {
        return htmlspecialchars($text, ENT_QUOTES, $this->charset);
    }

    /**
     * Retourne un tableau contenant les notes de base de page
     * @return array
     */
    public function getNotes() {
        $return = array();
        foreach ($this->_notes as $i =>$note) {
            $index = $i+1;
            $link = $this->notesNamePrefix.$index;
            if ($this->notesLinks) {
                $return[] = sprintf('<a href="#%s">[%d] %s</a>',
                        $link, $index, $note);
            }
            else {
                $return[] = sprintf('[%d] %s', $index, $note);
            }
        }

        return $return;
    }

    /**
     * A surcharger !
     * Retourne l'URL d'un lien interne wiki [[link]]
     * @param string $link L'url de base
     * @return string
     */
    protected function _makeWikiLink($link) {
        return $link;
    }

    /**
     * Rajoute les balises p aux bons endroits
     * Obligé de parser la chaîne ligne par ligne
     */
    protected function _makeParagraphs($str) {
        $lines = explode("\n", trim($str));
        $str = '';
        $inP = FALSE;
        $br = '';
        $prevBr = '';
        if ($this->autoP) {
            $str = '<p>';
            $inP = TRUE;
        }
        $emptyP = TRUE;

        foreach ($lines as $numLine => $line) {
            $addStr = '';
            $prevBr = $br;
            $br = "\n";
            $nl = ('' === $line);
            if ($nl and $inP and !$emptyP) {
                $prevBr = "";
                $addStr .= "\n</p>\n";
                $inP = FALSE;
            }
            if (!$nl and !preg_match('`</?(h[1-6r]|ul|ol|li|blockquote)`', $line)) {
                $emptyP = FALSE;
                if (!$inP) {
                    $addStr .= "<p>\n";
                    $inP = TRUE;
                    $emptyP = FALSE;
                }
                if ($inP)
                    $br = "<br />\n";
            }
            else {
                if ($nl)
                    $prevBr = "";
                else
                    $prevBr = "\n";
            }
            $str .= $prevBr.$addStr;
            if (!$nl)
                $str .= $line;

        }
        if ($inP)
            $str .= $prevBr.'</p>';
        return $str;
    }

    /**
     * Applique le minimum de modification sur une chaine.
     *
     * @param string $str
     * @return string
     */
    protected function _simpleParse($str)
    {
        // Sauts de ligne Unix uniquement
        $str = str_replace( array( "\r\n", "\r" ), "\n", $str);

        if (!$this->_config['noBlock'])
            $str = $this->_makeParagraphs($str);

        return $str;
    }
}
